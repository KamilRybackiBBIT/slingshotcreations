#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using GameCore.Utils;

public class MeshMerger : EditorWindow
{

	public static MeshMerger window;

	[MenuItem("Tools/MeshMerger")]
	public static void OpenWindow()
	{
		window = (MeshMerger)EditorWindow.GetWindow(typeof(MeshMerger)); //create a window
		window.titleContent = new GUIContent("Mesh Merger", "Tool window to marge and save multiple meshes into one");
		window.minSize = new Vector2(320, 175);
		lastSelected = null;
		if (permittedComponents.Count == 0)
		{
			LoadComponentList();
		}
	}

	private string lastPath = "Assets//";

	private bool autoArangeNewMesh;
	bool useNewMerger = true;
	bool permittedComponentHide = false;
	int x = 10;
	int z = 10;
	string x_string = "10";
	string z_string = "10";
	string errorMessage = "";
	int errorLine = 0;

	static List<string> permittedComponents = new List<string>();
	List<Type> permittedComponentsType;
	Color defoult;
	string tmpComponentName = "";
	GameObject visualizationObject;
	static UnityEngine.Object[] lastSelected = null;

	void OnGUI()
	{
		var currentHeight = 0;
		errorMessage = "";
		errorLine = 0;

		if (window == null)
			OpenWindow();

		bool canMerge;
		
		useNewMerger = GUI.Toggle(new Rect(5, currentHeight, position.width - 10, 16), useNewMerger, "Use new MeshMerger with chunk");
		currentHeight += 20;

		GameObject[] selectedObjects;


		List<MeshFilter> selectedMeshes;

		if (useNewMerger)
		{
			selectedObjects = Selection.gameObjects;
			if (lastSelected != null && lastSelected.Length != 0)
			{
				selectedObjects = new GameObject[lastSelected.Length];
				for (int i = 0; i < lastSelected.Length; i++)
				{
					selectedObjects[i] = lastSelected[i] as GameObject;
				}
			}

			selectedMeshes = new List<MeshFilter>();

			for (int i = 0; i < selectedObjects.Length; i += 1)
			{
				var mr = selectedObjects[i].GetComponentsInChildren<MeshFilter>();
				selectedMeshes.AddRange(mr);
			}
			canMerge = selectedMeshes.Count >= 1;

			defoult = GUI.contentColor;
			GUI.contentColor = Color.green;
			GUI.Label(new Rect(5, currentHeight, position.width - 10, 16), "Welcome to Mesh Merger, choose the size of chunk");
			GUI.contentColor = defoult;
			currentHeight += 20;

			if (!permittedComponentHide)
			{
				permittedComponentHide = GUI.Toggle(new Rect(5, currentHeight, position.width - 10, 16), permittedComponentHide, "Hide components");
				currentHeight += 20;

				for (int i = 0; i < permittedComponents.Count; i++)
				{
					tmpComponentName = GUI.TextField(new Rect(5, currentHeight, position.width - 10, 16), permittedComponents[i]);
					if (permittedComponents[i] != tmpComponentName)
					{
						permittedComponents[i] = tmpComponentName;
						SaveComponentList(permittedComponents);
					}
					currentHeight += 20;
				}

				if (GUI.Button(new Rect(5, currentHeight, 60, 16), "Add"))
				{
					permittedComponents.Add("");
					SaveComponentList(permittedComponents);
				}
				if (GUI.Button(new Rect(65, currentHeight, 60, 16), "Delete"))
				{
					permittedComponents.RemoveAt(permittedComponents.Count - 1);
					SaveComponentList(permittedComponents);
				}
				currentHeight += 20;
			}
			else
			{
				permittedComponentHide = GUI.Toggle(new Rect(5, currentHeight, position.width - 10, 16), permittedComponentHide, "Show components");
				currentHeight += 20;

			}

			for (int i = 0; i < permittedComponents.Count; i++)
			{
				permittedComponentsType = new List<Type>();
				try
				{
					Type typ;
					typ = Type.GetType(permittedComponents[i]);
					if (typ == null)
						typ = Type.GetType("UnityEngine." + permittedComponents[i] + ", UnityEngine");
					if (typ == null)
						typ = Type.GetType("AssemblyCSharp." + permittedComponents[i]);
					if (typ == null)
					{
						if (permittedComponents[i] == "")
							AddError("Delete empty row");
						else
							AddError(permittedComponents[i] + " not found, mabey add namespace");
						canMerge = false;
					}
					else
						permittedComponentsType.Add(typ);
				}
				catch (Exception m)
				{
					AddError("Cant get type: " + m.Message);
				}
			}

			GUI.Label(new Rect(5, currentHeight, position.width - 10, 16), "Chunk X");

			x_string = GUI.TextField(new Rect(60, currentHeight, 60, 16), x_string);

			currentHeight += 20;

			GUI.Label(new Rect(5, currentHeight, position.width - 10, 16), "Chunk Z");

			z_string = GUI.TextField(new Rect(60, currentHeight, 60, 16), z_string);

			currentHeight += 20;

			try
			{
				x = Int32.Parse(x_string);
				z = Int32.Parse(z_string);
				//currentHeight += 20;
			}
			catch (Exception)
			{
				AddError("Invalide value");
				canMerge = false;
			}

			defoult = GUI.contentColor;
			GUI.contentColor = Color.red;
			GUI.Label(new Rect(5, currentHeight, position.width - 10, 16 * errorLine), errorMessage);
			GUI.contentColor = defoult;
			currentHeight += 14 * errorLine;


			GUI.enabled = canMerge;
			if (GUI.Button(new Rect(5, currentHeight, position.width - 95, 16), "Visualize"))
			{
				MergeMeshesChunks(selectedObjects);
				if (visualizationObject == null)
					if (FindObjectOfType<MeshMergerVisualization>() != null)
						visualizationObject = FindObjectOfType<MeshMergerVisualization>().gameObject;
				if (visualizationObject == null)
				{
					visualizationObject = new GameObject("MeshMerageVisual", typeof(MeshMergerVisualization));
				}
				lastSelected = Selection.objects;
				Selection.activeGameObject = visualizationObject;
				Selection.selectionChanged += SelectionChange;
				timeChange = 0;

			}
			if (GUI.Button(new Rect(position.width - 85, currentHeight, 80, 16), "Swap colour"))
			{
				MeshMergerVisualization.SwapColour();
			}
			currentHeight += 20;
			if (GUI.Button(new Rect(5, currentHeight, position.width - 10, 16), "Merge and Save"))
			{

				string path = GetPathToSave(lastPath);
				if (string.IsNullOrEmpty(path)) return;

				path = FileUtil.GetProjectRelativePath(path);
				lastPath = path.Substring(0, path.LastIndexOf('/') + 1);

				var newMesh = MergeMeshesChunks(selectedObjects);

				path = path.Substring(0, path.LastIndexOf('.'));

				for (int i = 0; i < newMesh.Length; i++)
				{
					SaveMesh(newMesh[i], path + i + ".obj");
				}
			}
			currentHeight += 20;
		}
		else
		{
			selectedObjects = Selection.gameObjects;
			if (lastSelected != null && lastSelected.Length != 0)
			{
				selectedObjects = new GameObject[lastSelected.Length];
				for (int i = 0; i < lastSelected.Length; i++)
				{
					selectedObjects[i] = lastSelected[i] as GameObject;
				}
			}

			selectedMeshes = new List<MeshFilter>();

			for (int i = 0; i < selectedObjects.Length; i += 1)
			{
				var mr = selectedObjects[i].GetComponentsInChildren<MeshFilter>();
				selectedMeshes.AddRange(mr);
			}
			canMerge = selectedMeshes.Count >= 1;

			autoArangeNewMesh = GUI.Toggle(new Rect(5, currentHeight, position.width - 10, 16), autoArangeNewMesh, "Auto arange output mesh to first selected object.");
			currentHeight += 20;

			GUI.enabled = canMerge;

			if (GUI.Button(new Rect(5, currentHeight, position.width - 10, 16), "Merge and Save"))
			{
				var newMesh = MergeMeshes(selectedMeshes);
				if (newMesh != null)
				{
					if (autoArangeNewMesh)
					{
						var go = Selection.gameObjects[0];

						var mf = go.GetComponent<MeshFilter>();
						if (mf == null)
							mf = go.AddComponent<MeshFilter>();

						var mr = go.GetComponent<MeshRenderer>();
						if (mr == null)
							mr = go.AddComponent<MeshRenderer>();

						mf.mesh = newMesh;

						autoArangeNewMesh = false;
					}
				}
			}
			currentHeight += 20;
		}
		GUI.enabled = true;

		if (selectedMeshes.Count < 1)
		{
			EditorGUI.HelpBox(new Rect(5, currentHeight, position.width - 10, 30), "Less than 2 Meshes found at selection. Please select more in order to merge them.", MessageType.Error);
			currentHeight += 20;
		}
		else {
			EditorGUI.HelpBox(new Rect(5, currentHeight, position.width - 10, 30), string.Format("{0} Meshes found at selection.", selectedMeshes.Count), MessageType.Info);
			currentHeight += 20;
		}

		this.Repaint();
	}

	int timeChange = 0;
	private void SelectionChange()
	{
		if (timeChange > 0)
		{
			lastSelected = null;
			Selection.selectionChanged -= SelectionChange;
			timeChange = 0;
		}
		else
			timeChange++;
	}

	StreamWriter sw = null;
	private void SaveComponentList(List<string> permittedComponents)
	{
		string pathToInfo = Path.Combine(Directory.GetParent(Application.dataPath).FullName, "MeshMerger.txt");
		if(sw == null)
			sw = File.CreateText(pathToInfo);
		for (int i = 0; i < permittedComponents.Count; i++)
		{
			sw.WriteLine(permittedComponents[i]);
		}
		sw.Flush();
		sw.Close();
		sw = null;
	}
	static StreamReader sr = null;
	private static void LoadComponentList()
	{
		string pathToInfo = Path.Combine(Directory.GetParent(Application.dataPath).FullName, "MeshMerger.txt");
		if (Directory.Exists(pathToInfo))
		{
			if (sr == null)
				sr = new StreamReader(pathToInfo);
			while (!sr.EndOfStream)
			{
				permittedComponents.Add(sr.ReadLine());
			}
			sr.Close();
			sr = null;
		}
	}

	private void AddError(string v)
	{
		if (!(errorMessage == ""))
			errorMessage += "\n";
		errorMessage += v;
		errorLine++;
	}

	Vector3 centerTemp;
	float radiusTemp;
	int max_xI = 0, max_zI = 0, min_xI = 0, min_zI = 0;
	float max_x, max_z, min_x, min_z;

	class Chunk
	{
		Vector4 vec;
		float max_x, max_z, min_x, min_z;
		List<GameObject> GO = new List<GameObject>();
		public int group
		{ get; private set; }
		public Chunk(float max_x, float min_x, float max_z, float min_z, int group)
		{
			this.max_x = max_x;
			this.max_z = max_z;
			this.min_x = min_x;
			this.min_z = min_z;
			this.group = group;
			vec = new Vector4(max_x, min_x, max_z, min_z);
		}
		public void AddGameObject(GameObject go)
		{
			GO.Add(go);
		}
		public int GetGameObjectCount
		{
			get { return GO.Count; }
		}
		public Vector4 GetVec
		{
			get { return new Vector4(max_x, min_x, max_z, min_z); }
		}
		public GameObject GetGameObjcet(int index)
		{
			return GO[index];
		}
	}

	Mesh[] MergeMeshesChunks(GameObject[] selected)
	{
		List<GameObject> goodGameObject = ObjectWithoutPerrmitedComponent(selected);
		goodGameObject = GetOnlyGood(goodGameObject);
		List<List<GameObject>> ListOfSameMaterial = FindSameMaterial(goodGameObject);
		List<Chunk> chunks = new List<Chunk>();
		List<List<Renderer>> rends;

		MeshMergerVisualization.DeleteChunks();

		Debug.LogError("Find " + goodGameObject.Count);
		rends = new List<List<Renderer>>();
		Vector3 center;
		Vector3 radius;
		Renderer rend;

		for (int i = 0; i < ListOfSameMaterial.Count; i++)
		{
			rends.Add(new List<Renderer>());
			rends[i] = new List<Renderer>();
			for (int j = 0; j < ListOfSameMaterial[i].Count; j++)
			{
				rend = ListOfSameMaterial[i][j].GetComponent<Renderer>();
				rends[i].Add(rend);
			}
		}

		//Zaaczyna się zabawa
		QuickSortList(rends, ListOfSameMaterial, 0, rends.Count - 1);

		for (int i = 0; i < ListOfSameMaterial.Count; i++)
		{

			float lastZ = GetZ(rends[i][0]) - z - 1;//Zabezpieczenie aby w pierszym obiegu pętli stworzyć nowy chunk

			for (int j = 0; j < rends[i].Count; j++)
			{
				if (!(lastZ + z > GetZ(rends[i][j])))
				{
					lastZ = GetZ(rends[i][j]);
					center = rends[i][j].bounds.center;
					radius = rends[i][j].bounds.extents;
					float goMax_x = center.x + x;
					float goMin_x = center.x - x;
					float goMax_z = center.z - radius.z + z;
					float goMin_z = center.z - radius.z;
					chunks.Add(new Chunk(goMax_x, goMin_x, goMax_z, goMin_z, i));
				}
				chunks[chunks.Count - 1].AddGameObject(goodGameObject[j]);
			}
		}
		Dictionary<int, bool> goodChunks = new Dictionary<int, bool>();
		for (int i = 0; i < chunks.Count; i++)
		{
			if (goodChunks.ContainsKey(chunks[i].group))
				goodChunks[chunks[i].group] = true;
			else
				goodChunks.Add(chunks[i].group, false);
		}
		for (int i = chunks.Count - 1; i >= 0; i--)
		{
			if (goodChunks[chunks[i].group] == false)
				chunks.RemoveAt(i);
			else
				//Debug
				MeshMergerVisualization.AddChunk(chunks[i].GetVec, chunks[i].group);
		}
		//Debug
		for (int i = 0; i < ListOfSameMaterial.Count; i++)
		{
			for (int j = 0; j < ListOfSameMaterial[i].Count; j++)
			{
				center = rends[i][j].bounds.center;
				radius = rends[i][j].bounds.extents;
				float goMax_x = center.x + radius.x;
				float goMin_x = center.x - radius.x;
				float goMax_z = center.z + radius.z;
				float goMin_z = center.z - radius.z;
				MeshMergerVisualization.AddItemToChank(goMax_x, goMin_x, goMax_z, goMin_z, i);
			}
		}

		List<Mesh> ret = new List<Mesh>();

		for (int i = 0; i < chunks.Count; i++)
		{
			List<MeshFilter> mf = new List<MeshFilter>();
			for (int j = 0; j < chunks[i].GetGameObjectCount; j++)
			{
				mf.Add(chunks[i].GetGameObjcet(j).GetComponent<MeshFilter>());
			}

			ret.Add(CombineMesh(mf));
		}

		return ret.ToArray();
	}


	private List<List<GameObject>> FindSameMaterial(List<GameObject> goodGameObject)
	{
		List<List<GameObject>> ret = new List<List<GameObject>>();
		List<Renderer> renList = new List<Renderer>();
		for (int i = 0; i < goodGameObject.Count; i++)
		{
			//Co tu sie dzieje....
			Renderer ren = goodGameObject[i].GetComponent<Renderer>();
			bool find = false;
			for (int j = 0; j < renList.Count; j++)
			{
				if(ren.sharedMaterial == renList[j].sharedMaterial)
				{
					ret[j].Add(goodGameObject[i]);
					find = true;
					break;
				}
			}
			if(!find)
			{
				ret.Add(new List<GameObject>());
				ret[renList.Count] = new List<GameObject>();
				ret[renList.Count].Add(goodGameObject[i]);
				renList.Add(ren);
			}
		}
		return ret;
	}
	private void QuickSortList(List<List<Renderer>> rends, List<List<GameObject>> listOfSameMaterial, int left, int right)
	{
		for (int i = 0; i < rends.Count; i++)
		{
			QuickSort(rends[i], listOfSameMaterial[i], 0, (rends[i].Count - 1));
		}
	}

	public static void QuickSort(List<Renderer> rends, List<GameObject> goodGameObject, int left, int right)
	{
		var i = left;
		var j = right;
		var pivot = GetZ(rends[(left + right) / 2]);
		while (i < j)
		{
			while (GetZ(rends[i]) < pivot) i++;
			while (GetZ(rends[j]) > pivot) j--;
			if (i <= j)
			{
				int iTmp = i;
				int jTmp = j;
				// swap
				var tmp = rends[i];
				var tmp2 = goodGameObject[iTmp];
				rends[i++] = rends[j];  // ++ and -- inside array braces for shorter code
				goodGameObject[iTmp++] = goodGameObject[jTmp];
				rends[j--] = tmp;
				goodGameObject[jTmp--] = tmp2;
			}
		}
		if (left < j) QuickSort(rends, goodGameObject, left, j);
		if (i < right) QuickSort(rends, goodGameObject, i, right);
	}

	private static float GetZ(Renderer rend)
	{
		return rend.bounds.center.z - rend.bounds.extents.z;
	}

	private List<GameObject> GetOnlyGood(List<GameObject> goodGameObject)
	{
		List<GameObject> ret = new List<GameObject>();
		Renderer ren;
		MeshFilter meshFilter;
		foreach (var item in goodGameObject)
		{
			ren = item.GetComponent<Renderer>();
			meshFilter = item.GetComponent<MeshFilter>();
			if (item.activeInHierarchy && meshFilter != null && ren != null && ren.enabled && ren.sharedMaterials != null && ren.sharedMaterials.Length == 1 && ren.bounds.size.x < x && ren.bounds.size.z < z)
				ret.Add(item);
		}
		return ret;
	}

	List<GameObject> ObjectWithoutPerrmitedComponent(GameObject[] parent)
	{
		List<GameObject> goodGameObject = new List<GameObject>();
		for (int j = 0; j < parent.Length; j++)
		{
			bool isGood = true;
			for (int i = 0; i < permittedComponentsType.Count; i++)
			{
				if (parent[j].GetComponent(permittedComponentsType[i]) != null)
					isGood = false;
			}
			if (isGood)
			{
				goodGameObject.Add(parent[j]);
				for (int i = 0; i < parent[j].transform.childCount; i++)
				{
					goodGameObject.AddRange(ObjectWithoutPerrmitedComponent(parent[j].transform.GetChild(i).gameObject));
				}
			}
		}
		return goodGameObject;
	}

	List<GameObject> ObjectWithoutPerrmitedComponent(GameObject parent)
	{
		List<GameObject> goodGameObject = new List<GameObject>();
		bool isGood = true;
		for (int i = 0; i < permittedComponentsType.Count; i++)
		{
			if (parent.GetComponent(permittedComponentsType[i]) != null)
				isGood = false;
		}
		if (isGood)
		{
			goodGameObject.Add(parent);
			for (int i = 0; i < parent.transform.childCount; i++)
			{
				goodGameObject.AddRange(ObjectWithoutPerrmitedComponent(parent.transform.GetChild(i).gameObject));
			}
		}
		return goodGameObject;
	}

	Mesh CombineMesh(List<MeshFilter> meshFilters)
	{
		CombineInstance[] combine = new CombineInstance[meshFilters.Count];

		int i = 0;
		while (i < meshFilters.Count)
		{
			combine[i].mesh = meshFilters[i].sharedMesh;
			combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
			i++;
		}

		Mesh ret = new Mesh();
		ret.CombineMeshes(combine);

		return ret;
	}

	Mesh SaveMesh(Mesh mesh, string path)
	{

		ObjExporter.MeshToFile(mesh, path);
		//AssetDatabase.CreateAsset(ret, path);
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();

		return mesh;
	}

	Mesh MergeMeshes(List<MeshFilter> meshFilters)
	{
		string path = GetPathToSave(lastPath);
		if (string.IsNullOrEmpty(path)) return null;

		path = FileUtil.GetProjectRelativePath(path);
		lastPath = path.Substring(0, path.LastIndexOf('/') + 1);

		return SaveMesh(CombineMesh(meshFilters), path);
	}
	string GetPathToSave(string lastpath)
	{
		return EditorUtility.SaveFilePanel("Save Merged Mesh Asset", lastPath, name, "obj");
	}
}

public class ObjExporter
{

	public static string MeshToString(Mesh m)
	{

		StringBuilder sb = new StringBuilder();

		sb.Append("g ").Append(m.name).Append("\n");
		foreach (Vector3 v in m.vertices)
		{
			sb.Append(string.Format("v {0} {1} {2}\n", v.x, v.y, v.z));
		}
		sb.Append("\n");
		foreach (Vector3 v in m.normals)
		{
			sb.Append(string.Format("vn {0} {1} {2}\n", v.x, v.y, v.z));
		}
		sb.Append("\n");
		foreach (Vector3 v in m.uv)
		{
			sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
		}

		for (int material = 0; material < m.subMeshCount; material++)
		{
			sb.Append("\n");
			sb.Append("usemtl ").Append("").Append("\n");
			sb.Append("usemap ").Append("").Append("\n");

			int[] triangles = m.GetTriangles(material);
			for (int i = 0; i < triangles.Length; i += 3)
			{
				sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
					triangles[i] + 1, triangles[i + 1] + 1, triangles[i + 2] + 1));
			}
		}

		return sb.ToString();
	}

	public static void MeshToFile(Mesh m, string filename)
	{
		using (StreamWriter sw = new StreamWriter(filename))
		{
			sw.Write(MeshToString(m));
		}
	}
}
#endif
