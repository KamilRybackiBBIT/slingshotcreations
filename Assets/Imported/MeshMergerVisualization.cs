using System.Collections.Generic;
using UnityEngine;
namespace GameCore.Utils
{
	public class MeshMergerVisualization : MonoBehaviour
	{

		struct FourPoint
		{
			public FourPoint(float max_x, float min_x, float max_z, float min_z, int group)
			{
				this.max_x = max_x;
				this.min_x = min_x;
				this.max_z = max_z;
				this.min_z = min_z;
				this.group = group;
				if (group > maxGroup)
					maxGroup = group;
			}
			public float max_x;
			public float min_x;
			public float max_z;
			public float min_z;
			public int group;
			public static int maxGroup;
		}

		static List<FourPoint> chunks = new List<FourPoint>();
		static List<FourPoint> items = new List<FourPoint>();
		public static void DeleteChunks()
		{
			chunks = new List<FourPoint>();
			items = new List<FourPoint>();
		}

		public static void AddItemToChank(float max_x, float min_x, float max_z, float min_z, int group)
		{
			items.Add(new FourPoint(max_x, min_x, max_z, min_z, group));
		}
		public static void AddChunk(float max_x, float min_x, float max_z, float min_z, int group)
		{
			chunks.Add(new FourPoint(max_x, min_x, max_z, min_z, group));
		}
		public static void AddChunk(Vector4 vec, int group)
		{
			chunks.Add(new FourPoint(vec.x, vec.y, vec.z, vec.w, group));
		}
		public void Awake()
		{
			Destroy(gameObject);
		}

		private static Vector3 _colorVector2 = new Vector2(1,0);
		GUIStyle myStyle = new GUIStyle();
		private static int textColor = 1;

		void OnDrawGizmosSelected()
		{
			Gizmos.color = UnityEngine.Color.green;
			myStyle.fontSize = 15;
			myStyle.normal.textColor = new Color(textColor, textColor, textColor);

			for (int i = 0; i < chunks.Count; i++)
			{
				float absX = Mathf.Abs(chunks[i].max_x - chunks[i].min_x);
				float absZ = Mathf.Abs(chunks[i].max_z - chunks[i].min_z);
				Vector3 max = new Vector3(chunks[i].max_x, 0, chunks[i].max_z);
				Vector3 max_shift = new Vector3(chunks[i].max_x, 0, chunks[i].max_z + 3);
				Vector3 min = new Vector3(chunks[i].min_x, 0, chunks[i].min_z);
				Vector3 center = Vector3.Lerp(max, min, 0.5f);
				Gizmos.color = new Color(Mathf.Abs( _colorVector2.x - (1f / FourPoint.maxGroup) * chunks[i].group), 0, Mathf.Abs( _colorVector2.y -  (1f / FourPoint.maxGroup) * chunks[i].group));
				Gizmos.DrawWireCube(center, new Vector3(absX, 1, absZ));
#if UNITY_EDITOR
				UnityEditor.Handles.Label(max, "Chunk " + i, myStyle);
				UnityEditor.Handles.Label(max_shift, "Group " + chunks[i].group, myStyle);
#endif

			}
			for (int i = 0; i < items.Count; i++)
			{
				float absX = Mathf.Abs(items[i].max_x - items[i].min_x);
				float absZ = Mathf.Abs(items[i].max_z - items[i].min_z);
				Vector3 max = new Vector3(items[i].max_x, 0, items[i].max_z);
				Vector3 min = new Vector3(items[i].min_x, 0, items[i].min_z);
				Vector3 center = Vector3.Lerp(max, min, 0.5f);
				Gizmos.color = new Color(Mathf.Abs( _colorVector2.y - (1f / FourPoint.maxGroup) * items[i].group), 0.5f, Mathf.Abs( _colorVector2.x - (1f / FourPoint.maxGroup) * items[i].group));
				Gizmos.DrawWireCube(center, new Vector3(absX, 1, absZ));
				//Gizmos.DrawWireCube(Vector3.zero, new Vector3(40, 40, 40));

			}
		}

		public static void SwapColour()
		{
			_colorVector2 = new Vector2(_colorVector2.y, _colorVector2.x);
			if (textColor == 1)
				textColor = 0;
			else
				textColor = 1;
		}
	}
}
