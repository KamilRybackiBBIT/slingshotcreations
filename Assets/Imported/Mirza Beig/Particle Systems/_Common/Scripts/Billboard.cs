﻿
// =================================	
// Namespaces.
// =================================

using UnityEngine;

// =================================	
// Define namespace.
// =================================

namespace MirzaBeig
{

    namespace ParticleSystems
    {

        // =================================	
        // Classes.
        // =================================

        public class Billboard : MonoBehaviour
        {
            public bool lockY;

            // =================================	
            // Nested classes and structures.
            // =================================

            // ...

            // =================================	
            // Variables.
            // =================================

            private static Transform cameraTransform;
            // ...

            // =================================	
            // Functions.
            // =================================

            // ...

            private void Start()
            {
                if (cameraTransform == null)
                {
                    cameraTransform = Camera.main.transform;
                }
            }

            void LateUpdate()
            {
                if (lockY)
                {
                    var camPos = cameraTransform.position;
                    camPos.y = transform.position.y;
                    transform.LookAt(camPos);
                }
                else
                {
                    transform.LookAt(cameraTransform.position);
                }
            }

            // Disable the behaviour when it becomes invisible...
            void OnBecameInvisible()
            {
                enabled = false;
            }

            // ...and enable it again when it becomes visible.
            void OnBecameVisible()
            {
                enabled = true;
            }

            // =================================	
            // End functions.
            // =================================

        }

        // =================================	
        // End namespace.
        // =================================

    }

}

// =================================	
// --END-- //
// =================================
