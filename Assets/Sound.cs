using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    [HideInInspector] public bool Advanced;
    [HideInInspector] public bool Direct;
    public enum type
    {
        other,
        audio,
        xd
    }

    [HideInInspector] public type myType;

    public void Play()
    {
        //Audio.Play();
    }

    public void Stop()
    {
        //Audio.Stop();
    }

    [Header("AdvancedSettings")]
    [HideInInspector] public bool PlayOnAwake = false;
    [HideInInspector] public bool Loop = false;
    [HideInInspector] [Range(-3f, 3f)] public float Pitch;
    [HideInInspector] [Range(0f, 1f)] public float Place2D3D;
    [HideInInspector] public float DistanceMin3D;
    [HideInInspector] public float DistanceMax3D;

    

    [Header("Info")]
    //in
    [HideInInspector] public string Category;
    [HideInInspector] public string CategoryDisplay;
    [HideInInspector] public string SoundName;
    [HideInInspector] public string SoundHolderID;
    [HideInInspector] public SoundHolder MainSound;
    [HideInInspector] public AudioSource MyAudio;

    //out and help for current selection
    [HideInInspector] public int categoryIndex;
    [HideInInspector] public int soundIndex;

    //bekaa
}
