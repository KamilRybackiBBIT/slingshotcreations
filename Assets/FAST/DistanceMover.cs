using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class DistanceMover : MonoBehaviour
{
    [Header("SetThis")]
    
    [Header("Moving")]
    [SerializeField] private Transform movingTransform;
    [SerializeField] private Transform start;
    [SerializeField] private Transform finish;
    [SerializeField] private float movingDistancePercent;
    [SerializeField] private bool movingClampedPercent;
    
    [Header("ToMove")]
    [SerializeField] private Transform from;
    [SerializeField] private Transform to;
    [SerializeField] private Transform transformToMove;
    [SerializeField] private float moveDistancePercent;
    [SerializeField] private bool moveClampPercent;

    [Header("AddictionalSettings")] 
    [SerializeField] private bool xMoving;
    [SerializeField] private bool yMoving;
    [SerializeField] private bool zMoving = true;
    [SerializeField] [Range(0.05f,5f)]
    private float outputMovePercentageMultiplier = 1f;
    
    [Header("Info")]
    
    [DisableEdit] [SerializeField] private Vector3 fromMovingFinal = new Vector3(0f, 0f, 0f);
    [DisableEdit] [SerializeField] private Vector3 toMovingFinal = new Vector3(0f, 0f, 0f);
    [DisableEdit] [SerializeField] private float startDifference;
    [DisableEdit] [SerializeField] private float updateDifference;
    
    //temp
    
    
    private void Start()
    {

        if (xMoving)
        {
            fromMovingFinal.x = start.position.x;
            toMovingFinal.x = finish.position.x;
        }
        
        if (yMoving)
        {
            fromMovingFinal.y = start.position.y;
            toMovingFinal.y = finish.position.y;
        }
        
        if (zMoving)
        {
            fromMovingFinal.z = start.position.z;
            toMovingFinal.z = finish.position.z;
        }

        startDifference = fromMovingFinal.magnitude - toMovingFinal.magnitude;

    }

    private void FixedUpdate()
    {
        updateDifference = movingTransform.position.magnitude - toMovingFinal.magnitude;
        movingDistancePercent = FST.map(startDifference, 0f, 0f, 1f, updateDifference);

        moveDistancePercent = movingDistancePercent * outputMovePercentageMultiplier;
        transformToMove.position = Vector3.Lerp(from.position, to.position, moveDistancePercent);

        //temp
        if (movingDistancePercent > 1)
        {
            
        }
    }
    
    //temp
}
