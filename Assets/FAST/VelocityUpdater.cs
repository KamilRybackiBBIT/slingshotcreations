using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityUpdater : MonoBehaviour
{
    public GameObject rb;
    [Range(0f, 50f)] public float lerpValue;

    [SerializeField] FST.Updates MyUpdate;
    
    private void Update()
    {
        if (MyUpdate == FST.Updates.Update )
        {
            transform.position = Vector3.Lerp(transform.position, rb.gameObject.transform.position, lerpValue * Time.deltaTime);
        }
        
    }

    private void FixedUpdate()
    {
        if (MyUpdate == FST.Updates.FixedUpdate)
        {
            transform.position = Vector3.Lerp(transform.position, rb.gameObject.transform.position, lerpValue * Time.deltaTime);
        }
    }

    private void LateUpdate()
    {
        if (MyUpdate == FST.Updates.LateUpdate)
        {
            transform.position = Vector3.Lerp(transform.position, rb.gameObject.transform.position, lerpValue * Time.deltaTime);
        }
    }
}
