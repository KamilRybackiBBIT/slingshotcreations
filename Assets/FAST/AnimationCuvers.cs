﻿using UnityEngine;

public static class AnimationCurves
{
    public static float BiggestValue(this AnimationCurve curve)
    {
        float biggest = curve[0].value;

        for (int i = 0; i < curve.length; i++)
        {
            if (curve[i].value > biggest)
            {
                biggest = curve[i].value;
            }
        }

        return biggest;
    }

    public static float LowestValue(this AnimationCurve curve)
    {
        float lowest = curve[0].value;

        for (int i = 0; i < curve.length; i++)
        {
            if (curve[i].value < lowest)
            {
                lowest = curve[i].value;
            }
        }

        return lowest;
    }

    public static int LowestValueKey(this AnimationCurve curve)
    {
        float lowest = curve[0].value;
        int lowestKey = 0;

        for (int i = 0; i < curve.length; i++)
        {
            if (curve[i].value < lowest)
            {
                lowest = curve[i].value;
                lowestKey = i;
            }
        }

        return lowestKey;
    }

    public static int BiggestValueKey(this AnimationCurve curve)
    {
        float biggest = curve[0].value;
        int biggestKey = 0;

        for (int i = 1; i < curve.length; i++)
        {
            var value = curve[i].value;
            if (value > biggest)
            {
                biggest = value;
                biggestKey = i;
            }
        }

        return biggestKey;
    }

    public static float BiggestValueTime(this AnimationCurve curve)
    {
        float biggest = curve[0].value;
        int biggestKey = 0;

        for (int i = 0; i < curve.length; i++)
        {

            if (curve[i].value > biggest)
            {
                biggest = curve[i].value;
                biggestKey = i;
            }
        }

        return curve[biggestKey].time;
    }

    public static float AverageValue(this AnimationCurve curve)
    {
        Debug.Log("This is not 100% accurate");
        return (BiggestValue(curve) + LowestValue(curve)) / 2;
    }

    public enum whatBoolIwant
    {
        isValueBiggerThanKeyValue,
        isValueSmallerThanKeyValue,
        isTimeFurtherThanKeyTime,
        isTimeCloserThanKeyTime
           
    }

    public static bool isValueBiggerThanKey(this AnimationCurve curve, int key, float currentValue)
    {
        if (key > curve.length - 1)
        {
            Debug.LogError("Wrong Key");
            return false;
        }

        float keyValue = curve.Evaluate(curve[key].time);
        float keyTime = curve[key].time;
        if (currentValue > keyValue)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public static bool isValueSmallerThanKey(this AnimationCurve curve, int key, float currentValue)
    {
        if (key > curve.length - 1)
        {
            Debug.LogError("Wrong Key");
            return false;
        }

        float keyValue = curve.Evaluate(curve[key].time);
        if (currentValue < keyValue)
        {
            return true;
        }

        else
        {
            return false;
        }
    }


    public static bool isTimeFurtherThanKey(this AnimationCurve curve, int key, float currentTime)
    {
        if (key > curve.length - 1)
        {
            Debug.LogError("Wrong Key");
            return false;
        }

        float keyTime = curve[key].time;
        if (currentTime > keyTime)
        {
            return true;
        }

        else
        {
            return false;
        }

    }

}
