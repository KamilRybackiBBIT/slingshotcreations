﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(FstAnimator))]
public class FstAnimatorInspector : Editor
{
    public FstAnimator script;
    private void OnEnable()
    {
        script = (FstAnimator)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        /*
        if (script == null) script = (CustomVehicleLoader)target;
        if (script.vehicles == null) return;
        if (script.selectedAsset == null) getAsset();
        if (script.selectedPrefab == null) getPrefab();
 

        //car select
        EditorGUILayout.BeginHorizontal();
        script.carIndex = EditorGUILayout.IntSlider(script.carIndex, 0, script.vehicles.elements.Count - 1);
        EditorGUILayout.LabelField("Car: " + "Selected car: " + script.vehicles.elements[script.carIndex].name);
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("SelectCar"))
        {
            SetCar();
        }

        if (script.BodyPartsCombiner == null)
        {
            getBodyPartsCombiner();
        }
        
        if (script.WheelsPartsCombiner == null)
        {
            getWheelPartsCombiner();
        }

        if (script.rimsPainter == null)
        {
            getWheelPainter();
        }

        if(script.windowPainter == null)
        {
            getWindowPainter();
        }

        //body customization
        for (int i = 0; i < script.possibleCustomizations.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(script.BodyPartsCombiner.parts[i].name + ": " + script.possibleCustomizations[i].parts.elements[script.possibleCustomizationIndex[i]].name);
            script.possibleCustomizationIndex[i] = EditorGUILayout.IntSlider(script.possibleCustomizationIndex[i], 0, script.possibleCustomizations[i].parts.elements.Count - 1);
            EditorGUILayout.EndHorizontal();
        }

        //customizations parts array



        //body color
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("BODY COLOR: " + script.selectedPrefab.GetComponent<VehiclePainter>().paints.elements[script.colorIndex].name);
        script.colorIndex = EditorGUILayout.IntSlider(script.colorIndex, 0, script.selectedPrefab.GetComponent<VehiclePainter>().paints.elements.Count - 1);
        EditorGUILayout.EndHorizontal();



        for (int i = 0; i < script.possibleWheelCustomizations.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(script.WheelsPartsCombiner.parts[i].name + " " + script.possibleWheelCustomizations[i].parts.elements[script.possibleWheelsCustomizationIndex[i]].name);
            script.possibleWheelsCustomizationIndex[i] = EditorGUILayout.IntSlider(script.possibleWheelsCustomizationIndex[i], 0, script.possibleWheelCustomizations[i].parts.elements.Count - 1);
            EditorGUILayout.EndHorizontal();
        }


        //customizations wheels parts array



        //wheels color
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Wheel Color: " + script.rimsPainter.paints.elements[script.rimsColorIndex].name);
        script.rimsColorIndex = EditorGUILayout.IntSlider(script.rimsColorIndex, 0, script.rimsPainter.paints.elements.Count - 1);
        EditorGUILayout.EndHorizontal();

        //window color
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Window Color: " + script.windowPainter.paints.elements[script.windowColorIndex].name);
        script.windowColorIndex = EditorGUILayout.IntSlider(script.windowColorIndex, 0, script.windowPainter.paints.elements.Count - 1);
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Save"))
        {
            Save();
        }

    }

    public void Save()
    {
        EditorUtility.SetDirty(script);
    }

    public void SetCar()
    {
        //variables
        getAsset();
        getPrefab();
        getBodyPartsCombiner();
        getBodyPainter();
        getWheelPartsCombiner();
        getWheelPainter();
        getWindowPainter();

        //arrays
        setBodyCustomizations();
        setWheelCustomizations();

        //zero variables
        script.colorIndex = 0;
        script.rimsColorIndex = 0;
        script.windowColorIndex = 0;
    }

    void getAsset() { script.selectedAsset = script.vehicles.elements[script.carIndex].reference; }

    void getPrefab() { script.selectedPrefab = (GameObject)script.selectedAsset.editorAsset; }

    void getBodyPartsCombiner() { script.BodyPartsCombiner = script.selectedPrefab.GetComponent<PartsCombiner>(); }

    void getBodyPainter() { script.bodyPainter = script.selectedPrefab.GetComponent<VehiclePainter>(); }

    void getWheelPartsCombiner() { script.WheelsPartsCombiner = script.selectedPrefab.GetComponentInChildren<WheelsPartsCombiner>(); }

    void getWheelPainter() { script.rimsPainter = script.selectedPrefab.GetComponentInChildren<RimsVehiclePainter>(); }

    void getWindowPainter() { script.windowPainter = script.selectedPrefab.GetComponentInChildren<WindowsVehiclePainter>(); }

    void setBodyCustomizations()
    {
        script.possibleCustomizations = script.BodyPartsCombiner.parts;
        script.possibleCustomizationIndex = new int[script.BodyPartsCombiner.parts.Count];
    }

    void setWheelCustomizations()
    {
        script.possibleWheelCustomizations = script.WheelsPartsCombiner.parts;
        script.possibleWheelsCustomizationIndex = new int[script.WheelsPartsCombiner.parts.Count];
    }
        */
    }
}
