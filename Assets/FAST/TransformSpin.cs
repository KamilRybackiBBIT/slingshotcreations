using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class TransformSpin : MonoBehaviour
{
    public float Speed;

    public FST.Spaces Space;
    public FST.Updates Style;

    public Vector3 dir;
    
    void Update()
    {
        if (Style == FST.Updates.Update)
        {
            Spin();
        }
    }

    private void FixedUpdate()
    {
        if (Style == FST.Updates.FixedUpdate)
        {
            Spin();
        }
    }

    private void LateUpdate()
    {
        if (Style == FST.Updates.LateUpdate)
        {
            Spin();
        }
    }

    void Spin()
    {
        if (Space == FST.Spaces.Local)
        {
            transform.localRotation *= quaternion.Euler(dir * Speed * Time.deltaTime);
        }

        else
        {
            transform.rotation *= quaternion.Euler(dir * Speed * Time.deltaTime);
        }
    }
    
    public void SlowDownFunction(float sec)
    {
        StartCoroutine(SlowDown(sec));
    }
    
    public IEnumerator SlowDown(float Seconds)
    {
        float timeElapsed = 0f;
        float startSpeed = Speed;
        while (timeElapsed < Seconds)
        {
            timeElapsed += Time.deltaTime;
            float percent = FST.map(0f, Seconds, 0f, 1f, timeElapsed);
            Speed = Mathf.Lerp(startSpeed, 0f, percent);
            yield return 0;
        }
    }
    
    
}
