using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    using UnityEngine;
    using UnityEditor;
    /// <summary>
    /// Drawer class for DisableEditAttribute 
    /// </summary>
    [CustomPropertyDrawer(typeof(DisableEditAttribute))]
    public class DisableEditAttributeDrawer : PropertyDrawer
    {
        /// <summary>
        /// This method extends stadard drawer by modification of GUI.enabled
        /// </summary>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var guiEnabled = GUI.enabled;
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label);
            GUI.enabled = guiEnabled;
        }
    }

