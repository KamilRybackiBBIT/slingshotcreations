using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reparent : MonoBehaviour
{
    [SerializeField] private Transform[] transformsToReparent;

    [SerializeField] private Transform newParent;

    public void SetParent()
    {
        foreach (var t in transformsToReparent)
        {
            t.parent = newParent;
        }
    }
    
}
