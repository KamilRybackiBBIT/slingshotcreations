using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider),typeof(Rigidbody))]
public class PhysicEventGameObject : MonoBehaviour
{
    [Header("SetThis")] 
    [SerializeField] private UnityEvent MainEvent;
    [SerializeField] private GameObject[] objects;
    
    [Header("Settings")] 
    [SerializeField] private FST.PhysicDetection Mode;
    [SerializeField] private bool TriggerOnce = false;
    [DisableEdit] private bool triggered = false;
    
    
    private void OnTriggerEnter(Collider other)
    {
        if (IsAnyRecord(other.gameObject) && Mode == FST.PhysicDetection.Trigger)
        {
            Action();
        }
 
    }

    private void OnCollisionEnter(Collision other)
    {
        if (IsAnyRecord(other.gameObject) && Mode == FST.PhysicDetection.Collision)
        {
            Action();
        }
        
    }

    private void OnValidate()
    {
        if (GetComponent<Collider>())
        {
            var coll = GetComponent<Collider>();
            
            if (Mode == FST.PhysicDetection.Trigger)
            {
                coll.isTrigger = true;
            }
            
            if (Mode == FST.PhysicDetection.Collision)
            {
                coll.isTrigger = false;
            }
        }

        else
        {
            Debug.Log("No Collider, Adding Some PlaceholderOne");
            var coll = gameObject.AddComponent<BoxCollider>();
        }

        if (GetComponent<Rigidbody>())
        {
            var rb = GetComponent<Rigidbody>();
            
            if (Mode == FST.PhysicDetection.Trigger)
            {
                rb.isKinematic = true;
            }
            
            if (Mode == FST.PhysicDetection.Collision)
            {
                rb.isKinematic = false;
            }
        }

        else
        {
            Debug.Log("No Rigidbody, Adding Own");
            var rb = gameObject.AddComponent<Rigidbody>();
        }
    }
    
    void Action()
    {
        if (!triggered && TriggerOnce)
        {
            return;
        }
        else
        {
            MainEvent?.Invoke();
            triggered = true;
        }
    }
    bool IsAnyRecord(GameObject coll)
    {
        foreach (var obj in objects)
        {
            if (coll == obj)
            {
                return true;
            }
        }
        return false;
    }


}
