using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class TimedEvent : MonoBehaviour
{
    [Header("On Enable it will start counting")]
    public float afterEnableTimeToEvent;
    public UnityEvent timedEvent;

    private void OnEnable()
    {
        StartCoroutine(timer());
    }

    public IEnumerator timer()
    {
        float timeElapsed = 0;
        float timeToElapse = afterEnableTimeToEvent;
        while (timeElapsed < timeToElapse)
        {
            timeElapsed += Time.deltaTime;
            yield return 0;
        }
        timedEvent?.Invoke();
    }
    
}