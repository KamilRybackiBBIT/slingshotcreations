using UnityEngine;
using UnityEngine.Events;

public class OnClickEvent : MonoBehaviour
{
    public UnityEvent clickEvent;
    public enum ClickType
    {
        Click,
        Continous
    }

    public ClickType inputType;
    public string eventKeyCode;
    
    void Update()
    {
        if (inputType == ClickType.Click)
        {
            if (Input.GetKeyDown(eventKeyCode))
            {
                clickEvent?.Invoke();;
            }
        }

        else if(inputType == ClickType.Continous)
        {
            if (Input.GetKey(eventKeyCode))
            {
                clickEvent?.Invoke();;
            }
        }
    }
}
