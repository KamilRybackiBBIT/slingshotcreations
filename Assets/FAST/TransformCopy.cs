﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;

public class TransformCopy : MonoBehaviour
{
    

    [Header("Set This")]
    [SerializeField] Transform constrained;
    [SerializeField] Transform copy;
    [SerializeField] private FST.Updates updateType;
    
    [Header("Position")]
    [SerializeField] private bool position;
    [SerializeField] private FST.Spaces posSpace;
    [SerializeField] private FST.Spaces posOffsetSpace;
    [SerializeField] private Vector3 posOffset;
    
    [Header("Rotation - Offset Not Working Perfectly, Keep That in mind")]
    [SerializeField] private bool rotation;
    [SerializeField] private FST.Spaces rotSpace;
    [SerializeField] private FST.Spaces rotOffsetSpace;
    [SerializeField] private Vector3 rotOffset;
    
    [Header("Scale - Not Working")]
    [SerializeField] private bool scale;
    [SerializeField] private FST.Spaces scaleSpace;
    [SerializeField] private FST.Spaces scaleOffsetSpace;
    [SerializeField] private Vector3 scaleOffset;


    private void Start()
    {
        if (constrained == null)
        {
            constrained = this.transform;
        }
    }

    void Update()
    {
        if (updateType == FST.Updates.Update)
        {
            Copy();
        }
    }

    private void FixedUpdate()
    {
        if (updateType == FST.Updates.FixedUpdate)
        {
            Copy();
        }
    }

    private void LateUpdate()
    {
        if (updateType == FST.Updates.LateUpdate)
        {
            Copy();
        }
    }

    [ContextMenu("Copy")]
    public void Copy()
    {
        if (position)
        {
            
            if (posSpace == FST.Spaces.World)
            {
                constrained.position = copy.transform.position;
            }
            
            if (posSpace == FST.Spaces.Local)
            {
                constrained.position = copy.transform.localPosition;
            }

            if (posOffsetSpace == FST.Spaces.World)
            {
                constrained.position += posOffset;
            }
            
            if (posOffsetSpace == FST.Spaces.Local)
            {
                constrained.position = copy.TransformPoint(posOffset);
            }

        }

        if (rotation)
        {
            if (rotSpace == FST.Spaces.World)
            {
                constrained.rotation = copy.transform.rotation;
            }
            
            if (rotSpace == FST.Spaces.Local)
            {
                constrained.rotation = copy.transform.localRotation;
            }

            if (rotOffsetSpace == FST.Spaces.World)
            {
                constrained.Rotate(Vector3.right,rotOffset.x);
                constrained.Rotate(Vector3.up,rotOffset.y);
                constrained.Rotate(Vector3.forward,rotOffset.z);// constrained.rotation *= quaternion.Euler(rotOffset/360f);
            }
            
            if (rotOffsetSpace == FST.Spaces.Local)
            {
                constrained.Rotate(transform.right,rotOffset.x);
                constrained.Rotate(transform.up,rotOffset.y);
                constrained.Rotate(transform.forward,rotOffset.z);
            }
        }
        
        
    }

    

    
    
}
