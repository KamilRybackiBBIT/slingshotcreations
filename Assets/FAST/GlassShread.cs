﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GlassShread : MonoBehaviour
{
    [SerializeField] private FST.PhysicDetection detection;

    

    public Transform partsPool;
    public Transform ShreadFrom;
    public GameObject[] glassParts;

    public string TagToTrigger;
    public float Force;
    public ForceMode MyMode;

    public UnityEvent OnShread;
    bool shreaded = false;

    [ContextMenu("GetGlassParts")]
    public void GetGlassParts()
    {
        glassParts = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            glassParts[i] = transform.GetChild(i).gameObject;
        }
    }

    [ContextMenu("AddRbs")]
    public void AddRbs()
    {
        for (int i = 0; i < transform.childCount; i++)
        {

            for (int d = 0; d < glassParts[i].GetComponents<Rigidbody>().Length; d++)
            {
                Destroy(glassParts[i].GetComponents<Rigidbody>()[d]);
            }

            Rigidbody rb = null;
            glassParts[i].AddComponent<Rigidbody>();
            rb = glassParts[i].GetComponent<Rigidbody>();
            rb.isKinematic = true;
        }
    }

    [ContextMenu("Delete All Rbs")]
    public void DeleteAllRbs()
    {
        for (int i = 0; i < transform.childCount; i++)
        {

            for (int d = 0; d < glassParts[i].GetComponents<Rigidbody>().Length; d++)
            {
                DestroyImmediate(glassParts[i].GetComponents<Rigidbody>()[d]);
            }
        }
    }

    [ContextMenu("AddBoxColliders")]
    public void AddBoxColliders()
    {
        for (int i = 0; i < transform.childCount; i++)
        {

            glassParts[i].AddComponent<BoxCollider>();

        }
    }

    [ContextMenu("BoxCollidersDisable")]
    public void BoxCollidersDisable()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            glassParts[i].GetComponent<BoxCollider>().enabled = false;
        }
    }

    [ContextMenu("DeleteBoxColliders")]
    public void DeleteBoxColliders()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            DestroyImmediate(glassParts[i].GetComponent<BoxCollider>());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (detection != FST.PhysicDetection.Trigger) return;

        if (shreaded) return;
        if (other.GetComponentInParent<GlassShreader>() == true)
        {
            GlassBreak();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (detection != FST.PhysicDetection.Collision) return;
        if (shreaded) return;

        if (collision.gameObject.GetComponentInParent<GlassShreader>() == true)
        {
            GlassBreak();
           
        }

    }

    public void GlassBreak()
    {
        Vector3 playerPos = ShreadFrom.position;


        for (int i = glassParts.Length - 1; i >= 0; i--)
        {
            Transform glassPart = glassParts[i].transform;
            glassPart.parent = partsPool;

            Rigidbody rb = glassPart.gameObject.GetComponent<Rigidbody>();
            glassPart.gameObject.GetComponent<Collider>().enabled = true;
            rb.isKinematic = false;
            rb.AddForce((glassPart.position -
                playerPos).normalized * Force * Time.deltaTime, MyMode);
        }
        shreaded = true;
        OnShread?.Invoke();
    }

    public void Debbug()
    {
        if (GetComponent<BoxCollider>().isTrigger)
        {
            Debug.Log("Glass Trigger");
        }
        else
        {
            Debug.Log("Glass Collision");
        }
    }


}
