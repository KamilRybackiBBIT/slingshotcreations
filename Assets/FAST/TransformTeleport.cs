using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformTeleport : MonoBehaviour
{
    private Quaternion startRot;
    private void Start()
    {
        startRot = transform.rotation;
    }

    public void Teleport(Vector3 worldPlace)
    {
        transform.position = worldPlace;
    }

    public void ResetRot()
    {
        transform.rotation = Quaternion.identity;
    }

    public void SetStartRot()
    {
        transform.rotation = startRot;
    }
    
    
}
