﻿using UnityEngine;

public class TransformLerp : MonoBehaviour
{

    [Header("Set This")]
    [SerializeField] Transform constrained;
    [SerializeField] Transform from;
    [SerializeField] Transform to;
    [Range(0f,1f)] [SerializeField] float percent;

    [Header("Optional")]
    [SerializeField] private bool position = true;
    [SerializeField] private bool rotation;
    [SerializeField] private bool scale;
    
    [Header("DeltaTime Lerp")]
    private FST.Updates upd;
    [SerializeField] bool timeLerped = false;
    [SerializeField] float timeFactor = 1f;
    
    private void Start()
    {
        if (constrained == null)
        {
            constrained = this.transform;
        }
    }

    private void Update()
    {
        if (upd == FST.Updates.Update)
        {
            Lerp();
        }
    }

    private void FixedUpdate()
    {
        if (upd == FST.Updates.FixedUpdate)
        {
            Lerp();
        }
    }

    private void LateUpdate()
    {
        if (upd == FST.Updates.LateUpdate)
        {
            Lerp();
        }
    }

    private void Lerp()
    {
        if (!timeLerped)
        {
            if (position)
            {
                constrained.position = Vector3.Lerp(from.position, to.position, percent);
            }

            if (rotation)
            {
                constrained.rotation = Quaternion.Lerp(from.rotation,to.rotation,percent);
            }
        
            if (scale)
            {
                constrained.rotation = Quaternion.Lerp(from.rotation,to.rotation,percent);
            }
        }

        else if(timeLerped)
        {
            if (position)
            {
                constrained.position = Vector3.Lerp(from.position, to.position, percent * Time.deltaTime * timeFactor);
            }

            if (rotation)
            {
                constrained.rotation = Quaternion.Lerp(from.rotation,to.rotation,percent * Time.deltaTime * timeFactor);
            }
        
            if (scale)
            {
                constrained.rotation = Quaternion.Lerp(from.rotation,to.rotation,percent * Time.deltaTime * timeFactor);
            }
        }
        
        
    }
}
