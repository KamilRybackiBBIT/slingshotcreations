﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public static class FST
{
    
    public static float map(float fromMin, float fromMax, float toMin, float toMax, float valueFrom)
    {
        return (valueFrom - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
    }

    public static float zeroDivide(float x, float y)
    {
        if (x == 0 || y == 0) return 0;
        else return x / y;
    }

    public enum Updates
    {
        Update,
        FixedUpdate,
        LateUpdate
    }
    
    public enum Smoothings
    {
        Raw,
        Lerp,
        Slerp
    }
    
    public enum Spaces
    {
        World,
        Local
    }
    
    public enum PhysicDetection
    {
        Collision,
        Trigger
    }
    
    public static bool IsInLayerMask(this GameObject obj, LayerMask mask)
    {
        return mask == (mask | (1 << obj.layer));
    }
    
}

