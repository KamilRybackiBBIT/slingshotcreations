using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SimpleAudio
{
    public static void playIfNotPlaying(this AudioSource clip)
    {
        if (!clip.isPlaying)
        {
            clip.Play();
        }
    }
}
