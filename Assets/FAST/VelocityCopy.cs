using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityCopy : MonoBehaviour
{
    [SerializeField] Rigidbody Constrained;
    [SerializeField] Rigidbody Copy;

    [Header("Optional")]
    [SerializeField] float Multuiplier = 1f;
    [SerializeField] float AddforceMultiplier = 1f;

    void Update()
    {
        Constrained.velocity = Copy.velocity * Multuiplier;
    }

    public void SetMultiplier(float value) {
        Multuiplier = value;
    }

    private void OnDisable()
    {
        Constrained.AddForce(Copy.velocity * AddforceMultiplier, ForceMode.VelocityChange);
    }
}
