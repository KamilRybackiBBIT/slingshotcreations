using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformScaler : MonoBehaviour
{
   [DisableEdit] [SerializeField] private Vector3 startScale;

   public bool fromZero;
    private void Start()
    {
        startScale = transform.localScale;
        if (fromZero)
        {
            transform.localScale = Vector3.zero;
        }
        
        
    }

    public void ScaleFromZero(float sec)
    {
        StartCoroutine(ScaleFromZeroEnumerator(sec));
    }
    
    public void ScaleZero(float sec)
    {
        StartCoroutine(ScaleZeroEnumerator(sec));
    }
    
    public IEnumerator ScaleZeroEnumerator(float Seconds)
    {
        float timeElapsed = 0f;
        float startMultiplier = 1f;
        while (timeElapsed < Seconds)
        {
            timeElapsed += Time.deltaTime;
            float percent = FST.map(0f, Seconds, 1f, 0f, timeElapsed);
            transform.localScale = startScale * percent;
            yield return 0;
        }
    }
    
    public IEnumerator ScaleFromZeroEnumerator(float Seconds)
    {
        float timeElapsed = 0f;
        while (timeElapsed < Seconds)
        {
            timeElapsed += Time.deltaTime;
            float percent = FST.map(0f, Seconds, 0f, 1f, timeElapsed);
            transform.localScale = startScale * percent;
            Debug.Log("XDDDD");
            yield return 0;
        }
    }
}
