﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Offset : MonoBehaviour
{
    public bool IhaveParent;
    public Transform offsetFromThis;
    public Vector3 offset;
    public enum UpdateType
    {
        Update,
        FixedUpdate,
        LateUpdate
    }

    public UpdateType MyUpdate;




    void Update()
    {
        if (MyUpdate == UpdateType.Update)
        {
            OffsetMe();
        }
    }

    private void FixedUpdate()
    {
        if (MyUpdate == UpdateType.FixedUpdate)
        {
            OffsetMe();
        }
    }

    private void LateUpdate()
    {
        if (MyUpdate == UpdateType.LateUpdate)
        {
            OffsetMe();
        }
    }

    void OffsetMe()
    {
        if (IhaveParent)
        {
            transform.localPosition = offset;
        }

        else
        {
            transform.position = offsetFromThis.position + offset;
        }
            
    } 
}
