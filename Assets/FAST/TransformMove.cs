﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformMove : MonoBehaviour
{
    public Vector3 Dir;
    public float Speed;

    [SerializeField] private FST.Updates updateType;

    [SerializeField] private FST.Spaces space;
    
    private void Update()
    {
        if (updateType == FST.Updates.Update)
        {
            UpdatePos();
        }
        
    }

    private void FixedUpdate()
    {
        if (updateType == FST.Updates.FixedUpdate)
        {
            UpdatePos();
        }
    }

    private void LateUpdate()
    {
        if (updateType == FST.Updates.LateUpdate)
        {
            UpdatePos();
        }
    }

    void UpdatePos()
    {
        if (space == FST.Spaces.Local)
        {
            transform.localPosition += Dir.normalized * Speed * Time.deltaTime;
        }
        
        else
        {
            if (GetComponent<Rigidbody>())
            {
                GetComponent<Rigidbody>().position += Dir.normalized * Speed * Time.deltaTime;
            }
            else
            {
                transform.position += Dir.normalized * Speed * Time.deltaTime;
            }
            
            
        }
    }

    public void SetSpeed(float newSpeed)
    {
        Speed = newSpeed;
    }

    public void SlowDownFunction(float sec)
    {
        StartCoroutine(SlowDown(sec));
    }
    
    public IEnumerator SlowDown(float Seconds)
    {
        float timeElapsed = 0f;
        float startSpeed = Speed;
        while (timeElapsed < Seconds)
        {
            timeElapsed += Time.deltaTime;
            float percent = FST.map(0f, Seconds, 0f, 1f, timeElapsed);
            Speed = Mathf.Lerp(startSpeed, 0f, percent);
            yield return 0;
        }
    }
}
