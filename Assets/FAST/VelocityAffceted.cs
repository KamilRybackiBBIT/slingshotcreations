using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class VelocityAffceted : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private Rigidbody rigidbody;
	[SerializeField] public float MaxVelocityDistanceX = 0.5f;
	[SerializeField] public float MaxVelocityDistanceY = 0.5f;
	[SerializeField] public float MaxVelocityDistanceZ = 0.5f;


	[Header("AddictionalSettings")]

	[SerializeField] private float finalSmoothingFactor = 1f;
	[Space]
	[SerializeField] private bool invertX = false;
	[SerializeField] private bool invertY = false;
	[SerializeField] private bool invertZ = false;
	[Space]
	[SerializeField] [Range(0f, 10f)] private float multiplierX = 1f;
	[SerializeField] [Range(0f, 100f)] private float multiplierY = 1f;
	[SerializeField] [Range(0f, 10f)] private float multiplierZ = 1f;
	[Space]
	
	[SerializeField] [Range(0f, -5f)] private float MinDistanceX = -0.5f;
	[SerializeField] [Range(0f, 5f)] private float MaxDistanceX = 0.5f;
	[SerializeField] [Range(0f, -5f)] private float MinDistanceY = -0.5f;
	[SerializeField] [Range(0f, 5f)] private float MaxDistanceY = 0.5f;
	[SerializeField] [Range(0f, -5f)] private float MinDistanceZ = -0.5f;
	[SerializeField] [Range(0f, 5f)] private float MaxDistanceZ = 0.5f;
	public enum VelocityDistanceCalculation
	{
		Lerp,
		Raw
	}
	[SerializeField] FST.Updates myUpdate = FST.Updates.FixedUpdate;
	[SerializeField] FST.Smoothings Smoothing = FST.Smoothings.Slerp;

	[Space]

	[Header("Admin Options")]
	public bool FakeVelocity;

	[SerializeField] [Range(-2f, 2f)] float fakeVelocityDistanceX = 0f;
	[SerializeField] [Range(-2f, 2f)] float fakeVelocityDistanceY = 0f;
	[SerializeField] [Range(-2f, 2f)] float fakeVelocityDistanceZ = 0f;




	[Header("Info")]

	[DisableEdit] [SerializeField] private Vector3 worldVelocity;
	[DisableEdit] [SerializeField] private Vector3 relativLocalVelocity;

	[Header("INFO X")]
	[SerializeField] [Range(0f, 1f)] private float VelocityDistancePercentX;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityFinalX;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityRawX;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityLerpX;
	[DisableEdit] [SerializeField] float lastFrameLocalVelocityX;
	[DisableEdit] [SerializeField] private float speedX;


	[Header("INFO Y")]
	[SerializeField] [Range(0f, 1f)] private float VelocityDistancePercentY;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityFinalY;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityRawY;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityLerpY;
	[DisableEdit] [SerializeField] float lastFrameLocalVelocityY;
	[DisableEdit] [SerializeField] private float speedY;


	[Header("InfoZ")]
	[SerializeField] [Range(0f, 1f)] private float VelocityDistancePercentZ;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityFinalZ;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityRawZ;
	[DisableEdit] [SerializeField] private float DistanceToLastFrameVelocityLerpZ;
	[DisableEdit] [SerializeField] float lastFrameLocalVelocityZ;
	[DisableEdit] [SerializeField] private float speedZ;


	[Header("Info Rest")]
	[DisableEdit] [SerializeField] private Vector3 worldVelocityDirection;
	[DisableEdit] [SerializeField] private Vector3 localVelocityDirection;
	[DisableEdit] [SerializeField] private float speed;
	[DisableEdit] [SerializeField] private Vector3 startPos;

	[Header("Settings To Delete In Future")]
	[SerializeField] VelocityDistanceCalculation FinalVelocityDistance = VelocityDistanceCalculation.Lerp;
	[SerializeField] float DistanceToLastFrameVelocityLerpFactor = 1f; //does not affect raw
	[SerializeField] bool clampToMaxDistance = true;




	private void Start()
	{
		startPos = transform.localPosition;
	}

	//lokalne velocity z ostatniej klatki, 3 zmienne procentowe

	private void Update()
	{
		if (myUpdate == FST.Updates.Update)
		{
			Affect();
		}
	}
	private void FixedUpdate()
	{
		if (myUpdate == FST.Updates.FixedUpdate)
		{
			Affect();
		}
	}
	private void LateUpdate()
	{
		if (myUpdate == FST.Updates.LateUpdate)
		{
			Affect();
		}
	}

	

	
	
	

	private void Affect()
	{
		

        if (FakeVelocity)
        {
			GetFakeVelocityVariables();
			GetFakeVelocityDistance();

			relativLocalVelocity.x = Mathf.Clamp01(relativLocalVelocity.x * MaxDistanceX);
			relativLocalVelocity.y = Mathf.Clamp01(relativLocalVelocity.y * MaxDistanceY);
			relativLocalVelocity.z = Mathf.Clamp01(relativLocalVelocity.z * MaxDistanceZ);

			localVelocityDirection.x = Mathf.Clamp(localVelocityDirection.x, -MaxVelocityDistanceX, MaxVelocityDistanceX);
			localVelocityDirection.y = Mathf.Clamp(localVelocityDirection.y, -MaxVelocityDistanceY, MaxVelocityDistanceY);
			localVelocityDirection.z = Mathf.Clamp(localVelocityDirection.z, -MaxVelocityDistanceZ, MaxVelocityDistanceZ);

			//fakeVelocityDistanceX = Mathf.Clamp(fakeVelocityDistanceX, -fakeVelocityDistanceX * MaxVelocityDistanceX, fakeVelocityDistanceX * MaxVelocityDistanceX);
			//fakeVelocityDistanceY = Mathf.Clamp(fakeVelocityDistanceY, -fakeVelocityDistanceY * MaxDistanceY, fakeVelocityDistanceY * MaxDistanceY);
			//fakeVelocityDistanceZ = Mathf.Clamp(fakeVelocityDistanceZ, -fakeVelocityDistanceZ * MaxDistanceZ, fakeVelocityDistanceZ * MaxDistanceZ);
		}

        else
        {
			GetVelocityVariables();
			GetVelocityDistance();
			
		}

		Vector3 FinalPosition = new Vector3(
			FST.map(-1f, 1f, MinDistanceX, MaxDistanceX,  VelocityDistancePercentX * localVelocityDirection.x),
			FST.map(-1f, 1f, MinDistanceY, MaxDistanceY,  VelocityDistancePercentY * localVelocityDirection.y),
			FST.map(-1f, 1f, MinDistanceZ, MaxDistanceZ,  VelocityDistancePercentZ * localVelocityDirection.z));



		/*
		Vector3 FinalPosition = new Vector3(
			localVelocityDirection.x * VelocityDistancePercentX,
			localVelocityDirection.y * VelocityDistancePercentY,
			localVelocityDirection.z * VelocityDistancePercentZ);
		*/

		//FinalPosition.x *= multiplierX;
		//FinalPosition.y *= multiplierY;
		//FinalPosition.z *= multiplierZ;


		if (invertX) FinalPosition.x *= -1f;
		if (invertY) FinalPosition.y *= -1f;
		if (invertZ) FinalPosition.z *= -1f;

		/*
		//jezeli chcesz uzywac limitow
		FinalPosition.x = Mathf.Clamp(FinalPosition.x, MinDistanceX, MaxDistanceX);
		FinalPosition.y = Mathf.Clamp(FinalPosition.y, MinDistanceY, MaxDistanceY);
		FinalPosition.z = Mathf.Clamp(FinalPosition.z, MinDistanceZ, MaxDistanceZ);
		*/
		
		if (Smoothing == FST.Smoothings.Raw)
		{
			transform.localPosition = startPos + FinalPosition;
		}

		else if (Smoothing == FST.Smoothings.Lerp)
		{
			transform.localPosition = Vector3.Lerp(
				transform.localPosition,
				startPos + FinalPosition,
				Time.deltaTime * finalSmoothingFactor);
		}

		else if (Smoothing == FST.Smoothings.Slerp)
		{
			transform.localPosition = Vector3.Slerp(
				transform.localPosition,
				startPos + FinalPosition,
				Time.deltaTime * finalSmoothingFactor);
		}

		lastFrameLocalVelocityX = relativLocalVelocity.x;
		lastFrameLocalVelocityY = relativLocalVelocity.y;
		lastFrameLocalVelocityZ = relativLocalVelocity.z;
	}

	private void GetVelocityVariables()
	{
		worldVelocityDirection = rigidbody.velocity.normalized;
		localVelocityDirection = transform.InverseTransformDirection(worldVelocityDirection);

		worldVelocity = rigidbody.velocity;
		relativLocalVelocity = transform.InverseTransformDirection(worldVelocity);

		speed = rigidbody.velocity.magnitude;
	}

	private void GetFakeVelocityVariables()
	{
		//worldVelocityDirection = rigidbody.velocity.normalized;
		localVelocityDirection = new Vector3(fakeVelocityDistanceX, fakeVelocityDistanceY, fakeVelocityDistanceZ);

		worldVelocity = rigidbody.velocity;
		relativLocalVelocity = Vector3.zero;

		speed = rigidbody.velocity.magnitude;
	}

	private void GetVelocityDistance()
	{
		//TOTAL SHIT TO DELETE IN FUTURE == RAW
		DistanceToLastFrameVelocityRawX = Mathf.Abs(lastFrameLocalVelocityX - relativLocalVelocity.x);
		DistanceToLastFrameVelocityRawY = Mathf.Abs(lastFrameLocalVelocityY - relativLocalVelocity.y);
		DistanceToLastFrameVelocityRawZ = Mathf.Abs(lastFrameLocalVelocityZ - relativLocalVelocity.z);

		DistanceToLastFrameVelocityLerpX = Mathf.Lerp(
			DistanceToLastFrameVelocityLerpX,
			DistanceToLastFrameVelocityRawX,
			DistanceToLastFrameVelocityLerpFactor * Time.deltaTime);

		DistanceToLastFrameVelocityLerpY = Mathf.Lerp(
	DistanceToLastFrameVelocityLerpY,
	DistanceToLastFrameVelocityRawY,
	DistanceToLastFrameVelocityLerpFactor * Time.deltaTime);

		DistanceToLastFrameVelocityLerpZ = Mathf.Lerp(
	DistanceToLastFrameVelocityLerpZ,
	DistanceToLastFrameVelocityRawZ,
	DistanceToLastFrameVelocityLerpFactor * Time.deltaTime);

		DistanceToLastFrameVelocityFinalX = FinalVelocityDistance == VelocityDistanceCalculation.Lerp
			? DistanceToLastFrameVelocityLerpX
			: DistanceToLastFrameVelocityRawX;

		DistanceToLastFrameVelocityFinalY = FinalVelocityDistance == VelocityDistanceCalculation.Lerp
	? DistanceToLastFrameVelocityLerpY
	: DistanceToLastFrameVelocityRawY;

		DistanceToLastFrameVelocityFinalZ = FinalVelocityDistance == VelocityDistanceCalculation.Lerp
	? DistanceToLastFrameVelocityLerpZ
	: DistanceToLastFrameVelocityRawZ;

		VelocityDistancePercentX = FST.map(
			0f,
			MaxVelocityDistanceX,
			0f,
			1f,
			DistanceToLastFrameVelocityFinalX);

		VelocityDistancePercentY = FST.map(
			0f,
			MaxVelocityDistanceY,
			0f,
			1f,
			DistanceToLastFrameVelocityFinalY);

		VelocityDistancePercentZ = FST.map(
			0f,
			MaxVelocityDistanceZ,
			0f,
			1f,
			DistanceToLastFrameVelocityFinalZ);


		if (clampToMaxDistance)
		{
			VelocityDistancePercentX = Mathf.Clamp(VelocityDistancePercentX, 0f, 1f);
			VelocityDistancePercentY = Mathf.Clamp(VelocityDistancePercentY, 0f, 1f);
			VelocityDistancePercentZ = Mathf.Clamp(VelocityDistancePercentZ, 0f, 1f);
		}

	}

	private void GetFakeVelocityDistance()
	{
		//TOTAL SHIT TO DELETE IN FUTURE == RAW
		DistanceToLastFrameVelocityRawX = Mathf.Abs(lastFrameLocalVelocityX - fakeVelocityDistanceX);
		DistanceToLastFrameVelocityRawY = Mathf.Abs(lastFrameLocalVelocityY - fakeVelocityDistanceY);
		DistanceToLastFrameVelocityRawZ = Mathf.Abs(lastFrameLocalVelocityZ - fakeVelocityDistanceZ);

		DistanceToLastFrameVelocityLerpX = Mathf.Lerp(
			DistanceToLastFrameVelocityLerpX,
			DistanceToLastFrameVelocityRawX,
			DistanceToLastFrameVelocityLerpFactor * Time.deltaTime);

		DistanceToLastFrameVelocityLerpY = Mathf.Lerp(
	DistanceToLastFrameVelocityLerpY,
	DistanceToLastFrameVelocityRawY,
	DistanceToLastFrameVelocityLerpFactor * Time.deltaTime);

		DistanceToLastFrameVelocityLerpZ = Mathf.Lerp(
	DistanceToLastFrameVelocityLerpZ,
	DistanceToLastFrameVelocityRawZ,
	DistanceToLastFrameVelocityLerpFactor * Time.deltaTime);

		DistanceToLastFrameVelocityFinalX = FinalVelocityDistance == VelocityDistanceCalculation.Lerp
			? DistanceToLastFrameVelocityLerpX
			: DistanceToLastFrameVelocityRawX;

		DistanceToLastFrameVelocityFinalY = FinalVelocityDistance == VelocityDistanceCalculation.Lerp
	? DistanceToLastFrameVelocityLerpY
	: DistanceToLastFrameVelocityRawY;

		DistanceToLastFrameVelocityFinalZ = FinalVelocityDistance == VelocityDistanceCalculation.Lerp
	? DistanceToLastFrameVelocityLerpZ
	: DistanceToLastFrameVelocityRawZ;

		VelocityDistancePercentX = FST.map(
			0f,
			MaxVelocityDistanceX,
			0,
			1f,
			DistanceToLastFrameVelocityFinalX);

		VelocityDistancePercentY = FST.map(
			0f,
			MaxVelocityDistanceY,
			0,
			1f,
			DistanceToLastFrameVelocityFinalY);

		VelocityDistancePercentZ = FST.map(
			0f,
			MaxVelocityDistanceZ,
			0,
			1f,
			DistanceToLastFrameVelocityFinalZ);


		if (clampToMaxDistance)
		{
			VelocityDistancePercentX = Mathf.Clamp(VelocityDistancePercentX, 0f, 1f);
			VelocityDistancePercentY = Mathf.Clamp(VelocityDistancePercentY, 0f, 1f);
			VelocityDistancePercentZ = Mathf.Clamp(VelocityDistancePercentZ, 0f, 1f);
		}
	}
}

