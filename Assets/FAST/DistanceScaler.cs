using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceScaler : MonoBehaviour
{
    [SerializeField] private AnimationCurve scaler;

    [SerializeField] private Transform moving;
    [SerializeField] private Transform target;
    
    
    [DisableEdit] [SerializeField] private float maxDistance;
    [DisableEdit] [SerializeField] private float currentDistance;

    [DisableEdit] [SerializeField] private Vector3 startScale;
    
    void Start()
    {
        startScale = transform.localScale;
        setMaxDistance();
    }
    
    void Update()
    {
        currentDistance = Vector3.Distance(moving.position, target.position);
        setScale();
    }

    private void OnValidate()
    {
        setMaxDistance();
    }

    void setMaxDistance()
    {
        maxDistance = Vector3.Distance(moving.position, target.position);
    }

    void setScale()
    {
        transform.localScale = startScale * scaler.Evaluate(currentDistance);
    }
    
    
}
