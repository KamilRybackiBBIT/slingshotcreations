using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CreateAssetMenu(menuName = "Audio/ManagerEditorOnlyOnce/manager")]
public class AudioMangerEditor : ScriptableObject
{

    [Range(0f, 1f)] public float validatorIksde;

    public string[] AudioFolders;
    public string[] AudioFoldersDisplay;
    public List<AudioFolder> AudioCategories;

    [HideInInspector] public SoundHolder[] Holders;
    [HideInInspector] public string Ids;

    public SoundHolder[] AllHolders;
    public string[] AllIDs;

    private static AudioMangerEditor instance;
    public static AudioMangerEditor Instance
    {
        get
        {
            if (instance == null)
            {
                instance = LoadFromResources();
            }

            return instance;
        }
    }

    private const string FileName = "AudioManager";

    private const string ConfigPath = "" + FileName;

    private static AudioMangerEditor LoadFromResources()
    {
        var resource = Resources.Load(ConfigPath) as AudioMangerEditor;
        return resource;
    }


    private void OnEnable()
    {
        EditorUtility.SetDirty(this);
    }

    private void OnValidate()
    {
    }

    public void AssignVariables()
    {
        //variables
        AudioFolders = AssetDatabase.GetSubFolders("Assets/Resources/Audio");
        AudioFoldersDisplay = new string[AudioFolders.Length];

        //decoration
        for (int i = 0; i < AudioFolders.Length; i++)
        {
            AudioFoldersDisplay[i] = AudioFolders[i].Substring(23);
        }

        //Setting Categories
        AudioCategories = new List<AudioFolder>();
        for (int i = 0; i < AudioFolders.Length; i++)
        {
            AudioCategories.Add(new AudioFolder(AudioFoldersDisplay[i]));
        }

        //filling Categories With SoundHolders
        for (int i = 0; i < AudioCategories.Capacity - 1; i++)
        {
            AudioCategories[i].sounds = Resources.LoadAll<SoundHolder>("Audio/" + AudioFoldersDisplay[i]);
        }

        //filling big array
        AllHolders = Resources.LoadAll<SoundHolder>("Audio");


        Debug.Log("Assigning");
    }

    public void AssignIds()
    {
        AllIDs = new string[AllHolders.Length];
        for (int i = 0; i < AllHolders.Length; i++)
        {
            var holder = AllHolders[i];
            AllIDs[i] = holder.ID;
        }
    }

    public void CheckForIdDuplication()
    {
        for (int i = 0; i < AllHolders.Length; i++)
        {
            for (int d = 0; d < AllHolders.Length; d++)
            {
                if (i != d)
                {
                    if (AllHolders[i].ID == AllHolders[d].ID)
                    {
                        AllHolders[i].CreateID();
                    }
                }

                else if (AllHolders[i].ID == "")
                {
                    AllHolders[i].CreateID();
                }
            }
        }

        AssignIds();
    }

    [ContextMenu("ReloadAllIds")]
    public void ReloadAllIds()
    {
        for (int i = 0; i < AllHolders.Length; i++)
        {
             AllHolders[i].CreateID();
        }
        AssignIds();
    }

    [CustomEditor(typeof(AudioMangerEditor))]
    public class AudioMangerEditorInspector : Editor
    {
        public AudioMangerEditor script;
        private void OnEnable()
        {
            script = (AudioMangerEditor)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Get Arrays And Sounds"))
            {
                script.AssignVariables();   
            }

            if (GUILayout.Button("AssignIds"))
            {
                script.AssignIds();
            }

            if (GUILayout.Button("CheckForDuplicates"))
            {
                script.CheckForIdDuplication();
            }
        }

    }
        [System.Serializable]
    public class AudioFolder
    {
        [HideInInspector] public string name; //cool looking in inspector
        public SoundHolder[] sounds;

        public AudioFolder(string _name)
        {
            name = _name;
        }
    }
}
