using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Coin : MonoBehaviour
{
    public GameObject Mesh;
    public ParticleSystem onTrig;
    public GameObject whatCanTrigger;
    public UnityEvent OnTriggerEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == whatCanTrigger)
        {
            onTrig.Play();
            Mesh.SetActive(false);
            OnTriggerEvent?.Invoke();
        }
    }

}
