using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Sound))]
public class SoundInspector : Editor
{
    public Sound script;
    public AudioMangerEditor manager;


    int categoryIndex = 0;
    int lastFrameCategoryIndex;
    int soundIndex = 0;
    bool showInfo;
    string[] soundsOptions;


    private void OnEnable()
    {
        script = (Sound)target;
        manager = AudioMangerEditor.Instance;

        Debug.Log("OnEnable");

        if (!script.Direct)
        {
            //for good loking set the category indexes, to show properly

            int tempCategoryIndex = 0;
            int tempSoundIndex = 0;


            for (int i = 0; i < manager.AudioCategories.Count; i++)
            {
                for (int d = 0; d < manager.AudioCategories[i].sounds.Length; d++)
                {
                    if (manager.AudioCategories[i].sounds[d] == script.MainSound)
                    {
                        tempCategoryIndex = i;
                        tempSoundIndex = d;
                    }
                }
            }



            categoryIndex = tempCategoryIndex;
            soundIndex = tempSoundIndex;
        }
    }


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        EditorGUILayout.BeginHorizontal();

        SerializedProperty IsDirect = serializedObject.FindProperty("Direct");
        serializedObject.Update();
        EditorGUILayout.PropertyField(IsDirect);
        serializedObject.ApplyModifiedProperties();
        SerializedProperty IsAdvanced = serializedObject.FindProperty("Advanced");
        serializedObject.Update();
        EditorGUILayout.PropertyField(IsAdvanced);
        serializedObject.ApplyModifiedProperties();
        
        

        EditorGUILayout.EndHorizontal();


        if (script.Direct)
        {
            SerializedProperty direct = serializedObject.FindProperty("MainSound");
            serializedObject.Update();
            EditorGUILayout.PropertyField(direct);
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save"))
            {
                Debug.Log("Save Direct");
                SaveDirect();
            }
        }
        else
        {
            EditorGUILayout.BeginHorizontal();

            //category
            GUILayout.Label("Sound Category: ");
            categoryIndex = EditorGUILayout.Popup(categoryIndex, manager.AudioFoldersDisplay);


            //bake sound options
            soundsOptions = new string[manager.AudioCategories[categoryIndex].sounds.Length];
            for (int i = 0; i < manager.AudioCategories[categoryIndex].sounds.Length; i++)
            {
                soundsOptions[i] = manager.AudioCategories[categoryIndex].sounds[i].ToString();
            }

            //sound
            GUILayout.Label("Sound: ");
            soundIndex = EditorGUILayout.Popup(soundIndex, soundsOptions);
            EditorGUILayout.EndHorizontal();


            if (GUILayout.Button("Save"))
            {
                Debug.Log("Save non direct");
                SaveNonDirect();
            }

            
            

        }

        if (script.Advanced)
        {
            SerializedProperty PlayOnAwake = serializedObject.FindProperty("Category");
            EditorGUILayout.PropertyField(PlayOnAwake);

            SerializedProperty Loop = serializedObject.FindProperty("Category");
            EditorGUILayout.PropertyField(Loop);

            SerializedProperty Pitch = serializedObject.FindProperty("Category");
            EditorGUILayout.PropertyField(Pitch);

            SerializedProperty Place2D3D = serializedObject.FindProperty("Category");
            EditorGUILayout.PropertyField(Place2D3D);

            SerializedProperty DistanceMin3D = serializedObject.FindProperty("Category");
            EditorGUILayout.PropertyField(DistanceMin3D);

            SerializedProperty DistanceMax3D = serializedObject.FindProperty("Category");
            EditorGUILayout.PropertyField(DistanceMax3D);

        }

        showInfo = EditorGUILayout.Foldout(showInfo, "");
        if (showInfo)
        {
            SerializedProperty Category = serializedObject.FindProperty("Category");
            EditorGUILayout.PropertyField(Category);

            SerializedProperty CategoryDisplay = serializedObject.FindProperty("CategoryDisplay");
            EditorGUILayout.PropertyField(CategoryDisplay);

            SerializedProperty SoundName = serializedObject.FindProperty("SoundName");
            EditorGUILayout.PropertyField(SoundName);

            SerializedProperty SoundHolderID = serializedObject.FindProperty("SoundHolderID");
            EditorGUILayout.PropertyField(SoundHolderID);

            SerializedProperty MainSound = serializedObject.FindProperty("MainSound");
            EditorGUILayout.PropertyField(MainSound);

            SerializedProperty MyAudio = serializedObject.FindProperty("MyAudio");
            EditorGUILayout.PropertyField(MyAudio);

            serializedObject.Update();
            serializedObject.ApplyModifiedProperties();

        }

        void SaveNonDirect()
        {
            script.Category = manager.AudioFolders[categoryIndex];
            script.CategoryDisplay = manager.AudioFoldersDisplay[categoryIndex];

            script.SoundName = manager.AudioCategories[categoryIndex].sounds[soundIndex].name;
            script.SoundHolderID = manager.AudioCategories[categoryIndex].sounds[soundIndex].ID;
            script.MainSound = manager.AudioCategories[categoryIndex].sounds[soundIndex];
        }
        void SaveDirect()
        {
            int tempCategoryIndex = 0;


            for (int i = 0; i < manager.AudioCategories.Count; i++)
            {
                for (int d = 0; d < manager.AudioCategories[i].sounds.Length; d++)
                {
                    if (manager.AudioCategories[i].sounds[d] == script.MainSound)
                    {
                        tempCategoryIndex = i;
                    }
                }
            }


            script.categoryIndex = tempCategoryIndex;
            script.Category = manager.AudioFolders[tempCategoryIndex];
            script.CategoryDisplay = manager.AudioFoldersDisplay[tempCategoryIndex];

            script.SoundName = script.MainSound.name;
            script.SoundHolderID = script.MainSound.ID;

        }

        EditorUtility.SetDirty(this);
    }
}
