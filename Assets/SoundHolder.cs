using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Audio/New Sound")]
public class SoundHolder : ScriptableObject
{
    public AudioClip Clip;
    [Range(0f, 1f)] public float Volume = 1f;

    [DisableEdit][SerializeField] public string ID;

    public void CreateID()
    {
        ID = "";
        var newId = System.Guid.NewGuid();
        ID = newId.ToString();

        EditorUtility.SetDirty(this);
    }
}
