using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MifPickup : MonoBehaviour
{
   public planeController pc;
   public GameObject wings;
   public GameObject localWings;

   private void OnTriggerEnter(Collider other)
   {
       pc.forwardRezistance = -4f;
       pc.upRezistance = 150f;
       pc.sideRezistance = 15f;
       StartCoroutine(strechWings());
   }

   private IEnumerator strechWings()
   {
       float timeElapsed = 0;
       float timeToElapse = 1f;
       while (timeElapsed < timeToElapse)
       {
           timeElapsed += Time.deltaTime;
           wings.transform.localScale = Vector3.Lerp(wings.transform.localScale, new Vector3(3.5f,0.08f,0.45f), Time.deltaTime);
           localWings.transform.localScale = Vector3.Lerp(wings.transform.localScale, new Vector3(0f,0.0f,0.0f), Time.deltaTime * 100f);
           yield return 0;
       }
        
   }
}

