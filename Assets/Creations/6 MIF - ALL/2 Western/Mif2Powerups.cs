using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mif2Powerups : MonoBehaviour
{
    public GameObject wingsGo;
    public GameObject wheelsGo;
    public GameObject engineGo;

    [SerializeField] private WheelCollider[] wheelsxd;
    
    public string inputWings, inputWheels, inputEngine;
    public bool toggleWings, toggleWheels, toggleEngine;

    [SerializeField] private planeController controller;
    [SerializeField] private float startForward, startUp, startSide, startSpeed;

    public bool igonreThisInput;

    [SerializeField] private TrailRenderer[] engineTrails;
    private void Awake()
    {
        startForward = controller.forwardRezistance;
        startUp = controller.upRezistance;
        startSide = controller.sideRezistance;
        startSpeed = controller.boostSpeed;

        wheelsxd = transform.GetComponentsInChildren<WheelCollider>();
        
        Wings();
        Wheels();
        Engine();
    }

    private void Update()
    {
        if (igonreThisInput)
        {
            return;
        }
        
        if (Input.GetKeyDown(inputWings))
        {
            Wings();
        }
        
        if (Input.GetKeyDown(inputWheels))
        {
            Wheels();
        }
        
        if (Input.GetKeyDown(inputEngine))
        {
            Engine();
        }
    }

    public void Wings()
    {
        toggleWings = !toggleWings;
        if (toggleWings == false)
        {
            wingsGo.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            wingsGo.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
            wingsGo.transform.GetChild(0).GetComponentInChildren<TrailRenderer>().enabled = false;
            wingsGo.transform.GetChild(1).GetComponentInChildren<TrailRenderer>().enabled = false;
            controller.forwardRezistance = 0f;
            controller.upRezistance = 0f;
            controller.sideRezistance = 0f;

        }

        else
        {
            wingsGo.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            wingsGo.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
            wingsGo.transform.GetChild(0).GetComponentInChildren<TrailRenderer>().enabled = true;
            wingsGo.transform.GetChild(1).GetComponentInChildren<TrailRenderer>().enabled = true;
            controller.forwardRezistance =startForward;
            controller.upRezistance = startUp;
            controller.sideRezistance = startSide;
        }
    }
    
    public void Wheels()
    {
        toggleWheels = !toggleWheels;
        foreach (var wheel in wheelsxd)
        {
            wheel.gameObject.GetComponent<MeshRenderer>().enabled = toggleWheels;
        }
        
    }
    
    public void Engine()
    {
        toggleEngine = !toggleEngine;
        if (toggleEngine == false)
        {
            engineGo.GetComponent<MeshRenderer>().enabled = false;
            engineGo.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            controller.boostSpeed = 0;

            for (int i = 0; i < engineTrails.Length; i++)
            {
                engineTrails[i].enabled = false;
            }
            
        }

        else
        {
            engineGo.GetComponent<MeshRenderer>().enabled = true;
            engineGo.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            controller.boostSpeed = startSpeed;
            
            for (int i = 0; i < engineTrails.Length; i++)
            {
                engineTrails[i].enabled = true;
            }
        }
    }
}
