using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = System.Random;

public class PlaneFireManager : MonoBehaviour
{
    public MeshRenderer[] CardboardParts;
    public MeshRenderer[] MetalParts;
    public List<Transform> partsToDetatch;
    public Transform rootCardboard;
    public Material cardboard, metal;
    public ParticleSystem[] fireParticles;
    
    
    public bool isMetal;
    public bool isFire;
    
    [SerializeField] bool coroutineStarted;
    [SerializeField] bool blackingStarted;

    public UnityEvent onUpgrade;
    
    private void Start()
    {

        foreach (var part in fireParticles)
        {
            part.Stop();
        }
        
        AllMetalPartsEnable(false);
        cardboard.color = Color.white;
        
        foreach (var part in CardboardParts)
        {
            if (part.transform != rootCardboard.transform)
            {
                partsToDetatch.Add(part.transform);
            }
            
        }
        
    }

    public void StopAllCoroutinesProperly()
    {
        isFire = false;
    }

    private void Update()
    {
        if (isFire && !coroutineStarted)
        {
           DetatchingFunction(0.3f);
           BlackingFunction(5f);
           FireParticlesPlay();
        }
        
        
        
    }
    
    

    public void FireParticlesPlay()
    {
        foreach (var part in fireParticles)
        {
            part.Play();
        }
    }

    public float percentUpgraded = 0;
    
    public void CardboardPartsEnable(bool state)
    {

        for (int i = 0; i < CardboardParts.Length; i++)
        {
            float IndexPercent = FST.map(0f, CardboardParts.Length, 0f, 1f, i);
            
            Debug.Log(IndexPercent);
            
            if (IndexPercent>=percentUpgraded && IndexPercent <= percentUpgraded + 0.25f)
            {           
                Debug.Log(IndexPercent);
                var renderer = CardboardParts[i];
                renderer.enabled = state;
            
                if (renderer.GetComponentInChildren<TrailRenderer>())
                {
                    foreach (var trail in renderer.GetComponentsInChildren<TrailRenderer>())
                    {
                        trail.enabled = state;
                    }
                }
                
            }
            

        }
        
    }
    
    
    public void MetalPartsEnable(bool state)
    {
        for (int i = 0; i < MetalParts.Length; i++)
        {
            float IndexPercent = FST.map(0f, MetalParts.Length, 0f, 1f, i);
            
            Debug.Log(IndexPercent);
            
            if (IndexPercent>=percentUpgraded && IndexPercent <= percentUpgraded + 0.25f)
            {           
                Debug.Log(IndexPercent);
                var renderer = MetalParts[i];
                renderer.enabled = state;
            
                if (renderer.GetComponentInChildren<TrailRenderer>())
                {
                    foreach (var trail in renderer.GetComponentsInChildren<TrailRenderer>())
                    {
                        trail.enabled = state;
                    }
                }
                
            }
            

        }
            

    
    }

    public void AllMetalPartsEnable(bool state)
    {
        foreach (var renderer in MetalParts)
        {
            renderer.enabled = state;
            
            if (renderer.GetComponentInChildren<TrailRenderer>())
            {
                foreach (var trail in renderer.GetComponentsInChildren<TrailRenderer>())
                {
                    trail.enabled = state;
                }
            }
        


        }
    }
    
    
    public void DetatchingFunction(float interval)
    {
        StartCoroutine(Detatching(interval));
    }
    
    public IEnumerator Detatching(float intervalToBreak)
    {
        coroutineStarted = true;
        float timeElapsed = 0f;

        while (partsToDetatch.Count != 0)
        {
            timeElapsed += Time.deltaTime;
            while (timeElapsed > intervalToBreak)
            {
                
                if (partsToDetatch.Count == 0)
                {
                    yield break;
                }
                
                timeElapsed = 0;
                partsToDetatch[0].parent = null;
                if (partsToDetatch[0].GetComponent<Collider>() == false)
                {
                    partsToDetatch[0].gameObject.AddComponent<BoxCollider>();
                }
                
                if (partsToDetatch[0].GetComponent<Rigidbody>() == false)
                {
                    partsToDetatch[0].gameObject.AddComponent<Rigidbody>();
                }
                
                partsToDetatch.Remove(partsToDetatch[0]);
                //yield return 0;
                
            }
            
           
            yield return 0;
        }
        coroutineStarted = false;
    }
    
    public void BlackingFunction(float sec)
    {
        StartCoroutine(Blacking(sec));
    }
    
    public IEnumerator Blacking(float Seconds)
    {
        blackingStarted = true;
        float timeElapsed = 0f;

        var startColor = cardboard.color;
        
        while (timeElapsed < Seconds)
        {
            timeElapsed += Time.deltaTime;
            
            float percent = FST.map(0f, Seconds, 0f, 1f, timeElapsed);
            cardboard.color = Color.Lerp(startColor, Color.black, percent);
            
            yield return 0;
        }
        blackingStarted = false;
    }

    public void Upgrade()
    {
        CardboardPartsEnable(false);
        MetalPartsEnable(true);
        isMetal = true;
        percentUpgraded += 0.25f;
        onUpgrade?.Invoke();
    }
    
}
