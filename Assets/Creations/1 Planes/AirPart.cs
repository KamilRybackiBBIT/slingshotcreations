﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPart : MonoBehaviour
{

    //like in plane from aside

    //x - length
    //y - height
    //z - deep

    [SerializeField] float length; //5
    [SerializeField] float height; //1
    [SerializeField] float deep;  //3

    float forwardOhm;
    float bottomOhm;

    Rigidbody rb;

    float ForwardOhm()
    {
       return 1 - (height/length); // = 4/5
    }

    float BottomOhm()
    {
        float c = (length * length) + (height * height); //a2 + b2 // = 26
        c = Mathf.Sqrt(c); // =~5.1
        return length - c; // =~ 4.9
    }

    float SideOhm()
    {
        return ((length * height) * (1 / deep));
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 velocityIn = rb.velocity;
        Vector3 normalizedVel = velocityIn.normalized;

        float inX = velocityIn.x; //length
        float inY = velocityIn.y; //height
        float inZ = velocityIn.z; //deep


        float x = normalizedVel.z * forwardOhm;
        float y;
        float z;

       // rb.velocity += 

    }
}
