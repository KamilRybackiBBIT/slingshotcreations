﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sling : MonoBehaviour
{
    [SerializeField] bool x;
    [SerializeField] bool y;
    [SerializeField] bool z = true;

    [SerializeField] [Range(-1f, 1f)] float xSpeed;
    [SerializeField] [Range(-1f, 1f)] float ySpeed;
    [SerializeField] [Range(-1f, 1f)] float zSpeed = 0.65f;



    [TextArea]
    [SerializeField]
    string Instructions = "Press: I to sling, and J to shot       " + "This Is Temporary Script, for easy and " +
        "fast access to sling shot, delete this is you want make custom one";

    void Update()
    {
        if (Input.GetKey("i"))
        {
            if (GetComponent<SlingShot>().PercentX > -1f && GetComponent<SlingShot>().PercentX < 1f)
            {
                if (x)
                {
                    GetComponent<SlingShot>().PercentX -= xSpeed * Time.deltaTime;
                }
            }

            if (GetComponent<SlingShot>().PercentY > -1f && GetComponent<SlingShot>().PercentY < 1f)
            {
                if (y)
                {
                    GetComponent<SlingShot>().PercentY -= ySpeed * Time.deltaTime;
                }
            }

            if (GetComponent<SlingShot>().PercentZ > -1f)
            {
                if (z)
                {
                    GetComponent<SlingShot>().PercentZ -= zSpeed * Time.deltaTime;
                }
            }

        }

        if (Input.GetKeyDown("j"))
        {
            GetComponent<SlingShot>().ShotBool = true;
        }
    }
}
