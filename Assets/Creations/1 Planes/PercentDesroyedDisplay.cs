﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PercentDesroyedDisplay : MonoBehaviour
{
    PercentDestroyed dd;
    void Start()
    {
      dd = GetComponentInParent<PercentDestroyed>();
    }

    void Update()
    {
        GetComponent<Image>().fillAmount = dd.percentDestroyed;
    }
}
