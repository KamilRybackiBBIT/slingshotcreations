﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PercentDestroyed : MonoBehaviour
{
    public List<GameObject> rbs;
    public float[] ys;
    public bool[] destructed;
    [Range(0f,100f)]
    public float percentDestroyed;
    public int destructedInt;

    void Start()
    {
        foreach (Rigidbody rb in transform.GetComponentsInChildren<Rigidbody>())
        {
            rbs.Add(rb.gameObject);
        }

        ys = new float[rbs.Count - 1];

        for (int i = 0; i < ys.Length; i++)
        {
            ys[i] = rbs[i].transform.position.y;
        }

        destructed = new bool[rbs.Count - 1];

    }

    void Update()
    {

        for (int i = 0; i < rbs.Count - 1; i++)
        {
            if (rbs[i].transform.position.y < ys[i] - 5f && !destructed[i])
            {
                destructed[i] = true;
                destructedInt ++;
            }
        }

        percentDestroyed = FST.map(0f, destructed.Length, 0f,1f, destructedInt);

    }
}
