﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneEffects : MonoBehaviour
{
    public bool isRealistic;
    public GameObject RExplosion, CExplosion;

    public void Explosion(Vector3 worldPos, Transform parent)
    {
        GameObject partObj = Instantiate(RExplosion, worldPos, Quaternion.identity, parent);
        ParticleSystem particle = partObj.GetComponent<ParticleSystem>();

        particle.Play();
    }

}
