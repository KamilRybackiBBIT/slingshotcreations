﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FastAnimatorController : MonoBehaviour
{

    public string inputKey = "1";
    public string animatorTriggerString = "trig";

    public UnityEvent OnTrig;

    public bool startOff;
    
    private void Start()
    {
        if (startOff)
        {
            Main();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(inputKey))
        {
            Main();
        }
    }

    public void Main()
    {
        GetComponent<Animator>().SetTrigger(animatorTriggerString);
        OnTrig?.Invoke();
    }
    
}
