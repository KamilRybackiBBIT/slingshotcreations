﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPartV2 : MonoBehaviour
{
    //scale
    // x 3
    // y 1
    // z 5

    //sa - surface area - pole powierzchni
    public float forwardSA; // 3 * 1 = 3
    public float sideSA; //1 * 5 = 5
    public float bottomSA; // 5 * 3 = 15

    public Rigidbody rb;

    public GameObject VelocityNorm;
    public GameObject VelocityRel;

    [SerializeField] Vector3 dir;

    [SerializeField]
    [Range(-1f, 1f)]
    float forwardDir;
    [SerializeField]
    [Range(-1f, 1f)]
    float bottomDir;
    [SerializeField]
    [Range(-1f, 1f)]
    float sideDir;

    //siła relatywna do obiektu
    [SerializeField]
    float forwardRelVelocity;
    [SerializeField]
    float bottomRelVelocity;
    [SerializeField]
    float sideRelVelocity;

    //modyfikacja siły
    [SerializeField]
    float outZ;
    [SerializeField]
    float outY;
    [SerializeField]
    float outX;

    //opór powietrza
    [SerializeField]
    float zOhm;
    [SerializeField]
    float yOhm;
    [SerializeField]
    float xOhm;

    [Range(0f,100f)]
    [SerializeField]
    float yModif;

    [ContextMenu("SetMass")]
    void SetMass()
    {
        rb = GetComponent<Rigidbody>();
        rb.mass = transform.localScale.x * transform.localScale.y * transform.localScale.z;
    }

    [ContextMenu("SetSa")]
    void SetSa()
    {
        forwardSA = transform.localScale.x * transform.localScale.y; // 3 * 1 = 3
        sideSA = transform.localScale.y * transform.localScale.z; //1 * 5 = 5
        bottomSA = transform.localScale.x * transform.localScale.z; // 3 * 5 = 15
    }


    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {
        //velocity
        Vector3 velocityIn = rb.velocity;
        Vector3 velocityNormalized = velocityIn.normalized;
        Vector3 velocityRelative = velocityIn + transform.position;
        Vector3 velocityRelativeNormalized = velocityNormalized + transform.position;

        //Debug.Log(velocityRelative + " " + velocityRelativeNormalized);

        //info
        VelocityNorm.transform.position = velocityRelativeNormalized;
        VelocityRel.transform.position = velocityRelative;

        forwardDir = VelocityNorm.transform.localPosition.z * 5; 
        bottomDir = VelocityNorm.transform.localPosition.y * 1f;
        sideDir = VelocityNorm.transform.localPosition.x * 3;


        forwardRelVelocity = VelocityRel.transform.localPosition.z;
        bottomRelVelocity = VelocityRel.transform.localPosition.y;
        sideRelVelocity = VelocityRel.transform.localPosition.x;

        zOhm = Mathf.Abs(forwardDir * forwardSA * forwardRelVelocity);
        //Debug.Log(zOhm);
        yOhm = Mathf.Abs(bottomDir * bottomSA * bottomRelVelocity);
        //Debug.Log(yOhm);
        xOhm = Mathf.Abs(sideDir * sideSA * sideRelVelocity);
        //Debug.Log(yOhm);

        //finalne opornosci- siły w przeciwną stronę
        outZ = forwardDir * zOhm;
        outY = bottomDir * yOhm;
        outX = sideDir * xOhm;

        //dodać opór do 3 wartosci
        rb.AddForce(-transform.forward * outZ * Time.fixedDeltaTime);
        rb.AddForce(-transform.up * outY * Time.fixedDeltaTime);
        rb.AddForce(-transform.right * outX * Time.fixedDeltaTime);

        //d

        //outZ = forwardRelVelocity / ((forwardSA * rb.velocity.magnitude) * forwardDir);
        //outY = bottomRelVelocity / ((bottomSA * rb.velocity.magnitude) * bottomDir);

        ////////podstawowa sila//////////opór

        //rb.AddForce(transform.forward * outZ  * Time.fixedDeltaTime);
        //rb.AddForce(transform.up * outY  * Time.fixedDeltaTime);


        //rb.AddForce(transform.up * outY * 10000 * Time.fixedDeltaTime);
        //rb.AddForce(transform.right * outZ * 10000 * Time.fixedDeltaTime);

        if (Input.GetKey("s"))
        {
            transform.Rotate(-50f * Time.deltaTime, 0f, 0f);
        }

        if (Input.GetKey("w"))
        {
            transform.Rotate(50f * Time.deltaTime, 0f, 0f);
        }

        if (Input.GetKey("a"))
        {
            transform.Rotate(0f, 0f, 50f * Time.deltaTime);
        }

        if (Input.GetKey("d"))
        {
            transform.Rotate(0f, 0f, -50f * Time.deltaTime);
        }

        if (Input.GetKey("q"))
        {
            transform.Rotate(0f, -50f * Time.deltaTime, 0f);
        }

        if (Input.GetKey("e"))
        {
            transform.Rotate(0f, 50f * Time.deltaTime, 0f); 
        }

        if (Input.GetKey("space"))
        {
            //Debug.Log("Space");
            rb.AddForce(transform.forward * 1000 * Time.fixedDeltaTime);
        }

        /*

        forward = Vector3.Distance(transform.forward, VelocityNorm.transform.localPosition) - 1;

        Debug.Log((Vector3.forward - velocityIn).magnitude);

        bottom = Vector3.Distance(Vector3.up, velocityNormalized) - 1;
        side = Vector3.Distance(Vector3.right, velocityNormalized) - 1;

            */
        /*

        outZ = Mathf.Abs( velocityIn.z ) / forwardSA * forwardAffected;
        outY = Mathf.Abs( velocityIn.y ) / bottomSA * bottomAffected;
        //outX = Mathf.Abs( velocityIn.x ) / sideSA * sideAffected; //if faster more ohm





        //Vector3 velocityOut = new Vector3(outX,outY,outZ);
        //rb.velocity += velocityOut * Time.fixedDeltaTime;
        //rb.AddForce(velocityOut * Time.fixedDeltaTime);

        if (Input.GetKey("w"))
        {
            Debug.Log("XD");
            rb.AddForce(transform.forward * 10002 * Time.fixedDeltaTime);
        }
        */
    }
}
