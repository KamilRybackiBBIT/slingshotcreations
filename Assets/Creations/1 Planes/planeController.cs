﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class planeController : MonoBehaviour
{
    [Header("Settings")]
    float basicSpeed = 100f;
    [SerializeField] private ForceMode forceMode;
    public float boostSpeed;
    [SerializeField] float turnSpeed = 0.05f;

     public float forwardRezistance = 1f;
     public float upRezistance = 50f;
     public float sideRezistance = 15f;
    
    [Header("Info")]
    
    [SerializeField] [DisableEditAttribute]
    float zRot;
    [SerializeField][DisableEditAttribute]
    float yRot;
    [SerializeField][DisableEditAttribute]
    float xRot;

    [SerializeField][DisableEditAttribute]
    float oporZ;
    [SerializeField][DisableEditAttribute]
    float oporY;
    [SerializeField][DisableEditAttribute]
    float oporX;

    [SerializeField][DisableEditAttribute]
    float outZ;
    [SerializeField][DisableEditAttribute]
    float outY;
    [SerializeField][DisableEditAttribute]
    float outX;

    [SerializeField][DisableEditAttribute]
    bool JoporZ;
    [SerializeField][DisableEditAttribute]
    bool JoporY;
    [SerializeField][DisableEditAttribute]
    bool JoporX;

    [SerializeField][DisableEditAttribute]
    float speedMultiplier;

    Rigidbody rb;

    [SerializeField] [DisableEditAttribute] float efficiency = 1f;

    [DisableEditAttribute]
    public float startChildCount; 
    [DisableEditAttribute]
    public float currentChildCount;

    [DisableEditAttribute]
    public bool addingPower;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        startChildCount = transform.childCount;
        efficiency = 1f;
    }


    void FixedUpdate()
    {
        currentChildCount = transform.childCount;
        efficiency = currentChildCount / startChildCount;

        zRot = Input.GetAxisRaw("Horizontal");
        xRot = Input.GetAxisRaw("Vertical");

        if (Input.GetKey("q")) yRot = -1;
        else if (Input.GetKey("e")) yRot = 1;
        else yRot = 0;

        Vector3 VDir = rb.velocity.normalized;
        Vector3 relativeVDir = transform.InverseTransformDirection(VDir);

        Vector3 V = rb.velocity;
        Vector3 relativeV = transform.InverseTransformDirection(V);

        //Debug.Log("VDIR: " + VDir + " reladiveVDir: " + relativeVDir + " relativeV: " + relativeV);

        speedMultiplier = rb.velocity.magnitude / 50 * efficiency;

        //kalibracia oporów 0/1
        oporZ = -relativeVDir.z;
        oporY = -relativeVDir.y;
        oporX = -relativeVDir.x;

        //filnale opornośći
        outZ =  oporZ * forwardRezistance * rb.mass * speedMultiplier * efficiency;
        outY =  oporY * upRezistance * rb.mass * speedMultiplier * efficiency;
        outX =  oporX * sideRezistance * rb.mass * speedMultiplier * efficiency;

        //dodawanie oporności

        if (oporZ > -1f && oporZ < 1f)
        {
            JoporZ = true;
            rb.AddForce(transform.forward * outZ);
        }

        else
        {
            JoporZ = false;
        }

        if (oporY > -1f && oporY < 1f)
        {
            JoporY = true;
            rb.AddForce(transform.up * outY);
        }

        else
        {
            JoporY = false;
        }

        if (oporX > -1f && oporX < 1f)
        {
            JoporX = true;
            rb.AddForce(transform.right * outX);
        }

        else
        {
            JoporX = false;
        }




        //Debug.Log(transform.forward * oporZ * 1000000f * Time.fixedDeltaTime);

        if (Input.GetKey("space"))
        {
            rb.AddForce(transform.forward * boostSpeed * Time.fixedDeltaTime, forceMode);
            addingPower = true;
        }

        else
        {
            addingPower = false;
        }

        Vector3 input = new Vector3(xRot, yRot, -zRot);
        Quaternion finalRot = transform.localRotation * Quaternion.Euler(input * 35);
        //transform.localRotation = Quaternion.Slerp(transform.localRotation, finalRot, Time.deltaTime);

        Vector3 forward = transform.forward * 3f; //forward
        Vector3 down = Vector3.down; //gravity

        //rb.rotation = rb.rotation * Quaternion.Euler(transform.right) * Quaternion.Euler(transform.forward);

        rb.angularVelocity += transform.right * xRot * turnSpeed;
        rb.angularVelocity += transform.up * yRot * turnSpeed;
        rb.angularVelocity += -transform.forward * zRot * turnSpeed;

    }

    public void CollidersEnablator(bool state)
    {
        if (rb == null) rb = GetComponent<Rigidbody>();

        rb.isKinematic = !state;
        foreach (SphereCollider sc in transform.GetComponentsInChildren<SphereCollider>())
        {
            sc.enabled = state;
        }
    }

}
