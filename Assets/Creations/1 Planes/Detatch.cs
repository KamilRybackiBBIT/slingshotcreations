﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detatch : MonoBehaviour
{
    public bool HasExplosion;
    public bool IsWheel;
    bool exploded = false;
    Rigidbody rb;
    private void Start()
    {
       //rb = gameObject.AddComponent<Rigidbody>();
       
        //rb.isKinematic = true;
        //rb.Sleep();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Building" || other.tag == "Ground")
        {

            DetatchPart();
            
        }
    }

    public void DetatchPart()
    {
        if (HasExplosion && !exploded)
        {
            exploded = true;
            PlaneEffects planeEffects = GetComponentInParent<PlaneEffects>();
            planeEffects.Explosion(transform.position, transform);
        }

        if (IsWheel)
        {
            gameObject.AddComponent<SphereCollider>();
            Destroy(gameObject.GetComponent<WheelCollider>());
        }

        //GetComponent<SphereCollider>().isTrigger = false;   
        if (rb == null)
        {
            rb = gameObject.AddComponent<Rigidbody>();
        }
            
        //rb.isKinematic = false;
        transform.parent = null;
    }
}
