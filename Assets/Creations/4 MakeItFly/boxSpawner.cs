﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class boxSpawner : MonoBehaviour
{

    public GameObject cubePrefab;
    public float howManyCubes;
    public float size;
    
    [Space]
    public float minSizeX;
    public float maxSizeX;
    
    public float minSizeY;
    public float maxSizeY;
    
    public float minSizeZ;
    public float maxSizeZ;




    [ContextMenu("Create")]
    public void Create()
    {
        for (int i = 0; i < howManyCubes; i++)
        {
            Vector3 pos = transform.position;


            float sizeX = Random.Range(minSizeX, maxSizeX);
            float sizeY = Random.Range(minSizeY, maxSizeY); ;
            float sizeZ = Random.Range(minSizeZ, maxSizeZ); ;

            float posX = Random.Range(-size / 2, size / 2);
            float posZ = Random.Range(-size / 2, size / 2);

            float posY = sizeY / 2;

            Vector3 Finalpos = new Vector3(posX, posY, posZ) + transform.position;
            Vector3 FinalSize = new Vector3(sizeX, sizeY, sizeZ);

            GameObject xd = Instantiate(cubePrefab, Finalpos, Quaternion.identity, transform);
            xd.transform.localScale = FinalSize;
        }
    }

    [ContextMenu("Clear")]
    public void DestroyAll()
    {
        int count = transform.childCount;
        for (int i = 0; i < count; i++)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }


}
