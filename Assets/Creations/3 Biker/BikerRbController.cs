﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikerRbController : MonoBehaviour
{
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKey("space"))
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, Time.deltaTime * 2);
            rb.angularVelocity = Vector3.Slerp(rb.angularVelocity, Vector3.zero, Time.deltaTime * 2);
            foreach(WheelCollider wc in GetComponentsInChildren<WheelCollider>())
            {
                //wc.enabled = false;
            }
            Debug.Log("D");
        }

        else
        {
            foreach (WheelCollider wc in GetComponentsInChildren<WheelCollider>())
            {
                //wc.enabled = true;
            }

        }
    }
}
