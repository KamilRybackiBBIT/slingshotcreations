﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class Ragdoll : MonoBehaviour
{
    [SerializeField] Rigidbody[] limbs;
    [SerializeField] Rigidbody[] CertainLimbs;
    [SerializeField] Rigidbody character;
    
    [SerializeField] float Multiplier;
    [SerializeField] ForceMode myForceMode;
    public UnityEvent onRagdoll;
    public UnityEvent onStart;

    [FormerlySerializedAs("JointToBrake")]
    public FixedJoint jointToBrake;
    private void Awake()
    {
        if (limbs.Length == 0)
        {
            limbs = transform.GetComponentsInChildren<Rigidbody>();
        }

        if (character == null)
        {
            character = transform.GetComponentInParent<Rigidbody>();
        }


    }

    private void Start()
    {
        limbEnable(false);
        onStart?.Invoke();
    }

    private void FixedUpdate()
    {



        if (Input.GetKeyDown("5"))
        {
            GetComponent<Animator>().enabled = false;
            onRagdoll?.Invoke();
            for (int i = 0; i < limbs.Length;  i++)
            {
                limbs[i].velocity = Vector3.zero;
                limbs[i].AddForce(character.velocity * character.velocity.magnitude * Multiplier, myForceMode);
            }
            limbEnable(true);
        }

        if (Input.GetKeyDown("4"))
        {
            Destroy(jointToBrake);
        }

    }

    public void EnableRagdoll()
    {
        GetComponent<Animator>().enabled = false;
        onRagdoll?.Invoke();
        for (int i = 0; i < limbs.Length; i++)
        {
            limbs[i].velocity = Vector3.zero;
            //limbs[i].AddForce(character.velocity * character.velocity.magnitude * Multiplier, myForceMode);
        }
        limbEnable(true);
    }

    public void LimbsKinematic(bool state)
    {
        for (int i = 0; i < limbs.Length; i++)
        {
            limbs[i].isKinematic = state;
        }
    }

    public void limbEnable(bool state)
    {
        for (int i = 0; i < limbs.Length; i++)
        {
            if (limbs[i].GetComponent<SphereCollider>())
            {
                limbs[i].GetComponent<SphereCollider>().enabled = state;
            }

            if (limbs[i].GetComponent<CapsuleCollider>())
            {
                limbs[i].GetComponent<CapsuleCollider>().enabled = state;
            }

            if (limbs[i].GetComponent<BoxCollider>())
            {
            limbs[i].GetComponent<BoxCollider>().enabled = state;
        }
        }
    }
}
