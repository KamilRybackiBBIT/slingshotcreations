﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetController : MonoBehaviour
{
    float basicSpeed = 100f;

    [SerializeField] float turnSpeed;

    [SerializeField]
    float zRot;
    [SerializeField]
    float yRot;
    [SerializeField]
    float xRot;

    [SerializeField]
    float oporZ;
    [SerializeField]
    float oporY;
    [SerializeField]
    float oporX;

    [SerializeField]
    float outZ;
    [SerializeField]
    float outY;
    [SerializeField]
    float outX;

    [SerializeField]
    bool JoporZ;
    [SerializeField]
    bool JoporY;
    [SerializeField]
    bool JoporX;

    [SerializeField]
    float speedMultiplier;

    Rigidbody rb;

    [SerializeField] float efficiency = 1f;

    public float startChildCount;
    public float currentChildCount;

    public bool addingPower;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        startChildCount = transform.childCount;
        efficiency = 1f;
    }


    void FixedUpdate()
    {
        currentChildCount = transform.childCount;
        efficiency = currentChildCount / startChildCount;

        zRot = Input.GetAxisRaw("Horizontal");
        xRot = Input.GetAxisRaw("Vertical");

        if (Input.GetKey("q")) yRot = -1;
        else if (Input.GetKey("e")) yRot = 1;
        else yRot = 0;

        Vector3 VDir = rb.velocity.normalized;
        Vector3 relativeVDir = transform.InverseTransformDirection(VDir);

        Vector3 V = rb.velocity;
        Vector3 relativeV = transform.InverseTransformDirection(V);

        //Debug.Log("VDIR: " + VDir + " reladiveVDir: " + relativeVDir + " relativeV: " + relativeV);

        speedMultiplier = rb.velocity.magnitude / 30 * efficiency;

        //kalibracia oporów 0/1
        oporZ = -relativeVDir.z;
        oporY = -relativeVDir.y;
        oporX = -relativeVDir.x;

        //filnale opornośći
        outZ =  oporZ * 3f * rb.mass * speedMultiplier * efficiency;
        outY =  oporY * 25f * rb.mass * speedMultiplier * efficiency;
        outX =  oporX * 15f * rb.mass * speedMultiplier * efficiency;

        //dodawanie oporności

        if (oporZ > -1f && oporZ < 1f)
        {
            JoporZ = true;
            rb.AddForce(transform.forward * outZ);
        }

        else
        {
            JoporZ = false;
        }

        if (oporY > -1f && oporY < 1f)
        {
            JoporY = true;
            rb.AddForce(transform.up * outY);
        }

        else
        {
            JoporY = false;
        }

        if (oporX > -1f && oporX < 1f)
        {
            JoporX = true;
            rb.AddForce(transform.right * outX);
        }

        else
        {
            JoporX = false;
        }




        //Debug.Log(transform.forward * oporZ * 1000000f * Time.fixedDeltaTime);

        if (Input.GetKey("space"))
        {
            rb.AddForce(transform.forward * 100 * Time.fixedDeltaTime, ForceMode.VelocityChange);
            addingPower = true;
        }

        else
        {
            addingPower = false;
        }

        Vector3 input = new Vector3(xRot, yRot, -zRot);
        Quaternion finalRot = transform.localRotation * Quaternion.Euler(input * 35);
        //transform.localRotation = Quaternion.Slerp(transform.localRotation, finalRot, Time.deltaTime);

        Vector3 forward = transform.forward * 3f; //forward
        Vector3 down = Vector3.down; //gravity

        //rb.rotation = rb.rotation * Quaternion.Euler(transform.right) * Quaternion.Euler(transform.forward);

        rb.angularVelocity += transform.right * xRot * turnSpeed;
        rb.angularVelocity += transform.up * yRot * turnSpeed;
        rb.angularVelocity += -transform.forward * zRot * turnSpeed;

    }

    public void CollidersEnablator(bool state)
    {
        if (rb == null) rb = GetComponent<Rigidbody>();

        rb.isKinematic = !state;
        foreach (SphereCollider sc in transform.GetComponentsInChildren<SphereCollider>())
        {
            sc.enabled = state;
        }
    }

}
