﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetpackAnimator : MonoBehaviour
{


    public Animator Person;
    public Animator Pack;
    public JetController jc;
    public ParticleSystem ps;

    public void SlingShotted()
    {
        Person.SetTrigger("SlingShotted");
        Pack.SetTrigger("FoldTrig");
    }

    private void Update()
    {
        Pack.SetBool("AddingPower", jc.addingPower);
        Person.SetBool("AddingPower", jc.addingPower);

        //ParticleSystem.EmissionModule emit = ps.emission;

        if (ps == null)
        {
            return;
        }

        if (jc.addingPower)
        {
            if (!ps.isPlaying)
            {
                ps.Play();
                
            }
            SetEmission(ps, 120f);
        }

        else
        {
            if (ps.isPlaying)
            {
                SetEmission(ps, 0f);
                //ps.Stop();
            }
        }

    }


    public void SetEmission(ParticleSystem particle, float emitTimeValue)
    {
        if (!particle.isPlaying)
        {
            particle.Play();
        }

        var xd = particle.emission;
        xd.rateOverTime = new ParticleSystem.MinMaxCurve(emitTimeValue);
    }

    public void SetForce(ParticleSystem particle, Vector3 dir, float force)
    {
        var mainn = particle.main;
        mainn.startSpeed = force;
    }



}
