﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fuel : MonoBehaviour
{
    public string KeyCode;
    public float MaxSeconds;
    public float SecondsLeft;
    public Image fill;
    public bool Lerp;
    public float LerpFactor;

    private void Start()
    {
        SecondsLeft = MaxSeconds;
        fill.fillAmount = FST.map(0f, MaxSeconds, 0f, 1f, SecondsLeft);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode))
        {
            SecondsLeft -= Time.deltaTime;
            if (Lerp)
            {
                float amount = FST.map(0f, MaxSeconds, 0f, 1f, SecondsLeft);
                fill.fillAmount = Mathf.Lerp(fill.fillAmount, amount, Time.deltaTime * LerpFactor);
            }

            else
            {
                fill.fillAmount = FST.map(0f, MaxSeconds, 0f, 1f, SecondsLeft);
            }
            
        }
    }
}
