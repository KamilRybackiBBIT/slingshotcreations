﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorFunctions : MonoBehaviour
{
    public Animator animator;
    //public MyIntEvent m_MyEvent;
    public UnityEvent<bool,float> Setter;

    //public JetController

    void Start()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();

            //Debug.Log(animator.parameters.ToString());
        }
    }
    
    public void SetBool(MyIntEvent xd)
    {
        //animator.SetBool(boolName)
    }
    
}

[System.Serializable]
public class MyIntEvent : UnityEvent<int, int>
{
}

public class ExampleClass : MonoBehaviour
{
    public MyIntEvent m_MyEvent;

    void Start()
    {
        if (m_MyEvent == null)
            m_MyEvent = new MyIntEvent();

        m_MyEvent.AddListener(Ping);
    }

    void Update()
    {
        if (Input.anyKeyDown && m_MyEvent != null)
        {
            m_MyEvent.Invoke(5, 6);
        }
    }

    void Ping(int i, int j)
    {
        Debug.Log("Ping" + i + ", " + j);
    }
}
