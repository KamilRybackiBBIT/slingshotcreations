﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputNode : MonoBehaviour
{
    public string[] KeyCodes;
    public bool State;
    public UnityEvent OnInput;
    public SlingShot ss;

    void Update()
    {
        bool isIsnput = false;

        for (int i = 0; i < KeyCodes.Length; i++)
        {
            if (Input.GetKey(KeyCodes[i]))
            {
                isIsnput = true;
            }
        }

        if (isIsnput)
        {
            State = true;
            OnInput?.Invoke();
        }

        else
        {
            State = false;
        }

        GetComponent<Animator>().SetBool("Hold", State);
        GetComponent<Animator>().SetFloat("Sling", -ss.PercentZ);
    }

    public void SetAfterSling()
    {
        GetComponent<Animator>().SetBool("AfterSling", true);
    }
}
