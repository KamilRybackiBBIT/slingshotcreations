Shader "Mobile/Diffuse(Color-mat, static light)" {
Properties {
	_Color ("Color", Color) = (1,1,1,1)
	_LightColorT("Light Color", Color) = (1,1,1,1)
	_LightDirT("Light Direction", Vector) = (50, -30, 0, 0)
	_AmbientStr("Ambient Strength", Range(0, 1)) = 0.2
}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 150

CGPROGRAM
#pragma surface surf LambertBase noforwardadd noambient nofog

uniform fixed4 _Color;
uniform fixed4 _LightColorT;
uniform half4 _LightDirT;
uniform half _AmbientStr;

inline fixed4 LightingLambertBase(SurfaceOutput s, fixed3 lightDir, fixed atten)
{
	lightDir = normalize(_LightDirT.xyz);

	fixed diff = max (0, dot (s.Normal, lightDir));
	diff += _AmbientStr;
	
	fixed4 c;
	c.rgb = (s.Albedo * _LightColorT.rgb * diff) * atten;
	UNITY_OPAQUE_ALPHA(c.a);
	return c;
}

struct Input {
	float4 color : COLOR;
};

void surf (Input IN, inout SurfaceOutput o) {
	o.Albedo = _Color.rgb;
	o.Alpha = 1;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}