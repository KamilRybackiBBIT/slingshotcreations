﻿Shader "Grayscale/MobileDiffuse"
{
    Properties
    {
        _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
        _Effect ("Grayscale Effect", Range (0, 1)) = 1.0
        _Color("Color", Color)=(0.5,0.5,0.5,1.0)
        _TintColor("Tint Color", Color) = (0,0,0,1)
    }

    SubShader
    {
    	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}

	    LOD 200

	    CGPROGRAM
	    #pragma surface surf Lambert noforwardadd

	    sampler2D _MainTex;
	    uniform float _Effect;
	    fixed4 _Color;
        float4 _TintColor;

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			half4 c = (tex2D(_MainTex, IN.uv_MainTex) * _Color) + _TintColor;
			o.Albedo = lerp(c.rgb, dot(c.rgb, float3(0.3, 0.59, 0.11)), _Effect);
			o.Alpha = c.a;
		}
		ENDCG
	}
	Fallback "Mobile/Default"
}