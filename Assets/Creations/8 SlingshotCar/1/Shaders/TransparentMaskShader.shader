﻿//----------------------------------------------
//            Marvelous Techniques
// Copyright © 2015 - Arto Vaarala, Kirnu Interactive
// http://www.kirnuarp.com
//----------------------------------------------
Shader "Mobile/TransparentMaskShader" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Mask ("Noise", 2D) = "white" {}
		_MaskGradient ("Mask", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_DissolveProgress ("Dissolve", Range (0.01, 1)) = 1
		_TailCut ("Tail Cut", Float) = 50
	}
	SubShader {
		Tags { "QUEUE"="Transparent+2" "IgnoreProjector"="True" "RenderType"="Transparent" }
		LOD 200
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
		Tags { "LIGHTMODE"="ForwardBase" "QUEUE"="Transparent" "RenderType"="Transparent"}
		
		
		CGPROGRAM
		
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"
		
		struct IN{
			float4 vertex : POSITION;
			float4 texcoord : TEXCOORD0;
			float4  color  :  COLOR0;
		};
		
		struct OUT {
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
            float2 noiseuv : TEXCOORD2;
			float4 color : TEXCOORD1;
		};
			
		sampler2D _MainTex;
		float4 _MainTex_ST;
		sampler2D _Mask;
		float4 _Mask_ST;
		sampler2D _MaskGradient;
		float4 _MaskGradient_ST;

		float _DissolveProgress;
		half _TailCut;
		uniform half4 _Color;
		
		OUT vert(IN v) {
			OUT o;
			
			o.pos = UnityObjectToClipPos (v.vertex);
			o.uv =  TRANSFORM_TEX (v.texcoord, _MainTex);
			o.noiseuv = TRANSFORM_TEX (v.texcoord, _Mask);
			o.color = v.color;
			return o;
		}
		
		fixed4 frag(OUT vert) : COLOR {
			half4 ocolor = half4(1,1,1,1);
			half4 textureColor = tex2D (_MainTex, vert.uv);
			half maskColor = tex2D(_Mask, vert.noiseuv).r;
			half maskGradientColor = tex2D(_MaskGradient, vert.noiseuv).r;

			ocolor.xyz = textureColor * _Color.xyz;

			half progress = step(maskGradientColor, _DissolveProgress);
			half tailProgress = saturate(1 - ((maskGradientColor - _DissolveProgress) / (_DissolveProgress / (_TailCut * _DissolveProgress))));

			ocolor.a = max(progress, tailProgress * maskColor);

			return ocolor;
		}
		ENDCG
	}
	} 
	FallBack "Diffuse"
}
