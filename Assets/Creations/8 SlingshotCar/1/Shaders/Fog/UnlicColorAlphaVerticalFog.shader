// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit shader. Simplest possible colored shader.
// - no lighting
// - no lightmap support
// - no texture

Shader "Unlit/Color/AlphaVerticalFog" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
	_FogMaxHeight ("fog start (distance to camera)", Float) = 0.0
	_FogMinHeight ("fog end (distance to camera)", Float) = -1.0
}

SubShader {
    Tags { "Queue"="Transparent" "RenderType"="Transparent"}
			Blend SrcAlpha OneMinusSrcAlpha
    LOD 100

    Pass {
    
        Stencil {
                Ref 2
                Comp NotEqual
                Pass Keep
            }
    
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata_t {
                float4 vertex : POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f {
                float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
                UNITY_FOG_COORDS(0)
                UNITY_VERTEX_OUTPUT_STEREO
            };

            fixed4 _Color;
			float _FogMaxHeight;
			float _FogMinHeight;

            v2f vert (appdata_t v)
            {
                v2f o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex); 

                // fog calculations
				float vh = mul (unity_ObjectToWorld, v.vertex).y;

				half max = _WorldSpaceCameraPos.y + _FogMaxHeight;
				half min = _WorldSpaceCameraPos.y + _FogMinHeight;
				half h = clamp ( (vh - min) / (max - min) , 0, 1 );

				o.color = lerp (unity_FogColor, _Color, h);

                return o;
            }

            fixed4 frag (v2f i) : COLOR
            {
                fixed4 col = i.color;//_Color;
                UNITY_APPLY_FOG(i.fogCoord, col);
                //UNITY_OPAQUE_ALPHA(col.a);
                return col;
            }
        ENDCG
    }
}

}
