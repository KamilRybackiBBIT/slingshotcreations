﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Aidem/Surface/World Space Fog v4"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("color", Color) = (1, 1, 1, 1)
		_FogColor ("custom fog color", Color) = (0, 0 , 0, 0)
		_CustomFogColorPower ("custom fog color power", Range(0,1)) = 0.0
		_FogMaxHeight ("fog start", Float) = 0.0
		_FogMinHeight ("fog end", Float) = -1.0
		_EmissionValue ("emmision", Range(0,1)) = 0.0
		_UnityFogStartOffset ("unity FogStart offset (distance in units)", Float) = 0.0
		_UnityFogEndOffset ("unity FogEnd offset (distance in units)", Float) = 0.0
	}
	SubShader 
	{
    	Tags { "RenderType"="Opaque" }
    	LOD 200
		
    	CGPROGRAM
    	#pragma surface surf Lambert vertex:vertexFunc finalcolor:fogFunc
		#include "UnityCG.cginc"
		
    	half4 _Color;
		half4 _FogColor;
		half _FogMaxHeight;
		half _FogMinHeight;
		half _EmissionValue;
        sampler2D _MainTex;
		
		uniform half4 unity_FogStart;
		uniform half4 unity_FogEnd;
		half _UnityFogStartOffset;
		half _UnityFogEndOffset;
		half _CustomFogColorPower;
		
		struct Input
		{
			half4 worldSpaceVertexPosition;
			half cameraToVertexDistance;
            float2 uv_MainTex;
		};

		void vertexFunc (inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);
			o.cameraToVertexDistance = length(UnityObjectToViewPos(v.vertex).xyz);
			o.worldSpaceVertexPosition = mul (unity_ObjectToWorld, v.vertex);
    	}
		
    	void surf (Input IN, inout SurfaceOutput o)
		{
			o.Alpha = _Color.a;
			o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * _Color.rgb;
			o.Emission = _EmissionValue;
		}
		
		void fogFunc(Input IN, SurfaceOutput o, inout fixed4 color)
		{
			half max = _FogMaxHeight;
			half min = _FogMinHeight;
			half h = clamp ( (IN.worldSpaceVertexPosition.y - min) / (max - min) , 0, 1 );

			half end = unity_FogEnd.x + _UnityFogEndOffset; 
			half start = unity_FogStart.x + _UnityFogStartOffset;
			half fogFactor = saturate((end - IN.cameraToVertexDistance) / (end - start));
			
			half3 fog = lerp (lerp(unity_FogColor, _FogColor.rgb, _CustomFogColorPower), color.rgb, h);
			
			color.rgb = lerp(unity_FogColor.rgb, fog, fogFactor);

		}
		ENDCG
	}
	
	FallBack "Diffuse"
}