﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Aidem/Vertex/World Space Fog v6"
{
	Properties
	{
		_Color ("color", Color) = (1, 1, 1, 1)
		_FogColor ("fog color", Color) = (0, 0 , 0, 0)
		_FogMaxHeight ("fog start (distance to camera)", Float) = 0.0
		_FogMinHeight ("fog end (distance to camera)", Float) = -1.0
		_UnityFogStartOffset ("unity FogStart offset (distance in units)", Float) = 0.0
		_UnityFogEndOffset ("unity FogEnd offset (distance in units)", Float) = 0.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Tags { "LightMode" = "ForwardBase" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
//			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			uniform float4 _LightColor0;

			fixed4 _Color;
			fixed4 _FogColor;
			float _FogMaxHeight;
			float _FogMinHeight;
			
			uniform half4 unity_FogStart;
			uniform half4 unity_FogEnd;
			half _UnityFogStartOffset;
			half _UnityFogEndOffset;

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
			};
			
			v2f vert (appdata v)
			{
				v2f o;

				// lighting calculations
				float3 normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				fixed3 diff = _LightColor0.rgb * max(0.0, dot(normalDir, lightDir)) * _Color ; 

				// fog calculations
				float vh = mul (unity_ObjectToWorld, v.vertex).y;
				float cd = length(mul(UNITY_MATRIX_MV, v.vertex).xyz);

				half max = _WorldSpaceCameraPos.y + _FogMaxHeight;
				half min = _WorldSpaceCameraPos.y + _FogMinHeight;
				half h = clamp ( (vh - min) / (max - min) , 0, 1 );

				half end = unity_FogEnd.x; 
				half start = unity_FogStart.x;
				half fogFactor = saturate((end - cd) / (end - start));

				half b = unity_FogColor;

				fixed4 col = _Color * UNITY_LIGHTMODEL_AMBIENT + fixed4(diff,0);
				fixed4 fog = lerp (_FogColor, col, h);
				o.color = lerp(unity_FogColor, fog, fogFactor);

				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return i.color;
			}
			ENDCG
		}
	}
}
