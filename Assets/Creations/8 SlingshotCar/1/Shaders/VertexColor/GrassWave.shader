Shader "Custom/GrassWave"
{
    Properties {
		_Layout ("LayOutTexture", 2D) = "white" {}
		
		_WaveMagnitude ("Wave Magnitude", Float) = 1
 		_WaveFrequency ("Wave Frequency", Float) = 1
 		_WaveLength ("Wave Length", Float) = 10
 		_StrechLength ("Strech Length", Float) = 100
 		_UvMagnitude ("UV Magnitude", Float) = 10
        _Zpos ("Zpos", Float) = 1.5
        _Z2pos ("Z2pos", Float) = 1.5
	}
	
    Category {
	Tags { "RenderType"="Opaque" }
    Lighting Off
    LOD 100

	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
            #pragma multi_compile_fog 
			
			#include "UnityCG.cginc"
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
                UNITY_FOG_COORDS(1)
			};

			sampler2D _Layout;
			float4 _Layout_ST;
			
            uniform float _WaveLength;
            uniform float _WaveFrequency;
            uniform float _WaveMagnitude;
            uniform float _StrechLength;
            float _UvMagnitude;

            float _Zpos;
            float _Z2pos;
            
			v2f vert (appdata_t v)
			{
				v2f o;
				
                float3 wPos = mul(unity_ObjectToWorld, v.vertex).xyz;
                float addX = sin((_WaveFrequency * _Time) + (wPos.z / _WaveLength)) * _WaveMagnitude;
			    v.vertex.x += addX;
			    v.vertex.x += v.vertex.x * (wPos.z - _Zpos) / _StrechLength;
			
                float2 offset = _Layout_ST.zw;
                offset.x -= addX * _UvMagnitude;
				o.texcoord = TRANSFORM_TEX(v.texcoord, _Layout) + offset;
				
				half4 tex = tex2Dlod(_Layout, float4(v.texcoord.xy,0,0));
				o.color = lerp(unity_FogColor, v.color, tex.r);
				o.vertex = UnityObjectToClipPos(v.vertex);
				
                UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
			    fixed4 col = i.color;
                UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG 
		}
	}	
    }
}