Shader "Mobile/DiffuseColorTint"
{
	Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _Color("Color", Color)=(0.5,0.5,0.5,1.0)
        _TintColor("Tint Color", Color) = (0,0,0,1)
    }
 
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 150
        CGPROGRAM
        #pragma surface surf Lambert noforwardadd
 
        sampler2D _MainTex;
        fixed4 _Color;
        float4 _TintColor;
 
        struct Input
        {
            float2 uv_MainTex;
        };
 
        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = (tex2D(_MainTex, IN.uv_MainTex) * _Color) + _TintColor;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }

	Fallback "Mobile/Diffuse"
}