﻿Shader "Mobile/Diffuse(FadeAlphaEffect)"
{
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
//		_NormTex ("Normalmap", 2D) = "norm" {}
		_Color ("Main Color", Color) = (1,1,1,1)
		
		_TopStart ("Top Start", float) = 0.6 // Range(1.0, -1.0)) = 0.6
    	_TopRange ("Alpha Top Range", Range(0.0, 10)) = 0.4
    	_BottomStart ("Bottom Start", float) = -0.6 // Range(-1.0, 1.0)) = -0.6
    	_BottomRange ("Alpha Bottom Range", Range(0.0, 10)) = 0.4

    	_LeftStart ("Left Start", float) = -0.6 // Range(-1.0, 1.0)) = -0.6
    	_LeftRange ("Alpha Left Range", Range(0.0, 10)) = 0.4
    	_RightStart ("Right Start", float) = 0.6 // Range(1.0, -1.0)) = 0.6
    	_RightRange ("Alpha Right Range", Range(0.0, 10)) = 0.4
	}
	SubShader {
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Opaque"
			"IgnoreProjector" = "True"
		}

		LOD 150
		
	    Pass
        {
            ZWrite On
            Blend SrcAlpha OneMinusSrcAlpha
			
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            float4 _Color;
            sampler2D _MainTex;

            half _TopStart;
			half _TopRange;
			half _BottomStart;
			half _BottomRange;

			half _LeftStart;
			half _LeftRange;
			half _RightStart;
			half _RightRange;

            struct appdata
            {
                float4 vertex : POSITION;
                half4 color : COLOR;
                half4 uv : TEXCOORD;
            };
            	
            struct v2f
            {
                float4 vertex : SV_POSITION;
                half4 uv : TEXCOORD;
                float4 position : COLOR1;
                half4 color : COLOR;
            };
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);  
                o.position = o.vertex;
                o.color = v.color;
                o.uv = v.uv;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
            	half4 result = tex2D (_MainTex, i.uv) * i.color;
            	result *= _Color;

            	half vAlpha = 1 - lerp(0, 1, (i.position.y - _TopStart) / _TopRange);
            	vAlpha = min(vAlpha, 1 - lerp(0, 1, (i.position.y - _BottomStart) / -_BottomRange));

            	half hAlpha = 1 - lerp(0, 1, (i.position.x - _RightStart) / _RightRange);
            	hAlpha = min(hAlpha, 1 - lerp(0, 1, (i.position.x - _LeftStart) / -_LeftRange));

            	result.a = min(min(vAlpha, hAlpha), result.a);

	         	return result;
         	}

            ENDCG
        }

//		CGPROGRAM
//		#pragma surface surf Lambert noforwardadd alpha
//
//		float4 _Color;
//		sampler2D _MainTex;
////		sampler2D _NormTex;
//
//        half _TopStart;
//		half _TopRange;
//		half _BottomStart;
//		half _BottomRange;
//
//		half _LeftStart;
//		half _LeftRange;
//		half _RightStart;
//		half _RightRange;
//
//		struct Input {
//			float2 uv_MainTex;
////			float2 uv_NormTex;
//		};
//
//		void surf (Input IN, inout SurfaceOutput o) {
//			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color.rgba;
//			o.Albedo = c.rgb;
//			o.Alpha = c.a;
////			o.Normal = UnpackNormal (tex2D (_NormTex, IN.uv_NormTex));
//		}
//		ENDCG
	}

	Fallback "Mobile/Diffuse"
}