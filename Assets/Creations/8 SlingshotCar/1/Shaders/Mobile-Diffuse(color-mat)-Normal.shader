// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Mobile/Diffuse(Color-mat)-Normal" {
Properties {
	_Color ("Color", Color) = (1,1,1,1)
    _BumpMap ("Bumpmap", 2D) = "bump" {}
}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd

uniform half4 _Color;

struct Input {
	float4 color : COLOR;
    float2 uv_BumpMap;
};

sampler2D _BumpMap;

void surf (Input IN, inout SurfaceOutput o) {
	o.Albedo = _Color.rgb;
	o.Alpha = 1;
    o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
}
ENDCG
}

Fallback "Mobile/VertexLit"
}