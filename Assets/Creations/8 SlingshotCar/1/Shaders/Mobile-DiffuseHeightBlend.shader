﻿Shader "Mobile/DiffuseHeightBlend"
{
	Properties {
		_MainTex ("Top", 2D) = "white" {}
		_MainTexBump ("Top Normal", 2D) = "bump" {}
		_MainTex2 ("Bottom", 2D) = "white" {}
		_MainTex2Bump ("Bottom Normal", 2D) = "bump" {}
		_Height ("Height", float) = 0
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }

	CGPROGRAM
	#pragma surface surf Lambert noforwardadd vertex:vert
	
	float4 _Color;
	float _Height;
	sampler2D _MainTex;
    sampler2D _MainTexBump;
	sampler2D _MainTex2;
    sampler2D _MainTex2Bump;

	struct Input {
		float2 uv_MainTex;
		float2 uv_MainTex2;
		float3 worldPos;
        float3 localPos;
	};

    void vert (inout appdata_full v, out Input o) {
        UNITY_INITIALIZE_OUTPUT(Input,o);
        o.localPos = v.vertex.xyz;
    }

	void surf (Input IN, inout SurfaceOutput o) {
		fixed4 topC = tex2D(_MainTex, IN.uv_MainTex) * _Color.rgba;
		fixed4 bottomC = tex2D(_MainTex2, IN.uv_MainTex2) * _Color.rgba;
		
		float hPos = clamp(IN.localPos.z / _Height, 0, 1);
		
		o.Albedo = lerp(bottomC.rgb, topC.rgb, hPos);
		
		fixed3 topN = UnpackNormal (tex2D (_MainTexBump, IN.uv_MainTex));
		fixed3 bottomN = UnpackNormal (tex2D (_MainTex2Bump, IN.uv_MainTex2));
		
        o.Normal = lerp(bottomN, topN, hPos);
	}
	ENDCG
	}

	Fallback "Mobile/Diffuse"
}