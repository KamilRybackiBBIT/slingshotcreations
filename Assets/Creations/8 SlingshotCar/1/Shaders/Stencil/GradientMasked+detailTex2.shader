﻿Shader "Stencil/Gradient (VC masked detail)2"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_GradColA ("Gradient Color A", Color) = (1,1,1,1)
		_GradColB ("Gradient Color B", Color) = (1,1,1,1)
		_Intensity ("Gradient Intensity", Range(0, 5)) = 1

        // detail tex - jules
        _DetailTex ("Detail Texture", 2D) = "white" {}
        _DetailIntensity ("Detail Intensity", Range(0, 1)) = 1
        _DetailParams ("Detail Tiling(XY) Detail Offset(ZW)", Vector) = (1, 1, 0, 0)
        // end - jules
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			Name "FORWARD"
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			#pragma multi_compile_fwdbase nodynlightmap nolightmap

			#pragma shader_feature BLEND_MULTIPLY BLEND_LIGHTEN BLEND_SCREEN BLEND_OVERLAY
			#pragma shader_feature GRAD_RADIAL GRAD_VERTICAL GRAD_HORIZONTAL GRAD_TOP GRAD_BOTTOM
			
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				fixed3 vlight : COLOR0; // ambient/SH/vertexlights + vertex-lit
				float4 pos : SV_POSITION;
				float3 normal : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				fixed4 color : TEXCOORD3;
				UNITY_FOG_COORDS(4)
				SHADOW_COORDS(5)
				UNITY_VERTEX_INPUT_INSTANCE_ID
  				UNITY_VERTEX_OUTPUT_STEREO
			};

			inline fixed4 LightingLambertBase(SurfaceOutput s, fixed3 lightDir, fixed atten)
			{
				fixed diff = max (0, dot (s.Normal, lightDir));

				fixed4 c;
				c.rgb = (s.Albedo * _LightColor0.rgb * diff) * atten;
				c.a = 1;
				return c;
			}

			#if defined(BLEND_MULTIPLY)
				inline fixed4 Blend(fixed4 base, fixed4 blend)
				{ 
					fixed4 res = base * blend;
					res.a = base.a;
					return res;
				}
			#elif defined(BLEND_LIGHTEN)
				inline fixed4 Blend(fixed4 base, fixed4 blend)
				{ 
					fixed4 res = max(base, blend);
					res.a = base.a;
					return res;
				}
			#elif defined(BLEND_SCREEN)
				inline fixed4 Blend(fixed4 base, fixed4 blend) 
				{ 	
					fixed4 res = 1.0 - (1.0 - base) * (1.0 - blend);
					res.a = base.a;
					return res;
				}
			#else
				inline fixed4 Blend(fixed4 base, fixed4 blend) 
				{
					fixed4 res = base > 0.5 ? 1.0 - 2.0 * (1.0 - base) * (1.0 - blend) : 2.0 * base * blend;
					res.a = base.a;
					return res;
				}
			#endif

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed4 _GradColA;
			fixed4 _GradColB;
			fixed _Intensity;

            // detail tex - jules
            sampler2D _DetailTex;
            fixed _DetailIntensity;
            float4 _DetailParams;
            // end - jules

			v2f vert (appdata v)
			{
				UNITY_SETUP_INSTANCE_ID(v);
				v2f o;
				UNITY_INITIALIZE_OUTPUT(v2f, o);
				UNITY_TRANSFER_INSTANCE_ID(v,o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				o.normal = worldNormal;
				o.worldPos = worldPos;
				o.color = v.color;

				// SH/ambient and vertex lights
				#if UNITY_SHOULD_SAMPLE_SH
					float3 shlight = ShadeSH9 (float4(worldNormal,1.0));
				#else
					float3 shlight = float3(0,0,0);
				#endif
				#ifdef VERTEXLIGHT_ON
					shlight += Shade4PointLights (
						unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
						unity_LightColor[0].rgb, unity_LightColor[1].rgb, unity_LightColor[2].rgb, unity_LightColor[3].rgb,
						unity_4LightAtten0, worldPos, worldNormal );
				#endif
				o.vlight = shlight;

				TRANSFER_SHADOW(o);
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}
			
			fixed4 frag (v2f IN) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(IN);

				#ifndef USING_DIRECTIONAL_LIGHT
					fixed3 lightDir = normalize(UnityWorldSpaceLightDir(IN.worldPos));
				#else
					fixed3 lightDir = _WorldSpaceLightPos0.xyz;
				#endif

				#ifdef UNITY_COMPILER_HLSL
					SurfaceOutput o = (SurfaceOutput)0;
				#else
					SurfaceOutput o;
				#endif

				o.Albedo = tex2D(_MainTex, IN.uv);
				o.Normal = IN.normal; // this can be normalized if needed
				fixed4 c = fixed4(o.Albedo, o.Alpha);
				UNITY_LIGHT_ATTENUATION(atten, IN, IN.worldPos)
				c.rgb *= IN.vlight;
				c.rgb += LightingLambertBase(o, lightDir, atten).rgb;

				fixed2 uv = IN.color.rg;
				#if defined(GRAD_RADIAL)
					fixed dist = saturate(_Intensity * length(uv - fixed2(0.5, 0.5)));
				#elif defined(GRAD_VERTICAL)
					fixed dist = saturate(_Intensity * length(uv.x - fixed2(0.5, 0.5)));
				#elif defined(GRAD_HORIZONTAL)
					fixed dist = saturate(_Intensity * length(uv.y - fixed2(0.5, 0.5)));
				#elif defined(GRAD_TOP)
					fixed dist = saturate(_Intensity * (1 - uv.y));
				#else
					fixed dist = saturate(_Intensity * uv.y);
				#endif

				fixed4 grad = lerp(_GradColA, _GradColB, dist);
				fixed4 blended = lerp(c, Blend(c, grad), grad.a);
				c = lerp(c, blended, IN.color.b);
				// c = blended;

                // detail tex - jules (using uvs from vertex color (with user defined tiling/offset)
                fixed4 detail = tex2D(_DetailTex, uv * _DetailParams.xy + _DetailParams.zw);
                fixed4 intense = lerp (c, c * detail, _DetailIntensity);
                c = lerp (c, intense, IN.color.b);
                // end - jules

				// sample the texture
				// apply fog
				UNITY_APPLY_FOG(IN.fogCoord, c);
				return c;
			}
			ENDCG
		}
	}
	CustomEditor "MaskedGradientShaderGUI"
	FallBack "Mobile/Diffuse"
}
