﻿Shader "Stencil/MaskTexture"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
        _Color ("Top Color", Color) = (1,1,1,0)

		_Stencil ("Stencil ID", Float) = 1
	}
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue"="Geometry+1"}
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

//        ColorMask 0
//        ZWrite off

        Stencil {
            Ref [_Stencil]
            Comp GEqual
            Pass replace
        }

        Pass {        
			CGPROGRAM
			#pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata 
			{
                fixed4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f 
			{
                fixed4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            sampler2D _MainTex;
            fixed4 _MainTex_ST;
            fixed4 _Color;

            v2f vert(appdata v) 
			{
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
                return col;
            }
        	ENDCG
        }
    } 
}