Shader "Custom/SpriteFogLayout"
{
      Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_LayoutTex ("Layout", 2D) = "white" {}
        _LayoutPower("Layout Power", Range(0.0, 1.0)) = 1.0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "TransparentCutout"
		}
		
		Lighting Off


		Pass
		{  
			ZWrite Off 
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag 
            #pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
             	UNITY_FOG_COORDS(1)
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			sampler2D _LayoutTex;
			float4 _LayoutTex_ST;

            float _LayoutPower;

			v2f vert (appdata_t v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);

				return o;
			}

			half4 frag (v2f i) : COLOR
			{
				half4 tex = tex2D(_MainTex, i.texcoord);
				half4 layout = tex2D(_LayoutTex, i.texcoord);
				half4 col = i.color * tex;
				
				UNITY_APPLY_FOG(i.fogCoord, col);
               
                col.a = i.color.a * tex.a;
                col.a *= step(layout.r, _LayoutPower);
                
				return col;
			}
				
			ENDCG
		}
	}
}