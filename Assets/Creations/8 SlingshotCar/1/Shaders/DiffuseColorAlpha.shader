﻿// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Mobile/Diffuse(Color)Alpha" {
Properties {
}
SubShader {
	Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
	LOD 150
	Blend SrcAlpha OneMinusSrcAlpha

CGPROGRAM
#pragma surface surf Lambert noforwardadd

struct Input {
	float4 color : COLOR;
};

void surf (Input IN, inout SurfaceOutput o) {
	o.Albedo = IN.color.rgba;
	o.Alpha = 0.5;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}