﻿Shader "Masked/Mask" {
 
	SubShader {
    // draw after all opaque objects (queue = 2001):
    Tags { "Queue"="Geometry+1" }

    ZTest Always
	ZWrite Off 

    Pass {
      Blend Zero One // keep the image behind it
    }
  } 
}