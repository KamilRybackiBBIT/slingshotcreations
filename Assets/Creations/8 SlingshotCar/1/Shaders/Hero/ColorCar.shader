Shader "Custom/ColorCar"
{
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
        _Base ("Base", 2D) = "white" {}
        _MaskTex ("Mask", 2D) = "white" {}
        _PartsMaskTex ("PartsMask", 2D) = "white" {}
        _NormTex ("Normalmap", 2D) = "norm" {}
        
        _WheelArchesColor ("Wheel Arches Color", Color) = (1,1,1,1)
        _WheelArchesTex ("Wheel Arches Texture", 2D) = "white" {}
        _WheelArchesFade ("Wheel Arches Fade", Range(0,1)) = 0.0
        
        _HoodColor ("Hood Color", Color) = (1,1,1,1)
        _HoodTex ("Hood Texture", 2D) = "white" {}
        _HoodFade ("Hood fade", Range(0,1)) = 0.0
        
        _Body1Color ("Body1 Color", Color) = (1,1,1,1)
        _Body1Tex ("Body1 Texture", 2D) = "white" {}
        _Body1Fade ("Body1 fade", Range(0,1)) = 0.0
        
        _Body2Color ("Body2 Color", Color) = (1,1,1,1)
        _Body2Tex ("Body2 Texture", 2D) = "white" {}
        _Body2Fade ("Body2 fade", Range(0,1)) = 0.0
        
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 150
        CGPROGRAM
#pragma surface surf Lambert noforwardadd
        float4 _Color;
        sampler2D _MaskTex;
        sampler2D _Base;
        sampler2D _NormTex;
        sampler2D _PartsMaskTex;
        
        float4 _WheelArchesColor;
        float4 _HoodColor;
        float4 _Body1Color;
        float4 _Body2Color;
        
        sampler2D _WheelArchesTex;
        sampler2D _HoodTex;
        sampler2D _Body1Tex;
        sampler2D _Body2Tex;
        
        half _WheelArchesFade;
        half _HoodFade;
        half _Body1Fade;
        half _Body2Fade;
//Biały - wheel arches
//Czerwony - hood
//Zielony - body1
//Niebieski - body2
        struct Input {
            float2 uv_Base;
            float2 uv_NormTex;
        };
        void surf (Input IN, inout SurfaceOutput o) {
            
            float4 mask = tex2D(_MaskTex, IN.uv_Base);
            float4 partsMask = tex2D(_PartsMaskTex, IN.uv_Base);
            fixed4 base = tex2D(_Base, IN.uv_Base);
            
            fixed4 wheelArchesTex = tex2D(_WheelArchesTex, IN.uv_Base);
            fixed4 hoodTex = tex2D(_HoodTex, IN.uv_Base);
            fixed4 body1Tex = tex2D(_Body1Tex, IN.uv_Base);
            fixed4 body2Tex = tex2D(_Body2Tex, IN.uv_Base);
            
            fixed4 wheelArchesC = lerp(_WheelArchesColor, wheelArchesTex, _WheelArchesFade);
            fixed4 hoodC = lerp(_HoodColor, hoodTex, _HoodFade);
            fixed4 body1C = lerp(_Body1Color, body1Tex, _Body1Fade);
            fixed4 body2C = lerp(_Body2Color, body2Tex, _Body2Fade);
            
            o.Albedo = lerp(base, _Color, mask);
            
            o.Albedo = lerp(o.Albedo, hoodC, partsMask.r);
            o.Albedo = lerp(o.Albedo, body1C, partsMask.g);
            o.Albedo = lerp(o.Albedo, body2C, partsMask.b);
            o.Albedo = lerp(wheelArchesC, o.Albedo, partsMask.a);
            
            o.Normal = UnpackNormal (tex2D (_NormTex, IN.uv_NormTex));
        }
        ENDCG
    }
    Fallback "Mobile/Diffuse"
}
