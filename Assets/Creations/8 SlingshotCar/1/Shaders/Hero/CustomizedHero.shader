Shader "Hero/CustomizedHero"

{
	Properties {
		_MaskTex ("Mask", 2D) = "white" {}
		_Top ("Top", 2D) = "white" {}
		_Middle ("Middle", 2D) = "white" {}
		_Bottom ("Bottom", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Opaque"
			"IgnoreProjector" = "True"
		}

		LOD 150

		Pass {
	        ZWrite On
			ColorMask A
	    }

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd

		float4 _Color;
		sampler2D _MaskTex;
		sampler2D _Top;
		sampler2D _Middle;
		sampler2D _Bottom;

		struct Input {
			float2 uv_Top;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			
			float4 mask = tex2D(_MaskTex, IN.uv_Top);
			fixed4 top = tex2D(_Top, IN.uv_Top) * _Color.rgba;
			fixed4 middle = tex2D(_Middle, IN.uv_Top) * _Color.rgba;
			fixed4 bottom = tex2D(_Bottom, IN.uv_Top) * _Color.rgba;
			
			fixed4 c = 0;
			fixed maskVal = mask.r + mask.g + mask.b;
			c += step(1, mask.r) * top.rgba;
			c += step(1, mask.g) * middle.rgba;
			c += step(1, mask.b) * bottom.rgba;
			c += step(maskVal, 0) * top.rgba;
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}

	Fallback "Mobile/Diffuse"
}