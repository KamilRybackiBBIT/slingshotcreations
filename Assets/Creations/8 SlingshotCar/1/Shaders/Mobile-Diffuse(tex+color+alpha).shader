﻿Shader "Mobile/Diffuse(tex+color+alpha)"

{
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
    // Render into depth buffer only
    Pass {
        ColorMask 0
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
    }

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd alpha

		float4 _Color;
		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color.rgba;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}

	Fallback "Mobile/Diffuse"
}

//{
//	Properties {
//	    _Color ("Main Color", Color) = (1,1,1,1)
//	    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
//	}
//	SubShader {
//	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
//	    LOD 200
//
//	    // extra pass that renders to depth buffer only
//	    Pass {
//	        ZWrite On
//	        ColorMask 0
//	    }
//
//	    // paste in forward rendering passes from Transparent/Diffuse
//	    UsePass "Transparent/Diffuse/FORWARD"
//	}
//	Fallback "Mobile/Diffuse"
//}

//{
//	Properties
//	{
//		_Color("Color", Color) = (1, 1, 1, 1)
//		_MainTex("Base (RGB)", 2D) = "white" {}
//	}
//
//	SubShader
//	{
//		Tags
//		{
//			"Queue"="Transparent" 
//			"IgnoreProjector"="True" 
//			"RenderType"="Transparent" 
//			"PreviewType"="Plane"
//		}
//
////		Fog { Mode Global }
////		LOD 150
//
//		CGPROGRAM
//		#pragma surface surf Lambert alpha
//
//		struct Input
//		{
//			float4 color : Color;
//			float2 uv_MainTex;
//		};
//
//		float4 _Color;
//		sampler2D _MainTex;
//		void surf (Input IN, inout SurfaceOutput o) 
//		{
//			o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgba * _Color.rgb;
//			o.Alpha = _Color.a;
//		}
//		ENDCG
//	} 
//	FallBack "Mobile/Diffuse"
//}﻿