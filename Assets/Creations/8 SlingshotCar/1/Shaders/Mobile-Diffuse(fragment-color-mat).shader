// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Mobile/Diffuse(Fragment Color-mat)" {
Properties {
	_Color ("Color", Color) = (1,1,1,1)
}
SubShader
    {
        Tags { "LightMode"="ForwardBase" }
        LOD 100

        Pass
        {
            Tags {"LightMode"="ForwardBase"}
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                fixed4 diff : COLOR0;
                float4 vertex : SV_POSITION;
                half3 n : NORMAL;
            };

            v2f vert (appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.texcoord;
                o.n = UnityObjectToWorldNormal(v.normal);

                return o;
            }
            
            fixed4 _Color;

            fixed4 frag (v2f i) : SV_Target
            {
            	half nl = max(0, dot(i.n, _WorldSpaceLightPos0.xyz));
                fixed4 diff = nl * _LightColor0;
                diff.rgb += ShadeSH9(half4(i.n,1));

                fixed4 col = _Color;
                col *= diff;
                return col;
            }
            ENDCG
        }
    }

Fallback "Mobile/VertexLit"
}