﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UI/AlphaMaskFont"
{
	//Screen range for 1 (top) to -1 (bottom), 0 at middle
	//x - top start blur
	//y - top blur range
	//z - bottom start blur (should be negative)
	//w - bottom blur range (should be negative)

    Properties
    {
    	[PerRendererData]_MainTex ("Base (RGB)", 2D) = "white" { }

    	_TopStart ("Top Start", Range(1.0, -1.0)) = 0.6
    	_TopRange ("Alpha Top Range", Range(0.0, 10)) = 0.4
    	_BottomStart ("Bottom Start", Range(-1.0, 1.0)) = -0.6
    	_BottomRange ("Alpha Bottom Range", Range(0.0, 10)) = 0.4

    	_LeftStart ("Left Start", Range(-1.0, 1.0)) = -0.6
    	_LeftRange ("Alpha Left Range", Range(0.0, 10)) = 0.4
    	_RightStart ("Right Start", Range(1.0, -1.0)) = 0.6
    	_RightRange ("Alpha Right Range", Range(0.0, 10)) = 0.4

    	[HideInInspector]_StencilComp ("Stencil Comparison", Float) = 8
    	[HideInInspector]_Stencil ("Stencil ID", Float) = 0
    	[HideInInspector]_StencilOp ("Stencil Operation", Float) = 0
    	[HideInInspector]_StencilWriteMask ("Stencil Write Mask", Float) = 255
    	[HideInInspector]_StencilReadMask ("Stencil Read Mask", Float) = 255
    	[HideInInspector]_ColorMask ("Color Mask", Float) = 15
    }
    SubShader
    {
        Tags{
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
//            "CanUseSpriteAtlas" = "True"
        }
        Cull Off
        Lighting Off
        ZWrite Off
        Fog{ Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

        Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            sampler2D _MainTex;

			half _TopStart;
			half _TopRange;
			half _BottomStart;
			half _BottomRange;

			half _LeftStart;
			half _LeftRange;
			half _RightStart;
			half _RightRange;

            struct appdata
            {
                float4 vertex : POSITION;
                half4 color : COLOR;
                half4 uv : TEXCOORD;
            };
            	
            struct v2f
            {
                float4 vertex : SV_POSITION;
                half4 uv : TEXCOORD;
                float4 position : COLOR1;
                half4 color : COLOR;
            };
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);  
                o.position = o.vertex;
                o.color = v.color;
                o.uv = v.uv;
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
            	half4 result = i.color;
            	result.a *= tex2D (_MainTex, i.uv).a;

            	half vAlpha = 1 - lerp(0, 1, (i.position.y - _TopStart) / _TopRange);
            	vAlpha = min(vAlpha, 1 - lerp(0, 1, (i.position.y - _BottomStart) / -_BottomRange));

            	half hAlpha = 1 - lerp(0, 1, (i.position.x - _RightStart) / _RightRange);
            	hAlpha = min(hAlpha, 1 - lerp(0, 1, (i.position.x - _LeftStart) / -_LeftRange));

            	result.a = min(min(vAlpha, hAlpha), result.a);

	         	return result;
         	}

            ENDCG
        }
    }
}