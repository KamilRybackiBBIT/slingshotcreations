// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Effects/SimpleEQ" {
Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_BottomColor("Bottom Color", Color) = (1,1,1,1)
		_TopColor("Top Color", Color) = (1,1,1,1)
		_BottomOffset("Bottom Screen Offset", float) = 0
		_TopOffset("Top Screen Offset", float) = 0
	}

	SubShader
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane"}
        Blend SrcAlpha One
        Cull Off Lighting Off Fog { Color (0,0,0,0) }


		Pass
		{
		CGPROGRAM

        #pragma vertex vert
        #pragma fragment frag
        // make fog work
        #pragma multi_compile_fog 
		#include "UnityCG.cginc"

		struct appdata_t
		{
			float4 position	: POSITION;
			float2 uv		: TEXCOORD0;
		};

		struct v2f
		{
			float4 position	: SV_POSITION;
			float2 uv		: TEXCOORD0;
			half4 color		: COLOR;
		};

		sampler2D _MainTex;
		float4 _MainTex_ST;
		half4 _BottomColor;
		half4 _TopColor;
		float _BottomOffset;
		float _TopOffset;

		v2f vert(appdata_t IN)
		{
			v2f OUT;
			OUT.position = UnityObjectToClipPos(IN.position);
			OUT.uv = TRANSFORM_TEX(IN.uv, _MainTex);
			
			float factor = (OUT.position.y) / OUT.position.w;
			factor = (factor + 1) / 2;
			factor = (factor - _BottomOffset) / ((1 - _TopOffset) - _BottomOffset);
			factor = clamp(factor, 0, 1);
			
			OUT.color = lerp(_BottomColor, _TopColor, factor);

			return OUT;
		}

		half4 frag(v2f IN) : SV_Target
		{
			half4 col = tex2D(_MainTex, IN.uv);
			
			col *= IN.color;
			
			return col;
		}

		ENDCG
		}
	}
}
