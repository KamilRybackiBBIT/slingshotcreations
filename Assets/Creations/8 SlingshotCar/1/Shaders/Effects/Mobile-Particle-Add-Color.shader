// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Additive Particle shader. Differences from regular Additive Particle one:
// - no Tint color
// - no Smooth particle support
// - no AlphaTest
// - no ColorMask

Shader "Mobile/Particles/Additive_Color" {
Properties {
	        _Color ("Color", Color) = (0, 0, 0, 1)
            _MainTex ("Particle Texture", 2D) = "white" {}
        }
        SubShader {
            Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
            Blend SrcAlpha One
            Cull Off Lighting Off ZWrite Off Fog { Color (0,0,0,0) }
           
           Pass
            {
            CGPROGRAM
            #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"
 
                fixed4 _Color;
                sampler2D _MainTex;
                struct appdata_t {
                    float4 vertex : POSITION;
                    float2 texcoord: TEXCOORD0;
                    float4 color : COLOR;
                };
 
                struct v2f {
                    float4 vertex : SV_POSITION;
                    float2 uv : TEXCOORD1;
                    float4 color : COLOR;
                };
 
                float4 _MainTex_ST;
                v2f vert(appdata_t v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                    o.color = v.color;
                    return o;
                }
 
                fixed4 frag(v2f i) : SV_Target
                {
                    fixed4 texcol = tex2D(_MainTex, i.uv);
 
                    fixed3 rgb = i.color.rgb * texcol.rgb * _Color.rgb;
                    fixed alpha = i.color.a * texcol.a * _Color.a;
                    rgb = rgb * alpha;
                   
                    fixed4 output;
                    output.rgb = rgb;
                    output.a = 1.0;
                    return output;
                }
           
            ENDCG
            }
        }
 }    