﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/MultipleTexturesShader"
{
    Properties
    {
        _Color ("Top Color", Color) = (1,1,1,0)
        _MainTex ("Texture", 2D) = "white" {}

        _Add1Color ("Add 1 Color", Color) = (1,1,1,0)
        _Add1Tex ("Add 1 texture", 2D) = "white" {}

        _Add2Color ("Add 2 Color", Color) = (1,1,1,0)
        _Add2Tex ("Add 2 texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha
        Cull off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog 
            
            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                fixed3 normal : NORMAL;
            };

            struct v2f
            {
                fixed4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 Add1uv : TEXCOORD1;
                float2 Add2uv : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            }; 

            fixed4 _Color;
            fixed4 _Add1Color;
            fixed4 _Add2Color;

            sampler2D _MainTex;
            fixed4 _MainTex_ST;

            sampler2D _Add1Tex;
            fixed4 _Add1Tex_ST;

            sampler2D _Add2Tex;
            fixed4 _Add2Tex_ST;

            v2f vert (appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.Add1uv = TRANSFORM_TEX(v.uv, _Add1Tex);
                o.Add2uv = TRANSFORM_TEX(v.uv, _Add2Tex);

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;

                fixed4 add1Col = tex2D(_Add1Tex, i.Add1uv);
                add1Col *= add1Col.a * _Add1Color.a * _Add1Color * col.a;

                fixed4 add2Col = tex2D(_Add2Tex, i.Add2uv);
                add2Col *= add2Col.a * _Add2Color.a * _Add2Color * col.a;

                col = col + add1Col + add2Col;

                UNITY_APPLY_FOG(i.fogCoord, col);

                return col;
            }
            ENDCG
        }
    }
}