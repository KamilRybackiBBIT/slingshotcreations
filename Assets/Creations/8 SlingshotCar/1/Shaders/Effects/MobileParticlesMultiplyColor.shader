﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Multiply Particle shader. Differences from regular Multiply Particle one:
// - no Smooth particle support
// - no AlphaTest
// - no ColorMask

Shader "Mobile/Particles/MultiplyColor" {
	Properties {
		_Color ("Text Color", Color) = (1,1,1,1)
	}

	SubShader
	{
	    Tags { "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox"}
	    Blend Zero SrcColor
	    Cull Off Lighting Off ZWrite Off Fog { Color (1,1,1,1) }

	    Pass
	    { 
	        CGPROGRAM
	        #pragma vertex vert
	        #pragma fragment frag
	        // make fog work
	        #pragma multi_compile_fog 
	        
	        #include "UnityCG.cginc"

	       

	        struct appdata
	        {
	            fixed4 vertex : POSITION;
	            float2 uv : TEXCOORD0;
	            fixed3 normal : NORMAL;
	        };

	        struct v2f
	        {
	            fixed4 vertex : SV_POSITION;
	            float2 uv : TEXCOORD0;
	            UNITY_FOG_COORDS(1)
	        }; 

	        fixed4 _Color;
	        
	        v2f vert (appdata v)
	        {
	            v2f o;
	            o.vertex = UnityObjectToClipPos(v.vertex);
	            UNITY_TRANSFER_FOG(o,o.vertex);
	            return o;
	        }
	        
	        fixed4 frag (v2f i) : SV_Target
	        {
	        	fixed4 col = _Color;
                UNITY_APPLY_FOG(i.fogCoord, col);
	            return col;
	        }
	        ENDCG
	    }
	}
}
