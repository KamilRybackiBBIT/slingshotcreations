﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/RoadHighlightStencilShader"
{
    Properties
    {
        _Color ("Top Color", Color) = (1,1,1,0)
        _MainTex ("Texture", 2D) = "white" {}

        _Add1Color ("Add 1 Color", Color) = (1,1,1,0)
        _Add1Tex ("Add 1 texture", 2D) = "white" {}

        _Add2Color ("Add 2 Color", Color) = (1,1,1,0)
        _Add2Tex ("Add 2 texture", 2D) = "white" {}

        _Zpos ("Zpos", Float) = 1.5
        _Z2pos ("Z2pos", Float) = 1.5

        _Stencil ("Stencil ID", Float) = 1

        _HighlightTarget ("Highlight Target", Float) = 0
        _HighlightDistStart ("Highlight Dist Start", Float) = 0
        _HighlightDistEnd ("Highlight Dist End", Float) = 1
        _LeftColor ("Left Color", Color) = (1,1,1,1)
        _LeftTex ("Left Texture", 2D) = "white" {}
        _RightColor ("Right Color", Color) = (1,1,1,1)
        _RightTex ("Right Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha
        Cull off

        Stencil {
            Ref [_Stencil]
            Comp always
            Pass replace
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog 
            
            #include "UnityCG.cginc"

            float _Zpos;
            float _Z2pos;

            struct appdata
            {
                fixed4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                fixed3 normal : NORMAL;
            };

            struct v2f
            {
                fixed4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 Add1uv : TEXCOORD1;
                float2 Add2uv : TEXCOORD2;
                float2 LeftUV : TEXCOORD3;
                float2 RightUV : TEXCOORD4;
                fixed t : TEXCOORD5;
                float dist : TEXTCOORD6;
                UNITY_FOG_COORDS(7)
            }; 

            fixed4 _Color;
            fixed4 _Add1Color;
            fixed4 _Add2Color;

            sampler2D _MainTex;
            fixed4 _MainTex_ST;

            sampler2D _Add1Tex;
            fixed4 _Add1Tex_ST;

            sampler2D _Add2Tex;
            fixed4 _Add2Tex_ST;

            float _HighlightTarget;
            float _HighlightDistStart;
            float _HighlightDistEnd;

            sampler _LeftTex;
            fixed4 _LeftTex_ST;
            fixed4 _LeftColor;

            sampler _RightTex;
            fixed4 _RightTex_ST;
            fixed4 _RightColor;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.Add1uv = TRANSFORM_TEX(v.uv, _Add1Tex);
                o.Add2uv = TRANSFORM_TEX(v.uv, _Add2Tex);
                o.LeftUV = TRANSFORM_TEX(v.uv, _LeftTex);
                o.RightUV = TRANSFORM_TEX(v.uv, _RightTex);

                float3 wPos = mul(unity_ObjectToWorld, v.vertex).xyz;

                UNITY_TRANSFER_FOG(o,o.vertex);

                o.t = clamp((wPos.z - _Z2pos) / (_Zpos - _Z2pos),0.0,1.0);

//                float d = distance(wPos.z, _HighlightTarget);
//                o.dist = 1 - clamp((d - _HighlightDistStart) / (_HighlightDistEnd - _HighlightDistStart), 0, 1);
                o.dist = 1 - saturate(distance(wPos.z, _HighlightTarget) - _HighlightDistStart / (_HighlightDistEnd - _HighlightDistStart));

                o.vertex.x = o.vertex.x * (2 - o.t);

                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = (tex2D(_MainTex, i.uv) * _Color);
                fixed4 left = tex2D(_LeftTex, i.LeftUV).a * _LeftColor;
                fixed4 right = tex2D(_RightTex, i.RightUV).a * _RightColor;

                col.a = col.a * i.t * i.t * i.t;

                fixed4 add1Col = tex2D(_Add1Tex, i.Add1uv);
                add1Col *= add1Col.a  * _Add1Color;
                add1Col.a = add1Col.a * i.t * i.t;

                fixed4 add2Col = tex2D(_Add2Tex, i.Add2uv);
                add2Col *= add2Col.a  * _Add2Color;
                add2Col.a = add2Col.a * i.t;

                col = col + add1Col + add2Col;
                col = lerp(col, lerp(col, left, left.a), i.dist);
                col = lerp(col, lerp(col, right, right.a), i.dist);

                UNITY_APPLY_FOG(i.fogCoord, col);

                return col;
            }
            ENDCG
        }
    }
}