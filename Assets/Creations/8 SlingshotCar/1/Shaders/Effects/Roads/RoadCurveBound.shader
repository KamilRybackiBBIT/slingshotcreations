﻿Shader "Custom/RoadCurveBound"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _FadeLayout ("Albedo (RGB)", 2D) = "white" {}
        _CamFadeDist ("Camera Fade Distance", Float) = 1.5
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog 
            
            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                fixed3 normal : NORMAL;
            };

            struct v2f
            {
                fixed4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float2 Layoutuv : TEXCOORD2;
                fixed t : TEXCOORD4;
            }; 

            fixed4 _Color;
            half _CamFadeDist;

            sampler2D _MainTex;
            fixed4 _MainTex_ST;

            sampler2D _FadeLayout;
            fixed4 _FadeLayout_ST;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.Layoutuv = TRANSFORM_TEX(v.uv, _FadeLayout);

                UNITY_TRANSFER_FOG(o,o.vertex);

                o.t = clamp(0, 1, length(ObjSpaceViewDir(v.vertex)) - _CamFadeDist);

                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;

                UNITY_APPLY_FOG(i.fogCoord, col);
                
                fixed4 add2Col = tex2D(_FadeLayout, i.Layoutuv);
                col.a *= add2Col.r * i.t;

                return col;
            }
            ENDCG
        }
    }
}
