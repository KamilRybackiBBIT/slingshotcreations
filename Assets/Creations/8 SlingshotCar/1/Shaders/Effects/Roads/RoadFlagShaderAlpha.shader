﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/RoadFlagShaderAlpha"
{
    Properties
    {
        _Color ("Top Color", Color) = (1,1,1,0)
        _MainTex ("Texture", 2D) = "white" {}

        _WorldPos ("WorldPos", Vector) = (0,0,0,0)
        _Distance ("Distance", Float) = 1.5
        _Height ("Height", Float) = 1.5
        _WaveFrequency ("WaveFrequency", Float) = 1
        _WaveHeight ("WaveHeight", Float) = 10
        
        _ZFadeStart ("Z Fade Start", Float) = 1.5
        _ZFadeDistance ("Z Fade Distance", Float) = 1.5
        
        _FadePower ("Fade Power", Range (0.0, 1.0)) = 0
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane"}
        Blend SrcAlpha One
        Cull Off Lighting Off Fog { Color (0,0,0,0) }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog 
            
            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                fixed3 normal : NORMAL;
            };

            struct v2f
            {
                fixed4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
				float4 color : Color;
            }; 

            fixed4 _Color;

            sampler2D _MainTex;
            fixed4 _MainTex_ST;

            float4 _WorldPos;
            float _Distance;
            float _Height;
            float _WaveFrequency;
            float _WaveDistance;
            float _WaveHeight;
            
            half _ZFadeStart;
            half _ZFadeDistance;
            half _FadePower;
            
            v2f vert (appdata v)
            {
                v2f o;

                float3 wPos = mul(unity_ObjectToWorld, v.vertex).xyz;

                float dist = distance(_WorldPos, wPos);
                float graviti = clamp(dist / _Distance, 0, 1);
                float sinePos = sin((wPos.z - _WorldPos.z) * _WaveFrequency);
                half clampV = clamp((wPos.z - (_WorldPos.z + _ZFadeStart)) / _ZFadeDistance, 0, 1) * _FadePower;

                v.vertex.y = v.vertex.y + (_Height * (graviti * graviti)) + (_WaveHeight * sinePos);
                //v.vertex.x *=  1 - clampV;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color.a = 1 - clampV;
                
                UNITY_TRANSFER_FOG(o,o.vertex);
                
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
                col.a *= i.color.a;
                
                UNITY_APPLY_FOG(i.fogCoord, col);

                return col;
            }
            ENDCG
        }
    }
}