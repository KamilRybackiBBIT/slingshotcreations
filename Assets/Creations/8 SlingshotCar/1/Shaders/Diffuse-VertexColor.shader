// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Mobile/Diffuse-VertexColorBlend" {
Properties {
		_MainTex ("Black", 2D) = "white" {}
        _MainTexBumpMap ("BlackBump", 2D) = "bump" {}
		_MainTexBumpMapPower ("BlackBump Power", Float ) = 1
		
		_RedTex ("Red", 2D) = "white" {}     
		_RedPower ("_RedPower", Range(0,1) ) = 1   
        _RedTexBumpMap ("RedBump", 2D) = "bump" {}
		_RedTexBumpMapPower ("RedBump Power", Float ) = 1

		_GreenTex ("Green", 2D) = "white" {}
		_GreenPower ("_GreenPower", Range(0,1) ) = 1
        _GreenTexBumpMap ("GreenBump", 2D) = "bump" {}
		_GreenTexBumpMapPower ("GreenBump Power", Float ) = 1
		
		_BlueTex ("Blue", 2D) = "white" {}
		_BluePower ("_BluePower", Range(0,1) ) = 1
        _BlueTexBumpMap ("BlueBump", 2D) = "bump" {}
		_BlueTexBumpMapPower ("BlueBump Power", Float ) = 1
}
SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 150


    CGPROGRAM
        #pragma surface surf Lambert noforwardadd
        sampler2D _MainTex;
        sampler2D _RedTex;
        sampler2D _GreenTex;
        sampler2D _BlueTex;
        
        sampler2D _MainTexBumpMap;
        sampler2D _RedTexBumpMap;
        sampler2D _GreenTexBumpMap;
        sampler2D _BlueTexBumpMap;
        
        float4 _Color;
        float _RedPower;
        float _GreenPower;
        float _BluePower;
        
        half _MainTexBumpMapPower;
        half _RedTexBumpMapPower;
        half _GreenTexBumpMapPower;
        half _BlueTexBumpMapPower;
        
        struct Input {
            float2 uv_MainTex;
            float2 uv_RedTex;
            float2 uv_GreenTex;
            float2 uv_BlueTex;
            float4 color : COLOR;
        };
        
        void surf (Input IN, inout SurfaceOutput o) {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
            fixed4 r = tex2D(_RedTex, IN.uv_RedTex);
            fixed4 g = tex2D(_GreenTex, IN.uv_GreenTex);
            fixed4 b = tex2D(_BlueTex, IN.uv_BlueTex);
            
            half3 cN = UnpackScaleNormal(tex2D(_MainTexBumpMap, IN.uv_MainTex), _MainTexBumpMapPower);
            half3 rN = UnpackScaleNormal(tex2D(_RedTexBumpMap, IN.uv_RedTex), _RedTexBumpMapPower);
            half3 gN = UnpackScaleNormal(tex2D(_GreenTexBumpMap, IN.uv_GreenTex), _GreenTexBumpMapPower);
            half3 bN = UnpackScaleNormal(tex2D(_BlueTexBumpMap, IN.uv_BlueTex), _BlueTexBumpMapPower);
            
            o.Albedo = c;
            o.Albedo = lerp(o.Albedo, r, IN.color.r * _RedPower);
            o.Albedo = lerp(o.Albedo, g, IN.color.g * _GreenPower);
            o.Albedo = lerp(o.Albedo, b, IN.color.b * _BluePower);
            
            o.Normal = cN;
            o.Normal = lerp(o.Normal, rN, IN.color.r * _RedPower);
            o.Normal = lerp(o.Normal, gN, IN.color.g * _GreenPower);
            o.Normal = lerp(o.Normal, bN, IN.color.b * _BluePower);
            
        }
        ENDCG
    }

Fallback "Mobile/VertexLit"
}