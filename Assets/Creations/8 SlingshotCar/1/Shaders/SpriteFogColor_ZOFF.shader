﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/SpriteFogColor_ZOFF"
{
	Properties
	{
    	_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_FogMultipler ("Fog Multipler", Range(0,1.0)) = 1.0
		_MainTex ("Texture", 2D) = "white" {}
	} 

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "TransparentCutout"
		}
		
		Lighting Off
		Cull Off

		Pass
		{  
			ZTest Always
			ZWrite Off 
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag 
            #pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
             	UNITY_FOG_COORDS(1)
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _FogMultipler;

    	fixed4 _Color;

			v2f vert (appdata_t v)
			{
				v2f o;

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);

				return o;
			}

			half4 frag (v2f i) : COLOR
			{
				half4 tex = tex2D(_MainTex, i.texcoord);
				half4 col = i.color * tex;

				col.rgb = _Color;
                col.a = i.color.a*tex.a*_Color.a;

                half4 beforeFog = col;

				UNITY_APPLY_FOG(i.fogCoord, col);

				col = lerp(beforeFog, col, _FogMultipler);
				 
				return col;
			}
				
			ENDCG
		}
	}
}