﻿Shader "Custom/Snow Glitter Optimized" {
Properties 
{
	_Color ("Color", Color) = (1,1,1,1)
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_SpecularGlitterTex ("Specular glitter", 2D) = "white" {}
	_SpecularPower ("Specular power (0 - 5)", Range(0, 5)) = 1.5
	_SpecularContrast ("Specular contrast (1 - 3)", Range(1, 3)) = 1
	_SpecularMap ("Glitter map", 2D) = "white" {}
	_SpecularColor ("Glitter color", Color) = (1,1,1,1)
	_GlitterPower ("Glitter power (0 - 10)", Range(0, 10)) = 2
	_GlitterContrast ("Glitter contrast (1 - 3)", Range(1, 3)) = 1.5
	_GlitterSpeed ("Glittery speed (0 - 1)", Range(0, 1)) = 0.5
	_Szpachla ("Szpachla", Range (0, 10)) = 1
	_GlitterMaskScale ("Glittery & mask dots scale", Range(0.1, 8)) = 2.5
	_MaskAdjust ("Mask adjust (0.5 - 1.5)", Range(0.5, 1.5)) = 1
}
SubShader
{ 
	Tags { "RenderType"="Opaque" }
	LOD 250
Pass {
	Name "FORWARD"
	Tags { "LightMode" = "ForwardBase" }
	
	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#pragma target 2.0

	#pragma multi_compile_fog
	#pragma multi_compile_fwdbase nodynlightmap nolightmap

	#define UNITY_PASS_FORWARDBASE
	#include "UnityCG.cginc"
	#include "Lighting.cginc"
	#include "AutoLight.cginc"

	fixed4 _Color;

	sampler2D _BumpMap;
	float4 _BumpMap_ST;
	half _Shininess;

	sampler2D _SpecularGlitterTex; 
	float4 _SpecularGlitterTex_ST;
	float _SpecularPower;
	float _GlitterPower;
	sampler2D _NormalMap; 
	float4 _NormalMap_ST;
	float4 _SpecularColor;
	float _GlitterMaskScale;
	float _GlitterSpeed;
	sampler2D _SpecularMap; 
	float4 _SpecularMap_ST;
	float _GlitterContrast;
	float _SpecularContrast;
	float _MaskAdjust;
	float _Szpachla;

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
		float4 tangent : TANGENT;
		float3 normal : NORMAL;
	};

	struct v2f
	{
		float4 pos : SV_POSITION;
		// worldTangent, worldBinormal, worldNormal, worldPos
		float4 tSpace0 : TEXCOORD0; // X
		float4 tSpace1 : TEXCOORD1; // Y
		float4 tSpace2 : TEXCOORD2; // Z
		float4 mixedUvA : TEXCOORD3;
		float4 mixedUvB : TEXCOORD4;
		float4 halfDir : TEXCOORD5; // XYZ - half, W - spec pow
		SHADOW_COORDS(7)
		UNITY_FOG_COORDS(8)
		UNITY_VERTEX_INPUT_INSTANCE_ID
  		UNITY_VERTEX_OUTPUT_STEREO
	};

	v2f vert(appdata v)
	{
		UNITY_SETUP_INSTANCE_ID(v);
		v2f o;
  		UNITY_INITIALIZE_OUTPUT(v2f, o);
		UNITY_TRANSFER_INSTANCE_ID(v,o);
  		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

		o.pos = UnityObjectToClipPos(v.vertex);
		fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
		float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

		fixed3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
		fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
		fixed3 worldBinormal = cross(worldNormal, worldTangent) * tangentSign;
		o.tSpace0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);
		o.tSpace1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);
		o.tSpace2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);

		float3 viewDirForLight = UnityWorldSpaceViewDir(worldPos);
		float3 worldLightDir = UnityWorldSpaceLightDir(worldPos);
		float3 worldViewDir = UnityWorldSpaceViewDir(worldPos);
		viewDirForLight = normalize(normalize(viewDirForLight) + normalize(worldLightDir));
		o.halfDir.xyz = viewDirForLight;

		o.mixedUvA.xy = TRANSFORM_TEX(v.uv, _BumpMap);
		o.mixedUvA.zw = TRANSFORM_TEX(v.uv, _SpecularGlitterTex);

		float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - worldPos);
		float3x3 tangentTransform = float3x3( worldTangent, worldBinormal, worldNormal);
		float2 tView = mul(tangentTransform, viewDirection).xy;
		// tView = 0.05 * _GlitterSpeed * tView;
		// float2 vwvxwv = tView + float2(1,1) - v.uv;

		// float2 _SpecularMapUv = (tView + v.uv) * (0.5 * _GlitterSpeed + _Szpachla) * _GlitterMaskScale;
		// float2 _SpecularMapUv2 = vwvxwv * _GlitterMaskScale * (1.0 - (_GlitterSpeed * 0.31830988618)) * _MaskAdjust;

		float2 vwvxwv = (mul((0.05*((-1*_GlitterSpeed))*tView + v.uv).rg-float2(0.5, 0.5), float2x2( -1, 0, 0, -1))+float2(0.5, 0.5));
		float2 _SpecularMapUv2 = (vwvxwv*_GlitterMaskScale*(1.0-(_GlitterSpeed/3.141592654))*_MaskAdjust);

		float2 _SpecularMapUv = ((0.05*(_GlitterSpeed)*tView + v.uv).rg*((_GlitterSpeed/2.0)+1.0)*_GlitterMaskScale);

		o.mixedUvB.xy = TRANSFORM_TEX(_SpecularMapUv, _SpecularMap);
		o.mixedUvB.zw = TRANSFORM_TEX(_SpecularMapUv2, _SpecularMap);

		o.halfDir.w = exp2(_Shininess * 10.0+1.0);

		TRANSFER_SHADOW(o);
		UNITY_TRANSFER_FOG(o,o.pos);
		return o;
	}

	fixed4 frag (v2f IN) : SV_Target
	{
		UNITY_SETUP_INSTANCE_ID(IN);

		float3 worldPos = float3(IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w);

		#ifndef USING_DIRECTIONAL_LIGHT
			fixed3 lightDir = normalize(UnityWorldSpaceLightDir(worldPos));
		#else
			fixed3 lightDir = _WorldSpaceLightPos0.xyz;
		#endif

		fixed4 _SpecularMap_var = tex2D(_SpecularMap, IN.mixedUvB.xy);
		fixed4 _SpecularMap_var2 = tex2D(_SpecularMap, IN.mixedUvB.zw);
		fixed4 _SpecularGlitterTex_var = tex2D(_SpecularGlitterTex, IN.mixedUvA.zw);

		half3 _Normal_var = UnpackNormal (tex2D (_BumpMap, IN.mixedUvA.xy));

		fixed3 worldN;
		worldN.x = dot(IN.tSpace0.xyz, _Normal_var);
		worldN.y = dot(IN.tSpace1.xyz, _Normal_var);
		worldN.z = dot(IN.tSpace2.xyz, _Normal_var);

		float attenuation = LIGHT_ATTENUATION(IN);
		fixed3 attenColor = attenuation * _LightColor0.xyz;

		float NdotL = max(0, dot(worldN, lightDir));
		fixed3 directDiffuse = max(0.0, NdotL) * attenColor;

		fixed3 _SpecCol = lerp(float3(0,0,0), pow(((_GlitterPower * _SpecularColor.rgb) * _SpecularMap_var.rgb), _GlitterContrast), _SpecularMap_var2.rgb) + pow((_SpecularGlitterTex_var.rgb * _SpecularPower), _SpecularContrast);
		fixed3 directSpecular = attenColor * pow(max(0, dot(IN.halfDir, worldN)), IN.halfDir.w) * _SpecCol;

		fixed4 c;
		c.rgb = directDiffuse.rgb * _Color.rgb + UNITY_LIGHTMODEL_AMBIENT.rgb + directSpecular;
		c.a = 1;
		
		UNITY_APPLY_FOG(IN.fogCoord, c);
		return c;
	}
	ENDCG
}
}

FallBack "Diffuse"
}
