﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastRagdoll : MonoBehaviour
{
	[SerializeField]
	private string input;

	[SerializeField]
	private Rigidbody scooter;

	private void Update()
	{
		if (Input.GetKeyDown(input))
		{
			GetComponent<AnimationFollow>().AnimationPercent = 0;
			GetComponent<AnimationFollow>().addForceToLimbs(scooter,10f,ForceMode.Force);
			transform.parent.parent = null;
		}
	}
}