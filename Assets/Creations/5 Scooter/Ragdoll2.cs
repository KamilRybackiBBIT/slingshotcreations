﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class Ragdoll2 : MonoBehaviour
{
    [SerializeField]
    private GameObject[] limbs;
    [SerializeField]
    private Rigidbody[] limbsRb;

    public UnityEvent onStart;
    public UnityEvent onRagdoll;

    [SerializeField]
    private Rigidbody velocityOnRagdoll;
    [SerializeField]
    private GameObject velocityOnRagdollV2;
    
    
    [SerializeField]
    private float multiplier;
    [SerializeField]
    private ForceMode myForceMode;
    
    [SerializeField]
    private bool ragdollOnStart;

    [SerializeField]
    private bool isRagdoll;

    [SerializeField] private string ragdollInput = "3";
    [SerializeField] private string resetInput = "2";

    public GameObject ragdollRoot;
    public bool onRagdollIgnoreRoot;
    
    //public bool useGravity;

    private void Start()
    {
        onStart?.Invoke();

        if (ragdollOnStart)
        {
            EnableRagdoll();
        }

        else
        {   
            LimbCollisionEnable(false);
            LimbsKinematic(true);
            isRagdoll = false;
        }

    }


    private void Update()
    {
        if (Input.GetKeyDown(ragdollInput))
        {
            EnableRagdoll();
        }

        if (Input.GetKeyDown(resetInput))
        {
            StandUp();
        }
    }


    public void EnableRagdoll()
    {
        if (GetComponent<Animator>())
        {
            GetComponent<Animator>().enabled = false;
        }
        

        LimbCollisionEnable(true);
        LimbsKinematic(false);
        isRagdoll = true;

        //addForceToAllLimbs();

        onRagdoll?.Invoke();
    }

    public void StandUp()
    {
        Animator anim = GetComponent<Animator>();
       anim.enabled = true;
        anim.Play("Any State", 0);
        isRagdoll = false;
        ragdollRoot.GetComponent<Rigidbody>().isKinematic = true;
        

    }

    public void addForceToAllLimbs()
    {
        for (int i = 0; i < limbs.Length; i++)
        {
            if (velocityOnRagdoll)
            {
                //limbsRb[i].velocity = Vector3.zero;
                limbsRb[i].AddForce(velocityOnRagdoll.velocity * multiplier, myForceMode);
            }
        }
    }
    
    public void addForceToAllLimbsV2()
    {
        Vector3 dir = velocityOnRagdoll.transform.position - velocityOnRagdollV2.transform.position;
        float distance = Vector3.Distance(velocityOnRagdollV2.transform.position, velocityOnRagdoll.transform.position);
        for (int i = 0; i < limbs.Length; i++)
        {
            if (velocityOnRagdoll)
            {

                Vector3 lel = limbsRb[i].velocity;
                lel.y = velocityOnRagdoll.velocity.y;
                limbsRb[i].velocity = lel;
                //limbsRb[i].velocity = Vector3.zero;
                limbsRb[i].AddForce(dir * distance * multiplier, myForceMode);
            }
        }
    }
    
    public void LimbsKinematic(bool state)
    {
        for (int i = 0; i < limbs.Length; i++)
        {
            if (onRagdollIgnoreRoot)
            {
                if ( limbsRb[i].gameObject == ragdollRoot)
                {
                    limbsRb[i].isKinematic = true;
                }

                else
                {
                    limbsRb[i].isKinematic = state;
                }
            }
            else
            {
                limbsRb[i].isKinematic = state;
            }
            
        }
    }

    public void LimbCollisionEnable(bool state)
    {
        foreach (var limb in limbs)
        {
            if (limb.GetComponent<SphereCollider>())
            {
                limb.GetComponent<SphereCollider>().enabled = state;
            }

            if (limb.GetComponent<CapsuleCollider>())
            {
                limb.GetComponent<CapsuleCollider>().enabled = state;
            }

            if (limb.GetComponent<BoxCollider>())
            {
                limb.GetComponent<BoxCollider>().enabled = state;
            }
        }
    }



    [ContextMenu("FindLimbs")]
    private void FindLimbs()
    {
        limbsRb = transform.GetComponentsInChildren<Rigidbody>();
        limbs = new GameObject[limbsRb.Length];
        for (int i = 0; i < limbsRb.Length; i++)
        {
            limbs[i] = limbsRb[i].gameObject;
        }
    } 
    
    public void LimbsGravity(bool State)
    {
        for (int i = 0; i < limbsRb.Length; i++)
        {
            limbsRb[i].useGravity = State;
        }
    } 
}
