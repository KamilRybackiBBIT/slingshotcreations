using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RagdollAnimMerge : MonoBehaviour
{
	[Range(0f, 1f)] public float ragdoll;

	[Range(0f, 1f)] public float ragdollToAnim;

	[Header("Roots")]
	public Transform ragdollRoot;
	public Transform animRoot;
	public Transform finalRoot;

	[Header("Arrays")]
	public Transform[] ragdollBones;
	public Transform[] animBones;
	public Transform[] finalBones;

	public bool localPos;
	public bool localRot;

	[Header("AddictionalSettings")]
	public bool SetArraysAtStart;
	public bool MergeRagdollWithAnimationAtStart;

	public UnityEvent onMergeWithAnimation;
	private void Start()
	{
		if (SetArraysAtStart) SetArrays();
		if (MergeRagdollWithAnimationAtStart) mergeRagdollWithAnimation();
	}

	int frames = 0;
	private void FixedUpdate()
	{
		frames++;
		if (MergeRagdollWithAnimationAtStart && frames == 2)
		{
			MergeRagdollWithAnimationAtStart = false;
			//mergeRagdollWithAnimation();
			adjustPercentageToAnim(ragdollToAnim);
			onMergeWithAnimation?.Invoke();
		}
		adjustPercentage(ragdoll);

	}

	void adjustPercentage(float percent)
	{


		for (int i = 0; i < ragdollBones.Length; i++)
		{

			if (localPos)
			{
				//position
				finalBones[i].localPosition = Vector3.Lerp(
					ragdollBones[i].localPosition,
					animBones[i].localPosition,
					percent);
			}
			else
			{
				//position
				finalBones[i].position = Vector3.Lerp(
					ragdollBones[i].position,
					animBones[i].position,
					percent);
			}

			if (localRot)
			{
				//rotation
				finalBones[i].localRotation = Quaternion.Lerp(
					ragdollBones[i].localRotation,
					animBones[i].localRotation,
					percent);
			}

			else
			{
				//rotation
				finalBones[i].rotation = Quaternion.Lerp(
					ragdollBones[i].rotation,
					animBones[i].rotation,
					percent);
			}
		}
	}

	void adjustPercentageToAnim(float percent)
	{


		for (int i = 0; i < ragdollBones.Length; i++)
		{

			if (localPos)
			{
				//position
				ragdollBones[i].localPosition = Vector3.Lerp(
					ragdollBones[i].localPosition,
					animBones[i].localPosition,
					percent);
			}
			else
			{
				//position
				ragdollBones[i].position = Vector3.Lerp(
					ragdollBones[i].position,
					animBones[i].position,
					percent);
			}

			if (localRot)
			{
				//rotation
				ragdollBones[i].localRotation = Quaternion.Lerp(
					ragdollBones[i].localRotation,
					animBones[i].localRotation,
					percent);
			}

			else
			{
				//rotation
				ragdollBones[i].rotation = Quaternion.Lerp(
					ragdollBones[i].rotation,
					animBones[i].rotation,
					percent);
			}
		}
	}

	[ContextMenu("mergeRagdollWithAnimation")]
	public void mergeRagdollWithAnimation()
	{
		for (int i = 0; i < ragdollBones.Length; i++)
		{

			if (localPos)
			{
				//position
				ragdollBones[i].localPosition = animBones[i].localPosition;
			}
			else
			{
				//position
				ragdollBones[i].position = animBones[i].position;
			}

			if (localRot)
			{
				//rotation
				ragdollBones[i].localRotation = animBones[i].localRotation;
			}

			else
			{
				//rotation
				ragdollBones[i].rotation = animBones[i].rotation;
			}
		}
	}

	[ContextMenu("SetArrays")]
	public void SetArrays()
	{
		ragdollBones = ragdollRoot.GetComponentsInChildren<Transform>();
		animBones = animRoot.GetComponentsInChildren<Transform>();
		finalBones = finalRoot.GetComponentsInChildren<Transform>();

		//Rigidbodies = GetComponentsInChildren<Rigidbody>();
		//RigidbodyCharacterJoints = GetComponentsInChildren<CharacterJoint>();

		//RigidbodiesTransforms = new Transform[Rigidbodies.Length];
		//for (int i = 0; i < Rigidbodies.Length; i++)
		//{
		//	RigidbodiesTransforms[i] = Rigidbodies[i].transform; ;
		//}

		//animator


		//AnimationRbBones = new Transform[Rigidbodies.Length];
		/*
		int iterations = 0;
		for (int i = 0; i < AnimationTransforms.Length; i++)
		{
			for (int j = 0; j < RigidbodiesTransforms.Length; j++)
			{
				if (AnimationTransforms[i].gameObject.name == RigidbodiesTransforms[j].gameObject.name)
				{
					AnimationRbBones[iterations] = AnimationTransforms[i];
					iterations++;
				}
			}

		}
		*/


	}

	public void lerpToRagdollFunction(float sec)
	{
		StartCoroutine(lerpToRagdoll(sec));
	}

	public IEnumerator lerpToRagdoll(float seconds)
	{
		float timeElapsed = 0;

		while (timeElapsed < seconds)
		{
			timeElapsed += Time.deltaTime;
			ragdoll = FST.map(0f, seconds, 1f, 0f, timeElapsed);
			yield return null;
		}
		ragdoll = 0f;

	}

	public void lerpToAnimFunction(float sec)
	{
		StartCoroutine(lerpToAnim(sec));
	}

	public IEnumerator lerpToAnim(float seconds)
	{
		float timeElapsed = 0;

		while (timeElapsed < seconds)
		{
			timeElapsed += Time.deltaTime;
			ragdoll = FST.map(0f, seconds, 0f, 1f, timeElapsed);
			yield return null;
		}
		ragdoll = 1f;

	}
}
