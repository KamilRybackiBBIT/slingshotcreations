using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class RagdollController : MonoBehaviour
{
    [Header("MustHave")]
    [SerializeField] private Transform[] bones;
    [SerializeField] private Rigidbody[] rigidbodyBones;

    [Header("Settings")]
    [SerializeField] private bool ragdollOnStart;
    [SerializeField] private string ragdollInput = "3";
    [SerializeField] private string resetInput = "2";

    [Header("Optional")]
    public UnityEvent OnRagdoll;

    [Header("Info")]
    public bool IsRagdoll;

    private void Start()
    {
        if (ragdollOnStart) EnableRagdoll();
    }


    private void Update()
    {
        if (Input.GetKeyDown(ragdollInput)) EnableRagdoll();
        if (Input.GetKeyDown(resetInput)) StandUp();
    }


    public void EnableRagdoll()
    {
        if (GetComponent<Animator>())
        {
            GetComponent<Animator>().enabled = false;
        }

        LimbsKinematic(false);

        OnRagdoll?.Invoke();
        IsRagdoll = true;
    }

    public void StandUp()
    {

        Animator anim = GetComponent<Animator>();
        anim.enabled = true;
        anim.Play("Any State", 0);
        IsRagdoll = false;

    }

    public void LimbsKinematic(bool state)
    {
        for (int i = 0; i < rigidbodyBones.Length; i++)
        {
            rigidbodyBones[i].isKinematic = state;
        }
    }


    public void LimbCollisionEnable(bool state)
    {
        foreach (var bone in bones)
        {
            if (bone.GetComponent<SphereCollider>())
            {
                bone.GetComponent<SphereCollider>().enabled = state;
            }

            if (bone.GetComponent<CapsuleCollider>())
            {
                bone.GetComponent<CapsuleCollider>().enabled = state;
            }

            if (bone.GetComponent<BoxCollider>())
            {
                bone.GetComponent<BoxCollider>().enabled = state;
            }
        }
    }



    [ContextMenu("FindLimbs")]
    private void FindLimbs()
    {
        rigidbodyBones = transform.GetComponentsInChildren<Rigidbody>();
        bones = GetComponentsInChildren<Transform>();
    }


}
