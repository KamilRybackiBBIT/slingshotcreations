using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SlingShotPercentDisplay : MonoBehaviour
{
    public TextMeshProUGUI textxd;
    public SlingShot main;

    private void Start()
    {
        if (main == null)
        {
            main = GetComponentInParent<SlingShot>();
        }
    }

    void Update()
    {
        textxd.text = Mathf.Abs((main.PercentZ*100f)).ToString("0") + "%";
    }
}
