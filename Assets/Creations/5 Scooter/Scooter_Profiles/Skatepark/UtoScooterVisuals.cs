using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtoScooterVisuals : MonoBehaviour
{
    UtoMove utoMove;

    public ParticleSystem[] wheelSparks;
    [Range(0f, 10f)] [SerializeField] float speedAmountMultiplier = 1f;
    [Range(0f, 10f)] [SerializeField] float angleAmountMultiplier = 1f;

    public bool isOff = false;

    void Start()
    {
        utoMove = GetComponent<UtoMove>();

        if (isOff)
        {
            foreach (ParticleSystem part in wheelSparks)
            {
                var emission = part.emission;
                emission.rateOverTime = 0f;
            }
        }
    }


    void Update()
    {

        foreach (WheelCollider wheel in utoMove.wheels)
        {

            if (utoMove.IsGrounded && utoMove.VelocityToForwardAngle > 5f && utoMove.VelocityToForwardAngle < 150f)
            {
                if (utoMove.VelocityToForwardAngle > 5)
                {
                    foreach (ParticleSystem part in wheelSparks)
                    {
                        var emission = part.emission;
                        emission.rateOverTime = utoMove.Speed * speedAmountMultiplier * utoMove.VelocityToForwardAngle / angleAmountMultiplier;

                        var partDirection = part.velocityOverLifetime;

                        Vector3 direction = -utoMove.rb.velocity.normalized;
                        partDirection.x = direction.x;
                        partDirection.y = direction.y;
                        partDirection.z = direction.z;
                    }
                }
            }

            else
            {
                foreach (ParticleSystem part in wheelSparks)
                {
                    var emission = part.emission;
                    emission.rateOverTime = 0f;
                }
            }
        }
    }
}
