using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrindActivator : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponentInParent<UtoMove>())
        {
            Grind xd = gameObject.GetComponentInParent<Grind>();
            xd.startGrinding(other.gameObject.GetComponentInParent<UtoMove>().gameObject);
        }
    }
}
