using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grind : MonoBehaviour
{

    public GameObject whatIsGrinding;
    public float startSpeed;

    public Transform From, To;

    public bool isGrinding = false;

    void FixedUpdate()
    {
        if (isGrinding)
        {
            float distanceFrom = Vector3.Distance(From.position, whatIsGrinding.transform.position);
            float distanceTo = Vector3.Distance(To.position, whatIsGrinding.transform.position);
            float totalGrindDistance = Vector3.Distance(From.position, To.position);
            float percent = FST.map(0f, totalGrindDistance, 0f, 1f, distanceFrom);

            whatIsGrinding.transform.position = Vector3.Lerp(From.position, To.position, percent);
            Vector3.Lerp(whatIsGrinding.transform.position, Vector3.Lerp(From.position, To.position, percent), Time.deltaTime * 13f);
            Rigidbody rb = whatIsGrinding.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(rb.velocity.x, -1f, rb.velocity.z);


            if (Input.GetKey("space"))
            {
                stopGrinding();
            }

            if (percent > 0.95f)
            {
                stopGrinding();
            }
        }
    }

    public void startGrinding(GameObject utoMoveGameObject) {

        isGrinding = true;
        whatIsGrinding = utoMoveGameObject;

        float distanceFrom = Vector3.Distance(From.position, utoMoveGameObject.transform.position);
        float distanceTo = Vector3.Distance(To.position, utoMoveGameObject.transform.position);
        float totalGrindDistance = Vector3.Distance(From.position, To.position);

        //zdobadz procent pozycji
        float percentIn = FST.map(0f, totalGrindDistance, 0f, 1f, distanceFrom);


        //ustaw predkosc poruszania
        startSpeed = utoMoveGameObject.GetComponent<Rigidbody>().velocity.magnitude;
        utoMoveGameObject.GetComponent<Rigidbody>().velocity *= 0.6f;

    }

    public void stopGrinding()
    {
        isGrinding = false;
        enabled = false;
    }
}
