using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrindEffects : MonoBehaviour
{

    public bool sparks;
    public ParticleSystem sparksParticle;
    public GameObject  sparksParticleGO;
    public Grind grind;
    public Vector3 offset;
    private void Start()
    {
        grind = GetComponent<Grind>();
        sparksParticle.Play();
    }

    private void Update()
    {
        if (grind.isGrinding)
        {
            sparksParticleGO.transform.position = grind.whatIsGrinding.transform.position + offset;
        }
    }
}
