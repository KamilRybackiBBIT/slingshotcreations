﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.SocialPlatforms;

public class AnimationFollow : MonoBehaviour
{
	[Header("Animator Variables")]
	public Animator animator;
	public Transform animRootBone;
	public Transform[] AnimationTransforms;
	public Transform[] AnimationRbBones;

	[Header("Rigidbody Variables")]
	public Transform ragdollRootBone;
	public Transform[] RagdollTransforms;
	public Rigidbody[] Rigidbodies;
	public Transform[] RigidbodiesTransforms;
	public CharacterJoint[] RigidbodyCharacterJoints;

	[Header("Settings")]
	[Range(0f, 1f)]
	public float AnimationPercent; //0 = ragdoll 1 = animation
	public bool syncOnlyRigidbodies = false;
	public bool newSystem;

	public bool localPos;
	public bool localRot;
	[Range(1f,100f)]
	public int iterations;

	[Header("info")]
	[SerializeField]
	private float velocityBreaking;
	
	private void FixedUpdate()
	{
		adjustPercentage(AnimationPercent);
	}

	void adjustPercentage(float percent)
	{
		if (newSystem)
		{
			
			//pos&rot
			for (int i = 0; i < RagdollTransforms.Length; i++)
			{

				for (int j = 0; j < iterations; j++)
				{


					if (RagdollTransforms[i] != ragdollRootBone)
					{
						if (localPos)
						{
							//position
							RagdollTransforms[i].localPosition = Vector3.Lerp(
								RagdollTransforms[i].localPosition,
								AnimationTransforms[i].localPosition,
								percent);
						}
						else
						{
							//position
							RagdollTransforms[i].position = Vector3.Lerp(
								RagdollTransforms[i].position,
								AnimationTransforms[i].position,
								percent);
						}

						if (localRot)
						{
							//rotation
							RagdollTransforms[i].localRotation = Quaternion.Lerp(
								RagdollTransforms[i].localRotation,
								AnimationTransforms[i].localRotation,
								percent);
						}

						else
						{
							//rotation
							RagdollTransforms[i].rotation = Quaternion.Lerp(
								RagdollTransforms[i].rotation,
								AnimationTransforms[i].rotation,
								percent);
						}

					}
				}


			}
			
			
		}

		else
		{
			if (syncOnlyRigidbodies)
			{
				//pos&rot
				for (int i = 0; i < RigidbodiesTransforms.Length; i++)
				{

					if (RigidbodiesTransforms[i].gameObject != ragdollRootBone.gameObject)
					{
						//position
						RigidbodiesTransforms[i].position = Vector3.Lerp(
							RigidbodiesTransforms[i].position,
							AnimationRbBones[i].position,
							percent);
				
						//rotation
						RigidbodiesTransforms[i].rotation = Quaternion.Lerp(
							RigidbodiesTransforms[i].rotation,
							AnimationRbBones[i].rotation,
							percent);
					}

				
				}
			}

			else
			{
				//pos&rot
				for (int i = 0; i < RagdollTransforms.Length; i++)
				{
					if (RagdollTransforms[i] != ragdollRootBone)
					{
					
					}
					//position
					RagdollTransforms[i].position = Vector3.Lerp(
						RagdollTransforms[i].position,
						AnimationTransforms[i].position,
						percent);
				
					//rotation
					RagdollTransforms[i].rotation = Quaternion.Lerp(
						RagdollTransforms[i].rotation,
						AnimationTransforms[i].rotation,
						percent);
				
				}
			}
		}
		
		



		velocityBreaking = FST.map(0f, 1f, 1f, 0f, percent);
		if (newSystem)
		{
			velocityBreaking = 1f;
			for (int i = 0; i < Rigidbodies.Length; i++)
			{
				//velocity
				Rigidbodies[i].velocity = Rigidbodies[i].velocity * velocityBreaking;
			
				//angular
				Rigidbodies[i].angularVelocity = Rigidbodies[i].angularVelocity * velocityBreaking;
					
				Debug.Log(Rigidbodies[i].gameObject.name);
				
				if (Rigidbodies[i].gameObject != ragdollRootBone.gameObject)
				{

				}

				else
				{
						
				}
				
			}
			
			//velocityBreaking = 0;
			return;

			
		}
		for (int i = 0; i < Rigidbodies.Length; i++)
		{	
			//velocity
			Rigidbodies[i].velocity = Rigidbodies[i].velocity * velocityBreaking;
			
			//angular
			Rigidbodies[i].angularVelocity = Rigidbodies[i].angularVelocity * velocityBreaking;
		}
		
	}

	public void addForceToLimbs(Rigidbody rb, float multiplier, ForceMode fm)
	{
		for (int i = 0; i < Rigidbodies.Length; i++)
		{
			Rigidbodies[i].AddForce(rb.velocity * rb.velocity.magnitude * multiplier, fm);
		}
	}
	
	[ContextMenu("SetArrays")]
	public void SetArrays()
	{
		//rigidbody
		RagdollTransforms = ragdollRootBone.GetComponentsInChildren<Transform>();

		Rigidbodies = GetComponentsInChildren<Rigidbody>();
		RigidbodyCharacterJoints = GetComponentsInChildren<CharacterJoint>();

		RigidbodiesTransforms = new Transform[Rigidbodies.Length];
		for (int i = 0; i < Rigidbodies.Length; i++)
		{
			RigidbodiesTransforms[i] = Rigidbodies[i].transform;;
		}

		//animator
		AnimationTransforms = animRootBone.GetComponentsInChildren<Transform>();

		AnimationRbBones = new Transform[Rigidbodies.Length];

		int iterations = 0;
		for (int i = 0; i < AnimationTransforms.Length; i++)
		{
			for (int j = 0; j < RigidbodiesTransforms.Length; j++)
			{
				if (AnimationTransforms[i].gameObject.name == RigidbodiesTransforms[j].gameObject.name)
				{
					AnimationRbBones[iterations] = AnimationTransforms[i];
					iterations++;
				}
			}

		}


	}
	
}
