﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MifEffects : MonoBehaviour
{
    [SerializeField] ParticleSystem[] jetParticles;
    [SerializeField] MifController mc;

    private void Update()
    {

        if (mc.addingPower)
        {
            for (int i = 0; i < jetParticles.Length; i++)
            {
                if (!jetParticles[i].isPlaying)
                {
                    jetParticles[i].Play();
                }
                SetEmission(jetParticles[i], 420f);
            }
        }

        else
        {
            for (int i = 0; i < jetParticles.Length; i++)
            {
                if (jetParticles[i].isPlaying)
                {
                    SetEmission(jetParticles[i], 0f);
                }
            }
        }
    }


    public void SetEmission(ParticleSystem particle, float emitTimeValue)
    {
        if (!particle.isPlaying)
        {
            particle.Play();
        }

        var xd = particle.emission;
        xd.rateOverTime = new ParticleSystem.MinMaxCurve(emitTimeValue);
    }

    public void SetForce(ParticleSystem particle, Vector3 dir, float force)
    {
        var mainn = particle.main;
        mainn.startSpeed = force;
    }

}
