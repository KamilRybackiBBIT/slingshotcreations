﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtoJoystick : MonoBehaviour
{
	[SerializeField] FixedJoystick joystick;

	[SerializeField] UtoMove move;

	void Update()
	{
		move.InputVertical = joystick.Vertical;
		move.InputHorizontal = joystick.Horizontal;
	}
}
