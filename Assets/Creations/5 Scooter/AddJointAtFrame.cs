﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddJointAtFrame : MonoBehaviour
{

    public enum joints
    {
        fixedJoint,
        springJoint,
        characterJoint,
        configurableJoint
    }

    public joints JointToAdd;

    public Rigidbody connect;

    public int frame;

    private int frames = 0;

    public bool noFrames = false;
    void Update()
    {
        if (noFrames)
        {
            return;
        }

        if (frames < frame)
        {
            frames++;
        }

        else
        {
            Connect();
        }
    }

    public void Connect()
    {
        Joint joint;

        if (JointToAdd == joints.fixedJoint)
        {
            joint = gameObject.AddComponent<FixedJoint>();
        }

        else if (JointToAdd == joints.springJoint)
        {
            joint = gameObject.AddComponent<SpringJoint>();
        }

        else if (JointToAdd == joints.characterJoint)
        {
            joint = gameObject.AddComponent<CharacterJoint>();
        }

        else if (JointToAdd == joints.configurableJoint)
        {
            joint = gameObject.AddComponent<ConfigurableJoint>();
        }

        else
        {
            joint = gameObject.AddComponent<FixedJoint>();
        }

        joint.connectedBody = connect;
        enabled = false;
    }


}
