﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.SocialPlatforms;

public class AnimationFollow2 : MonoBehaviour
{
	[Header("Animator Variables")]
	public Animator animator;
	public Transform animRootBone;
	public Transform[] AnimationTransforms;
	public Transform[] AnimationRbBones;

	[Header("Rigidbody Variables")]
	public Transform ragdollRootBone;
	public Transform[] RagdollTransforms;
	public Rigidbody[] Rigidbodies;
	public Transform[] RigidbodiesTransforms;

	public Transform OutputRootBone;
	public Transform[] OutputTransforms;
	public Rigidbody[] OutputRigidbodies;
	public Transform[] OutputRigidbodiesTransforms;

	[Header("Settings")]
	[Range(0f, 1f)]
	public float AnimationPercent; //0 = ragdoll 1 = animation
	public bool syncOnlyRigidbodies = false;
	public bool newSystem;
	public bool addForceSystem;
	public bool ignoreRoot;
	public bool localPos;
	public bool localRot;
	[Range(1f,100f)]
	public int iterations;
	public float force;

	public bool useGravity;
	public bool useMineGravity;

	[Header("info")]
	[SerializeField]
	private float velocityBreaking;

	IEnumerator Start()
	{
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		Debug.Log("XDD");
		SyncRagdoll();Debug.Log("XDD");
		yield break;
	}

	void SyncRagdoll()
	{

		for (int i = 0; i < OutputTransforms.Length; i++)
		{
			//position
			RagdollTransforms[i].localPosition = Vector3.Lerp(
				RagdollTransforms[i].localPosition,
				AnimationTransforms[i].localPosition,
				1f);
		
			//rotation
			RagdollTransforms[i].localRotation = Quaternion.Lerp(
				RagdollTransforms[i].localRotation,
				AnimationTransforms[i].localRotation,
				1f);
		}
		
		
	}
	
	private void FixedUpdate()
	{
		adjustPercentage(AnimationPercent);
		
		foreach (var rb in Rigidbodies)
		{
			rb.useGravity = useGravity;
		}
		
		foreach (var rb in OutputRigidbodies)
		{
			rb.useGravity = useGravity;
		}
	}

	void adjustPercentage(float percent)
	{

		if (addForceSystem)
		{
			//pos&rot
			for (int i = 0; i < Rigidbodies.Length; i++)
			{
				if (Rigidbodies[i].gameObject != ragdollRootBone.gameObject)
				{
					float distance = Vector3.Distance(Rigidbodies[i].position, AnimationRbBones[i].position);
					Debug.Log(distance);
					//position
					Rigidbodies[i].AddForce(Rigidbodies[i].position - AnimationRbBones[i].position * force * percent * distance);
				}
				
		
			}
		}

		if (newSystem)
		{
			
			for (int i = 0; i < RagdollTransforms.Length; i++)
			{
				
				if (RagdollTransforms[i] != ragdollRootBone)
				{
						
					if (localPos)
					{
						//position
						OutputTransforms[i].localPosition = Vector3.Lerp(
							RagdollTransforms[i].localPosition,
							AnimationTransforms[i].localPosition,
							percent);
					}
						
					else
					{
						//position
						OutputTransforms[i].position = Vector3.Lerp(
							RagdollTransforms[i].position,
							AnimationTransforms[i].position,
							percent);
					}

					if (localRot)
					{
						//rotation
						OutputTransforms[i].localRotation = Quaternion.Lerp(
							RagdollTransforms[i].localRotation,
							AnimationTransforms[i].localRotation,
							percent);
					}

					else
					{
						//rotation
						OutputTransforms[i].rotation = Quaternion.Lerp(
							RagdollTransforms[i].rotation,
							AnimationTransforms[i].rotation,
							percent);
					}
				}
			}
		}

		
		
		if (newSystem)
		{
			velocityBreaking = FST.map(0f, 1f, 1f, 0f, percent);
			
			
			
			//velocityBreaking = 1f;
			for (int i = 0; i < Rigidbodies.Length; i++)
			{
				//OutputRootBone.GetComponent<Rigidbody>().velocity =
					//OutputRootBone.GetComponent<Rigidbody>().velocity * velocityBreaking;
					//OutputRigidbodies[i].velocity *= velocityBreaking * Time.deltaTime;
					//OutputRigidbodies[i].angularVelocity *=  velocityBreaking * Time.deltaTime;
					//Rigidbodies[i].velocity =  velocityBreaking * Time.deltaTime;
					if (useMineGravity)
					{
						if (Rigidbodies[i].gameObject != ragdollRootBone.gameObject)
						{
							Rigidbodies[i].AddForce(Vector3.down * -Physics.gravity.y * velocityBreaking *10f);
							OutputRigidbodies[i].AddForce(Vector3.down * -Physics.gravity.y * velocityBreaking *10f);
						}
						
					}
					//Rigidbodies[i].velocity = 

			}
			

			
		}
		
		
	}

	public void addForceToLimbs(Rigidbody rb, float multiplier, ForceMode fm)
	{
		for (int i = 0; i < Rigidbodies.Length; i++)
		{
			Rigidbodies[i].AddForce(rb.velocity * rb.velocity.magnitude * multiplier, fm);
		}
	}
	
	[ContextMenu("SetArrays")]
	public void SetArrays()
	{
		//rigidbody
		RagdollTransforms = ragdollRootBone.GetComponentsInChildren<Transform>();
		Rigidbodies = ragdollRootBone.transform.GetComponentsInChildren<Rigidbody>();

		RigidbodiesTransforms = new Transform[Rigidbodies.Length];
		for (int i = 0; i < Rigidbodies.Length; i++)
		{
			RigidbodiesTransforms[i] = Rigidbodies[i].transform;
		}
		
		//animator
		AnimationTransforms = animRootBone.GetComponentsInChildren<Transform>();
		AnimationRbBones = new Transform[Rigidbodies.Length];

		int iterations = 0;
		for (int i = 0; i < AnimationTransforms.Length; i++)
		{
			for (int j = 0; j < RigidbodiesTransforms.Length; j++)
			{
				if (AnimationTransforms[i].gameObject.name == RigidbodiesTransforms[j].gameObject.name)
				{
					AnimationRbBones[iterations] = AnimationTransforms[i];
					iterations++;
				}
			}

		}
		
		//output
		OutputTransforms = OutputRootBone.GetComponentsInChildren<Transform>();
		OutputRigidbodiesTransforms = new Transform[Rigidbodies.Length];
		OutputRigidbodies = OutputRootBone.GetComponentsInChildren<Rigidbody>();
		int iterationss = 0;
		for (int i = 0; i < OutputTransforms.Length; i++)
		{
			for (int j = 0; j < Rigidbodies.Length; j++)
			{
				if (OutputTransforms[i].gameObject.name == Rigidbodies[j].gameObject.name)
				{
					OutputRigidbodiesTransforms[iterationss] = OutputTransforms[i];
					iterationss++;
				}
			}
		}

		
	}
	
}
