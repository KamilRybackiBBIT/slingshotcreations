﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelSpin : MonoBehaviour
{

    public WheelCollider wc;

    private float lastSpeed = 0;

    void Update()
    {
        if (wc.isGrounded)
        {
            float spd = 1 * wc.rpm * 60 * Time.deltaTime;
            transform.localRotation *= Quaternion.Euler(spd, 0, 0);
            lastSpeed = spd;
        }

        else
        {
            transform.localRotation *= Quaternion.Euler(lastSpeed, 0, 0);
        }
    }
}
