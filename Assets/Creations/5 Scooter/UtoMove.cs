﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UtoMove : MonoBehaviour
{
    [Header("SetThis")]
    public Rigidbody rb;

    [SerializeField] float boostForce;
    [SerializeField] float turnForce;
    [SerializeField] float airTurnForce;
    [SerializeField] float jumpForce;
    public WheelCollider[] wheels = new WheelCollider[0];
    [HideInInspector] public Quaternion[] WheelsStartPos = new Quaternion[0];
    public AnimationCurve steeringMultiplier; //if u go 0kph, you should not be able to steer// if 5, u should tur a little bit, and 15 km should be a multiplayer 1f, but 15 km/h maybe 0.3f
    public ForceMode boostMode;
    public ForceMode JumpMode;


    [Header("Optional")]
    [SerializeField] GameObject velocityShow;
    [SerializeField] bool wheelsBoost;
    [SerializeField] bool wheelsTurn;


    [Header("Admin Options")]
    [SerializeField] public bool canTurn;
    [SerializeField] public bool additiveTurning;
    [SerializeField] public bool airSteering;
    [SerializeField] bool autoInput;


    [Header("InfoToOtherScripts")]
    public float Speed;
    public bool IsGrounded;
    public float VelocityToForwardAngle;


    [Header("Input")]
    [SerializeField] bool canSteer;
    [SerializeField] string JumpButton;
    [SerializeField] string BoostButton;
    [SerializeField] bool pushOffContunous;

    public bool InputPushOff = false;
    public bool InputJump = false;
    public float InputHorizontal;
    public float InputVertical;


    void Start()
    {
        rb.maxAngularVelocity = 1000000f;
        WheelsStartPos = new Quaternion[wheels.Length];
        for (int i = 0; i < wheels.Length; i++)
        {
            WheelsStartPos[i] = wheels[i].transform.localRotation;
        }
    }


    private void Update()
    {
        //input
        if (pushOffContunous)
        {
            InputPushOff = Input.GetKey(BoostButton);
        }

        else
        {
            InputPushOff = Input.GetKeyDown(BoostButton);
        }
        

        if (autoInput)
        {
            InputHorizontal = Input.GetAxis("Horizontal");
            InputVertical = Input.GetAxis("Vertical");
        }

        else
        {
            //tutaj twój customowy input
        }

        InputJump = Input.GetKeyDown(JumpButton);

        //info


        //setting VelocityShow
        if (!rb.isKinematic)
        {
            velocityShow.transform.forward = rb.velocity.normalized;
        }
        VelocityToForwardAngle = Quaternion.Angle(transform.rotation, velocityShow.transform.rotation);


        //setting variables
        Speed = rb.velocity.magnitude;
        IsGrounded = checkGrounded();

        if (IsGrounded)
        {

            if (InputPushOff)
            {
                PushOff();
            }

            if (InputJump)
            {
                Jump();
            }
        }
    }

    private void FixedUpdate()
    {
        if (!canSteer) return;

        if (IsGrounded)
        {
            if (InputHorizontal != 0)
            {
                if (canTurn)
                {
                    if (wheelsTurn)
                    {
                        for (int i = 0; i < wheels.Length; i++)
                        {
                            if (wheels[i].transform.localPosition.z > 0)
                            {
                                wheels[i].transform.localRotation = WheelsStartPos[i] * Quaternion.Euler(0f, InputHorizontal * 100f, 0f);
                            }
                        }
                    }

                    else
                    {
                        Turn();
                    }
                }
                
            }

        }

        else
        {
            if (airSteering)
            {
                AirSteering();
            }
        }
        

        //if speed and velocity == 0 desk can not start, it is deleting it
        if (rb.velocity.magnitude < 0.01f) lowSpeedBugDelete();

    }


    void PushOff()
    {
        if (wheelsBoost)
        {
            foreach (var wheel in wheels)
            {
                wheel.motorTorque += 1f;
            }
        }

        else
        {
            rb.AddForce(transform.forward * boostForce * Time.fixedDeltaTime, boostMode);
        }
        
    }

    public void publicPushOff(float force)
    {
        PushOff();
    }

    void Jump()
    {
        rb.AddForce(transform.up * jumpForce * Time.fixedDeltaTime, JumpMode);
    }

    public void PublicJump()
    {
        Jump();
    }


    void Turn()
    {
        float steeringMultiplierF = steeringMultiplier.Evaluate(Speed);
        Vector3 angularVelocityCurrent = rb.angularVelocity;
        Vector3 angularVelocityVectorToAdd = InputHorizontal * transform.up * turnForce * steeringMultiplierF * Time.fixedDeltaTime;
        if (additiveTurning)
        {
            rb.angularVelocity += angularVelocityVectorToAdd/15f;
        }

        else
        {
            rb.angularVelocity = angularVelocityVectorToAdd;
        }
        
    }

    void AirSteering()
    {
        Vector3 angularVelocityCurrent = rb.angularVelocity;
        Vector3 angularVelocityVectorToAdd = InputVertical * transform.right  * airTurnForce  * Time.deltaTime;
        angularVelocityVectorToAdd += InputHorizontal * transform.up  * airTurnForce  * Time.deltaTime;
        rb.angularVelocity = angularVelocityVectorToAdd;
    }



    void lowSpeedBugDelete()
    {
        rb.velocity += transform.forward / 5;
    }


    bool checkGrounded()
    {
        int howMuchWheelsGrounded = 0;

        foreach (WheelCollider wheel in wheels)
        {
            if (wheel.isGrounded)
            {
                howMuchWheelsGrounded += 1;
            }
        }

        if (howMuchWheelsGrounded > 1)
        {
            return true;
        }

        else
        {
            return false;
        }

    }
}
