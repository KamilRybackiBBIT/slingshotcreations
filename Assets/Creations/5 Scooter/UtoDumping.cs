﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtoDumping : MonoBehaviour
{

    [Range(-60f, 60f)]
    [SerializeField] float maxAngle;

    [SerializeField] UtoMove utoMove;



    
    public enum axis
    {
        x,y,z
    }
    [Header("Axis To Spin")]
    public axis AxisDumping;




    float RotX;
    float RotY;
    float RotZ;

    float FinalX, FinalY, FinalZ;

    Vector3 startRot;

    void Start()
    {
        startRot = transform.localRotation.eulerAngles;
    }


    void FixedUpdate()
    {
        //smoothing inputs
        RotX = Mathf.Lerp(RotX, utoMove.InputHorizontal, 3.4f * Time.deltaTime);
        RotY = Mathf.Lerp(RotY, utoMove.InputHorizontal, 3.4f * Time.deltaTime);
        RotZ = Mathf.Lerp(RotZ, utoMove.InputHorizontal, 3.4f * Time.deltaTime);

        //final inputs
        if (AxisDumping == axis.x) FinalX = FST.map(1f, -1f, -maxAngle, maxAngle, RotX);
        if (AxisDumping == axis.y) FinalY = FST.map(1f, -1f, -maxAngle, maxAngle, RotY);
        if (AxisDumping == axis.z) FinalZ = FST.map(1f, -1f, -maxAngle, maxAngle, RotZ);

        //adjusting rotation
        Quaternion addRotation = Quaternion.Euler(new Vector3(FinalX,FinalY,FinalZ) + startRot);
        transform.localRotation = addRotation;

    }

}
