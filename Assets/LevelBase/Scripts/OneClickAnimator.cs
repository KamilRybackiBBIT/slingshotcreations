﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneClickAnimator : MonoBehaviour
{
    public string AnimatorBool;
    public string KeyCode;

    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (anim == null)
        {
            return;
            
        }
        if (Input.GetKey(KeyCode))
        {
            anim.SetBool(AnimatorBool, true);
        }

        else
        {
            anim.SetBool(AnimatorBool, false);
        }
        
    }
}
