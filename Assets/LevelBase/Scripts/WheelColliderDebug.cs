using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelColliderDebug : MonoBehaviour
{
    private WheelCollider wc;
    void Start()
    {
        wc = GetComponent<WheelCollider>();
    }

    private void Update()
    {
        if (wc != null)
        {
            wc.motorTorque = 0.001f;
        }

    }
}
