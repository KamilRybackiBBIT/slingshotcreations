﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererPositionUpdater : MonoBehaviour
{
    [SerializeField] Transform transformToCopy;
    [SerializeField] int indexToUpdate;
    [SerializeField] Vector3 offset;
    LineRenderer lr;

    private void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        NewPos();
    }

    [ContextMenu("NewPos")]
    void NewPos()
    {
        if (lr == null) lr = GetComponent<LineRenderer>();

        Vector3 pos = transformToCopy.position;
        float x = pos.x + offset.x;
        float y = pos.y + offset.y;
        float z = pos.z + offset.z;

        lr.GetPosition(indexToUpdate).Set(x, y, z);
        lr.SetPosition(indexToUpdate, new Vector3(x,y,z));
    }

}
