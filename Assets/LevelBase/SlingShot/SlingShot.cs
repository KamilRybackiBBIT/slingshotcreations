﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SlingShot : MonoBehaviour
{
    public GameObject Main;
    public GameObject Holder;

    [Range(-1f, 1f)]
    public float PercentX;
    [Range(-0.3f, 1f)]
    public float PercentY;
    [Range(-1f, 1f)]
    public float PercentZ;

    [SerializeField] Vector3 Direction;

    public float MinForce;
    public float MaxForce;
    public float CurrentForce;

    public ForceMode MyForceMode;

    public bool AllowDebugs;
    public bool ShotBool;

    [Header("Callibration")]
    [SerializeField] Transform Calibration;



    [Space]
    [Space]
    [Space]
    public UnityEvent A_Awake;
    [Space]
    [Space]
    [Space]
    public UnityEvent A_Enable;
    public UnityEvent A_Disable;
    [Space]
    [Space]
    [Space]
    public UnityEvent A_Start;
    public UnityEvent A_Destroy;
    [Space]
    [Space]
    [Space]
    public UnityEvent A_Update;
    public UnityEvent A_FixedUpdate;
    public UnityEvent A_LateUpdate;
    [Space]
    [Space]
    [Space]
    public UnityEvent OnSlingShot;

    private void Awake()
    {
        A_Awake?.Invoke();
        if (AllowDebugs) Debug.Log("Awake");
    }

    private void OnEnable()
    {
        A_Enable?.Invoke();
        if (AllowDebugs) Debug.Log("OnEnable");
    }

    private void OnDisable()
    {
        A_Disable?.Invoke();
        if (AllowDebugs) Debug.Log("OnDisable");
    }

    private void Start()
    {
        A_Start?.Invoke();
        if (AllowDebugs) Debug.Log("Start");
    }

    private void OnDestroy()
    {
        A_Destroy?.Invoke();
        if (AllowDebugs) Debug.Log("OnDestroy");
    }



    private void Update()
    {
        A_Update?.Invoke();
        if (AllowDebugs) Debug.Log("Update");
    }

    private void FixedUpdate()
    {
        A_FixedUpdate?.Invoke();
        if (AllowDebugs) Debug.Log("FixedUpdate");
    }

    private void LateUpdate()
    {
        A_LateUpdate?.Invoke();
        if (AllowDebugs) Debug.Log("LateUpdate");
    }

    bool slingShotted = false;
    Vector3 mainStartPos;
    bool startPosSetted = false;
    bool offsetSetted = false;
    Vector3 holderOffset;
    public void DefaultSling()
    {
        if (slingShotted) return;

        //main
        if (!startPosSetted)
        {
            mainStartPos = Main.transform.position;
            startPosSetted = true;

            //isKinematic true for MAin
            Main.GetComponent<Rigidbody>().isKinematic = true;
        }

        //positioning Main
        Vector3 slingOffset = new Vector3(Calibration.localPosition.x * PercentX, Calibration.localPosition.y * PercentY, Calibration.localPosition.z * PercentZ);
        Main.transform.position = mainStartPos + slingOffset;

        //holder
        if (!offsetSetted)
        {
            holderOffset =  Holder.transform.position - Main.transform.position;
            if (AllowDebugs) Debug.Log(holderOffset);
            offsetSetted = true;
        }

        //positioning Holder
        Holder.transform.position = Main.transform.position + holderOffset;

        //calculate where to throw
        CurrentForce = FST.map(0f, 1f, MinForce, MaxForce, -PercentZ);
        Direction = -Main.transform.localPosition.normalized;

        if (ShotBool)
        {
            slingShotted = true;
            ShotBool = false;
            Main.GetComponent<Rigidbody>().isKinematic = false;
            OnSlingShot.Invoke();
            DefaultShot();
        }
    }

    public void DefaultShot()
    {
        Main.transform.parent = null;
        Main.GetComponent<Rigidbody>().AddForce(Direction * CurrentForce, MyForceMode);
    }

    public void Reload()
    {
        Main.transform.parent = this.transform;
        Main.GetComponent<Rigidbody>().isKinematic = true;
        slingShotted = false;
        PercentZ = -0.04f;
    }

}
