﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingshotHolder : MonoBehaviour
{

    Rigidbody rb;
    MeshCollider mc;

    private Vector3 startPos;
    private Quaternion startRot;
    
    void Start()
    {
        gameObject.TryGetComponent<Rigidbody>(out Rigidbody rbTmp);
        rb = rbTmp;

        gameObject.TryGetComponent<MeshCollider>(out MeshCollider mcTmp);
        mc = mcTmp;

        startPos = transform.position;
        startRot = transform.rotation;
    }

    public void EnableBounciness()
    {
        rb.isKinematic = false;
        mc.enabled = true;
    }
    
    public void DisableBounciness()
    {
        rb.isKinematic = true;
        mc.enabled = true;
    }

    public void setStartTransform()
    {
        transform.position = startPos;
        transform.rotation = startRot;
    }

}
