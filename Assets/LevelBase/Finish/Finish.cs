﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Finish : MonoBehaviour
{

    public GameObject WhatCanTrigger;
    public GameObject WhatCanTrigger2;

    public UnityEvent onStart;
    public UnityEvent onTrigger;



    void Start()
    {
        onStart?.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == WhatCanTrigger || other.gameObject == WhatCanTrigger2)
        {
            onTrigger?.Invoke();
        }
    }
}
